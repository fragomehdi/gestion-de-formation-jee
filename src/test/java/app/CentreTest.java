package app;

import static org.junit.Assert.*;

import java.util.List;
import java.util.ResourceBundle.Control;

import javassist.NotFoundException;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.app.model.Controle;
import com.app.model.Etudiant;
import com.app.model.Ville;
import com.app.service.EtudiantService;
import com.app.service.VilleService;
import com.app.model.Centre;
import com.app.service.CentreService;

public class CentreTest {

	private ClassPathXmlApplicationContext context;
	@Before
	public void Setup() throws Exception{
		
	}
	@Test
	public void test() throws NotFoundException {
		
		context = new ClassPathXmlApplicationContext(new String[]{ "applicationContext.xml" });
		VilleService vs = (VilleService) context.getBean("villeService");
		CentreService cs = (CentreService) context.getBean("centreService");
		
		System.out.println("TEST CENTRE *******");
		
		Ville v = new Ville();
		v.setIntitule("EL JADIDA");
		
		vs.add(v);
		
		
		Ville v2 = vs.findById(1);
		
		Centre c = new Centre();
		c.setIntitule("IGA EL JADIDA");
		c.setVille(v2);
		
		cs.add(c);
		
	
Controle con=new Controle();
	con.setId(2);
	EtudiantService etudservice=(EtudiantService) context.getBean("etudiantService");
	List<Etudiant> etud=etudservice.findByControle(con);
	System.out.println(etud.size());
		
	}
}
