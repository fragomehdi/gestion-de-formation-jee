package app;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.app.model.Personnel;
import com.app.service.PersonnelService;

public class PersonnelTest {
	private ClassPathXmlApplicationContext context;
	@Before
	public void Setup() throws Exception{
		
	}
	@Test
	public void test() {
		context = new ClassPathXmlApplicationContext(new String[]{ "applicationContext.xml" });
		PersonnelService ps = (PersonnelService) context.getBean("personnelService");
		List<Personnel> list1 = ps.getAll();
		Personnel r1 = new Personnel();
		r1.setNom("Bahlaouane");
		r1.setPrenom("Hamza");
		ps.add(r1);
		Personnel r2 = new Personnel();
		r2.setNom("Doee");
		r2.setPrenom("John");
		ps.add(r2);

		List<Personnel> list2 = ps.getAll();
		assertTrue((list1.size()+2) == list2.size());
		
	}

}
