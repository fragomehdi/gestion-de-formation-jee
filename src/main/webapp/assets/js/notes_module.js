var app = angular.module('goFormation',["ngResource", "localytics.directives"]);


app.controller('NotesModuleController', ['$scope', '$timeout', '$resource', function($scope, $timeout, $resource) {
	
	/**
	 * Students list
	 */
	$scope.students = [];

	/**
	 * Watch for form elements when they are ready
	 */
	$scope.formSetup = {
		isAnneeReady : true,
		isFormationReady : false,
		isFiliereReady : false,
		isNiveauReady : false,
		isSemestreReady : false,
		isModuleReady : false,
		isMatiereReady : false
	};
	
	/**
	 * Model for each field in form 
	 * Used to retrieve values
	 */
	$scope.models = {
			centre: '',
			annee: '',
			formation:'',
			filiere:'',
			niveau:'',
			semestre:'',
			module: '',
			matiere:''
	}
	
	/**
	 * Watch fields values to enable the next one
	 */
	$scope.$watch('models.centre', function(newValue, oldValue) {
		  if($scope.models.centre != ""){
			  $timeout(function() {
				  $scope.values.annee = $resource(baseURL+'data/getAnnee',{centre_id:$scope.models.centre.id}).query(function(){
					  if($scope.values.annee.length <= 0)
						  swal("Erreur", "Aucune année n'est disponible pour ce centre", "error");
					  else
						  $scope.formSetup.isAnneeReady = true;
				  });
				  
	 			  
			  }, 0); 
		  }
	});
	
	$scope.$watch('models.annee', function(newValue, oldValue) {
		  if($scope.models.annee != ""){
			  $timeout(function() {
				  $scope.values.formation = $resource(baseURL+'data/getFormation',{
					  centre_id:$scope.models.centre.id,
					  annee_id:$scope.models.annee.id
				  }).query(function(){
					  if($scope.values.formation.length <= 0)
						  swal("Erreur", "Aucune formation n'est disponible pour cet année", "error");
					  else
						  $scope.formSetup.isFormationReady = true;
				  });
				  
				  
	 			  
			  }, 0); 
		  }
	});
	
	$scope.$watch('models.formation', function(newValue, oldValue) {
		  if($scope.models.formation != ""){
			  $timeout(function() {
				  $scope.values.filiere = $resource(baseURL+'data/getFiliere',{
					  centre_id:$scope.models.centre.id,
					  annee_id:$scope.models.annee.id,
					  formation_id:$scope.models.formation.id
				  }).query(function(){
					  if($scope.values.filiere.length <= 0)
						  swal("Erreur", "Aucune filière n'est disponible for cette formation", "error");
					  else
						  $scope.formSetup.isFiliereReady = true;
				  });
				  
				  
	 			  
			  }, 0); 
		  }
	});
	
	$scope.$watch('models.filiere', function(newValue, oldValue) {
		  if($scope.models.filiere != ""){
			  $timeout(function() {
				  $scope.values.niveau = $resource(baseURL+'data/getNiveau',{
					  centre_id:$scope.models.centre.id,
					  annee_id:$scope.models.annee.id,
					  formation_id:$scope.models.formation.id,
					  filiere_id:$scope.models.filiere.id
				  }).query(function(){
					  if($scope.values.niveau.length <= 0)
						  swal("Erreur", "Aucun niveau n'est disponible pour cette filière", "error");
					  else
						  $scope.formSetup.isNiveauReady = true;
				  });
				  
	 			  
			  }, 0); 
		  }
	});
	
	$scope.$watch('models.niveau', function(newValue, oldValue) {
		  if($scope.models.niveau != ""){
			  $timeout(function() {
				  $scope.values.semestre = $resource(baseURL+'data/getSemestre',{
					  centre_id:$scope.models.centre.id,
					  annee_id:$scope.models.annee.id,
					  formation_id:$scope.models.formation.id,
					  filiere_id:$scope.models.filiere.id,
					  niveaux_id:$scope.models.niveau.id
				  }).query(function(){
					  if($scope.values.niveau.length <= 0)
						  swal("Erreur", "Aucun semestre n'est disponible pour ce niveau", "error");
					  else
						  $scope.formSetup.isSemestreReady = true;
				  });
				  
			  }, 0); 
		  }
	});
	
	$scope.$watch('models.semestre', function(newValue, oldValue) {
		  if($scope.models.semestre != ""){
			  $timeout(function() {
				  $scope.values.module = $resource(baseURL+'data/getModule',{
					  centre_id:$scope.models.centre.id,
					  annee_id:$scope.models.annee.id,
					  formation_id:$scope.models.formation.id,
					  filiere_id:$scope.models.filiere.id,
					  niveaux_id:$scope.models.niveau.id,
					  semestre_id:$scope.models.semestre.id
				  }).query(function(){
					  if($scope.values.module.length <= 0)
						  swal("Erreur", "Aucun module n'est disponible pour ce semestre", "error");
					  else
						  $scope.formSetup.isModuleReady = true;
				  });
				  
			  }, 0); 
		  }
	});
	
	
	
	/**
	 * Form values
	 */

	$scope.values = {
		centre:centres_list,
		annee:[],
		formation:[],
		filiere:[],
		niveau:[],
		semestre:[],
		module:[],
		matiere:[]
	};
	
	/**
	 * Verify if all fields are filled before submiting
	 */
	$scope.isFormCompleted = function(){
		return (
				$scope.formSetup.isAnneeReady && 	
				$scope.formSetup.isFormationReady && 	
				$scope.formSetup.isFiliereReady && 	
				$scope.formSetup.isNiveauReady && 	
				$scope.formSetup.isSemestreReady && 	
				$scope.formSetup.isModuleReady	
		);
	};
	
	
	/**
	 * Retrieving students list
	 */
	$scope.loading = false;
	$scope.searchStart = function(){
		$scope.loading = true;
		$scope.students = $resource(baseURL+'data/getEtudiantsNoteModule',{
			  module_id:$scope.models.module.id
		  }).query(function(){
			  $scope.loading = false;
		  });
	};

	  
}]);

/**
 * Extend
 */
app.controller('NotesMatiereController', ['$scope', '$timeout', '$resource', '$controller', function($scope, $timeout, $resource, $controller) {
	
	angular.extend(this, $controller('NotesModuleController', {$scope: $scope}));
	
	$scope.$watch('models.module', function(newValue, oldValue) {
		  if($scope.models.module != ""){
			  $timeout(function() {
				  $scope.values.matiere = $resource(baseURL+'data/getMatiere',{
					  centre_id:$scope.models.centre.id,
					  annee_id:$scope.models.annee.id,
					  formation_id:$scope.models.formation.id,
					  filiere_id:$scope.models.filiere.id,
					  niveaux_id:$scope.models.niveau.id,
					  semestre_id:$scope.models.semestre.id,
					  module_id:$scope.models.module.id
				  }).query(function(){
					  if($scope.values.matiere.length <= 0)
						  swal("Erreur", "Aucune matière n'est disponible pour ce module", "error");
					  else
						  $scope.formSetup.isMatiereReady = true;
				  });
				  
			  }, 0); 
		  }
	});
	
	
	/**
	 * Verify if all fields are filled before submiting
	 */
	$scope.isFormCompleted = function(){
		return (
				$scope.formSetup.isAnneeReady && 	
				$scope.formSetup.isFormationReady && 	
				$scope.formSetup.isFiliereReady && 	
				$scope.formSetup.isNiveauReady && 	
				$scope.formSetup.isSemestreReady && 	
				$scope.formSetup.isModuleReady	&&
				$scope.formSetup.isMatiereReady	
		);
	};
	
	
	/**
	 * Retrieving students list
	 */
	$scope.loading = false;
	$scope.searchStart = function(){
		$scope.loading = true;
		$scope.students = $resource(baseURL+'data/getEtudiantsNoteMatiere',{
			  module_id:$scope.models.module.id,
			  matiere_id:$scope.models.matiere.id
		  }).query(function(){
			  $scope.loading = false;
		  });
	};
	
	
}]);



/**
 * Extend
 */
app.controller('NotesBulletinController', ['$scope', '$timeout', '$resource', '$controller', function($scope, $timeout, $resource, $controller) {
	
	angular.extend(this, $controller('NotesModuleController', {$scope: $scope}));

	/**
	 * Verify if all fields are filled before submiting
	 */
	$scope.isFormCompleted = function(){
		return (
				$scope.formSetup.isAnneeReady && 	
				$scope.formSetup.isFormationReady && 	
				$scope.formSetup.isFiliereReady && 	
				$scope.formSetup.isNiveauReady
		);
	};
	
	
	/**
	 * Retrieving students list
	 */
	$scope.loading = false;
	$scope.searchStart = function(){
		$scope.loading = true;
		$scope.students = $resource(baseURL+'data/getEtudiantsNiveau',{
			  niveau_id:$scope.models.niveau.id
		  }).query(function(){
			  $scope.loading = false;
		  });
	};
	
	
}]);