<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="images/favicon.png" type="image/png">

  <title>Go Formation - Connexion</title>

  <link href="<c:url value="/assets/css/style.default.css" />" rel="stylesheet">


  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="<c:url value="/assets/js/html5shiv.js" />"></script>
  <script src="<c:url value="/assets/js/respond.min.js" />"></script>
  <![endif]-->
</head>

<body class="signin">

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.3&appId=394106330645436";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!-- Preloader -->
<div id="preloader">
    <div id="status"><i class="fa fa-spinner fa-spin"></i></div>
</div>

<section>
  
    <div class="signinpanel">
        <div class="logopanel">
                        <h1><span>[</span> GoFormation <span>]</span></h1>
                    </div><!-- logopanel -->
                
                    <div class="mb20"></div>
                
                    <h5><strong>LE PONT VERS L'ENTREPRISE</strong></h5>
        <div class="row">
            
            <div class="col-md-7">
                
                <div class="signin-info">
                    
                    <div class="fb-page" data-width="500" data-href="https://www.facebook.com/igaeljadida" data-hide-cover="false" data-show-facepile="true" data-show-posts="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/igaeljadida"><a href="https://www.facebook.com/igaeljadida">IGA El Jadida</a></blockquote></div></div>
                    
                    <!--<ul>
                        <li><i class="fa fa-arrow-circle-o-right mr5"></i> Lorem ipsum dolor.</li>
                        <li><i class="fa fa-arrow-circle-o-right mr5"></i> Lorem ipsum dolor sit amet.</li>
                        <li><i class="fa fa-arrow-circle-o-right mr5"></i> Lorem ipsum dolor sit amet, consectetur.</li>
                        <li><i class="fa fa-arrow-circle-o-right mr5"></i> Lorem ipsum dolor sit.</li>
                        <li><i class="fa fa-arrow-circle-o-right mr5"></i> and much more...</li>
                    </ul> -->
                    <div class="mb20"></div>
                    <!--  <strong>Not a member? <a href="signup.html">Sign Up</a></strong> -->
                </div><!-- signin0-info -->
            
            </div><!-- col-sm-7 -->
            
            <div class="col-md-5">
            
            
                
                <form method="post" action="<c:url value='j_spring_security_check'/>">
                    <h4 class="nomargin">Connexion</h4>
                    <c:if test="${not empty SPRING_SECURITY_LAST_EXCEPTION}">
            <div class="alert alert-danger" role="alert">
								<strong>Oh snap!</strong> Votre tentative de connexion a �chou� en raison de <c:out value="${SPRING_SECURITY_LAST_EXCEPTION.message}" />.
							</div>
				
			</c:if>
                    <p class="mt5 mb20">Vous devez vous identifier pour acc�der � votre compte..</p>
                
                    <input type="text" class="form-control uname"  name="j_username" required placeholder="Nom d'utilisateur" />
                    <input type="password" class="form-control pword" name="j_password" required placeholder="Mot de passe" />
                    <!-- <a href="#"><small>Forgot Your Password?</small></a> -->
                    <button type="submit" class="btn btn-success btn-block">Connexion</button>
                    
                </form>
            </div><!-- col-sm-5 -->
            
        </div><!-- row -->
        
        <div class="signup-footer">
            <div class="pull-left">
                &copy; 2015. All Rights Reserved.
            </div>
            <div class="pull-right">
                Created By: <a href="http://www.iga-eljadida.ma/" target="_blank">IGA EL JADIDA</a>
            </div>
        </div>
        
    </div><!-- signin -->
  
</section>


<script src="<c:url value="/assets/js/jquery-1.10.2.min.js" />"></script>
<script src="<c:url value="/assets/js/jquery-migrate-1.2.1.min.js" />"></script>
<script src="<c:url value="/assets/js/bootstrap.min.js" />"></script>
<script src="<c:url value="/assets/js/modernizr.min.js" />"></script>
<script src="<c:url value="/assets/js/retina.min.js" />"></script>
<script src="<c:url value="/assets/js/toggles.min.js" />"></script>
<script src="<c:url value="/assets/js/jquery.sparkline.min.js" />"></script>
<script src="<c:url value="/assets/js/jquery.cookies.js" />"></script>
<script src="<c:url value="/assets/js/custom.js" />"></script>

</body>
</html>
