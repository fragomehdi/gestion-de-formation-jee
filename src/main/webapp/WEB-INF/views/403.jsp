<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="images/favicon.png" type="image/png">

  <title>Go Formation - 403</title>

  <link href="<c:url value="/assets/css/style.default.css" />" rel="stylesheet">


  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="<c:url value="/assets/js/html5shiv.js" />"></script>
  <script src="<c:url value="/assets/js/respond.min.js" />"></script>
  <![endif]-->
</head>

<body class="notfound">

<!-- Preloader -->
<div id="preloader">
    <div id="status"><i class="fa fa-spinner fa-spin"></i></div>
</div>

<section>
  
  <div class="notfoundpanel">
    <h1>403!</h1>
    <h3>Vous n'avez pas la permission pour acc�der � cette page.</h3>
   <h4>Apparemment cette page n'est accessible que si vous avez un certain privil�ge <br />Contactez l'administrateur ou essayez de revenir � la page ant�c�dente</h4>
  	<p class="text-center">
  		<a href="javascript:window.history.back();" class="btn btn-success"><span class="glyphicon glyphicon-circle-arrow-left"></span>  Page Ant�c�dente</a>
  	</p>
  </div><!-- notfoundpanel -->
  
</section>



<script src="<c:url value="/assets/js/jquery-1.10.2.min.js" />"></script>
<script src="<c:url value="/assets/js/jquery-migrate-1.2.1.min.js" />"></script>
<script src="<c:url value="/assets/js/bootstrap.min.js" />"></script>
<script src="<c:url value="/assets/js/modernizr.min.js" />"></script>
<script src="<c:url value="/assets/js/retina.min.js" />"></script>
<script src="<c:url value="/assets/js/toggles.min.js" />"></script>
<script src="<c:url value="/assets/js/jquery.sparkline.min.js" />"></script>
<script src="<c:url value="/assets/js/jquery.cookies.js" />"></script>
<script src="<c:url value="/assets/js/custom.js" />"></script>

</body>
</html>
