<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f"%>
<%@taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>


<%! String menuActuel = "e_emploie";  %>

<jsp:include page="../../layout/header.jsp" />
<jsp:include page="../../layout/leftpanel.jsp" />
<jsp:include page="../../layout/topmenu.jsp" />


<div class="pageheader">
      <h2><i class="fa fa-graduation-cap"></i> Étudiants <span>Emploie du temps</span></h2>
      <div class="breadcrumb-wrapper">
          <span class="label">Vous êtes ici:</span>
        <ol class="breadcrumb">
          <li><a href="#.">Étudiants</a></li>
          <li class="active">Emploie du temps</li>
        </ol>
      </div>
    </div>
    
   
        	<div class="panel panel-default">
        <div class="panel-heading">
          <div class="panel-btns">
            <a href="#" class="panel-close">&times;</a>
            <a href="#" class="minimize">&minus;</a>
          </div><!-- panel-btns -->
          
           <c:forEach var="v" items="${liste_emploies}">
 <div class="text-center">
          	<a href="<c:url value="/etudiants/emploie/dowload?file=${v.path}"/>"  style=" color: #D9534F;  font-size: 550%; " ><i class="fa fa-cloud-download fa-5x" ></i></a>
<br>

<a style="
	text-shadow: 8px 3px 23px rgba(255, 15, 15, 0.88);
	color: #010408;
	font-size: 56px;
">Telecharger</a> 
 </div>
 
          </c:forEach>
        </div>
        <div class="panel-body">
	          
	   </div>
	  </div>

        
<jsp:include page="../../layout/rightpanel.jsp" />
<jsp:include page="../../layout/footer.jsp" />
<script src="<c:url value="/assets/js/chosen.jquery.min.js" />"></script>

<script>
jQuery(document).ready(function() {

	  jQuery("a#<%= menuActuel %>").parent("li").addClass("active");

});


jQuery(document).ready(function() {
	
    
    jQuery('#table1').dataTable({
        "sPaginationType": "full_numbers",
        "aoColumnDefs" : [ {
    	    "bSortable" : false,
    	    "aTargets" : [ "no-sort" ]
    	} ]
      });
    
    // Chosen Select
    jQuery("select").chosen({
      'min-width': '100px',
      'white-space': 'nowrap',
      disable_search_threshold: 10
    });
    
    jQuery("a.delete").click(function(e){
	  	  e.preventDefault();
	  	  var url = $(this).attr("href");
	      swal(
	      {
	          title: "Êtes-vous sure?",
	          text: "Vous ne serez pas en mesure de récupérer cet élément",
	          type: "warning",
	          showCancelButton: true,
	          confirmButtonColor: "#DD6B55",
	          confirmButtonText: "Oui, supprimez-le!",
	          cancelButtonText: "Non, annuler!",
	          closeOnConfirm: false,
	          closeOnCancel: false
	      },
	      function(isConfirm)
	      {
	          if (isConfirm) {
	          	window.location = url;
	          	swal("Suppression!", "L'élement va être supprimé dans quelques instants.", "success");
	          }
	          else {
	              swal("Annulé", "Aucune opération n'a été effectuer", "error");
	          }
	      });
	      
	      return false;
	    });

  });

</script>

</body>
</html>