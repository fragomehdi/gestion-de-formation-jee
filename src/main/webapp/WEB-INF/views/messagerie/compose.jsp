<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f"%>
<%@taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>

<%! String menuActuel = "menu_messagerie";  %>

<jsp:include page="../../views/layout/header.jsp" />
<jsp:include page="../../views/layout/leftpanel.jsp" />
<jsp:include page="../../views/layout/topmenu.jsp" />


<div class="pageheader">
      <h2><i class="fa fa-envelope"></i> Messagerie <span>Composer</span></h2>
      <div class="breadcrumb-wrapper">
          <span class="label">Vous êtes ici:</span>
        <ol class="breadcrumb">
          <li><a href="<c:url value="/messagerie" />">Messagerie</a></li>
          <li class="active">Composer</li>
        </ol>
      </div>
    </div>
    
    
        <div class="contentpanel panel-email">

        <div class="row">
        	<div class="col-sm-3 col-lg-2">
                <a href="<c:url value="/messagerie/compose" />" class="btn btn-danger btn-block btn-compose-email">Nouveau Message</a>
                
               
                <ul class="nav nav-pills nav-stacked nav-email">
                    <li >
                    <a href="<c:url value="/messagerie/" />">
                        <!-- <span class="badge pull-right">2</span> -->
                        <i class="glyphicon glyphicon-inbox"></i> Reçus
                    </a>
                    </li>
                    <li class=""><a href="<c:url value="/messagerie/outbox" />"><i class="glyphicon glyphicon-send"></i> Envoyees</a></li>
                   
                </ul>
                
                <div class="mb30"></div>
        	</div><!-- col-sm-3 -->
            <div class="col-sm-9 col-lg-10">
            
            <form class="form-horizontal form-bordered" method="post" action="envoyer">
                
                <div class="panel panel-default">
                    <div class="panel-body">

                        <h5 class="subtitle mb5">Composer un nouveau message</h5>
                        
                        <c:if test="${success != null}">
							<div class="alert alert-success" role="alert">
								<strong>Well done!</strong> Votre message a ete bien envoyer
							</div>
						</c:if>
                        
                        
            
					            <div class="form-group">
					              <label class="col-sm-3 control-label">Sujet</label>
					              <div class="col-sm-6">
					                <input type="text" placeholder="Sujet" name="subject" required class="form-control" />
					              </div>
					            </div>
					            
					            <div class="form-group">
					              <label class="col-sm-3 control-label">À</label>
					              <div class="col-sm-6">
					                <select name="user" id="" data-placeholder="Choissiser un utilisateur" style="width:350px;">
					                <c:forEach var="u" items="${users}">
					                	<option value="${u.id }">${u.username}</option>
				                	</c:forEach>
					                </select>
					              </div>
					            </div>
					            
					            <div class="form-group">
					              <label class="col-sm-3 control-label">Contenu</label>
					              <div class="col-sm-7">
					                <textarea class="form-control" name="message" required rows="5"></textarea>
					              </div>
					            </div>
					            
            			
                        
                    </div><!-- panel-body -->
                    
                    <div class="panel-footer">
			 <div class="row">
				<div class="col-sm-6 col-sm-offset-3">
				  <button type="submit" class="btn btn-primary">Envoyer</button>&nbsp;
				  <button type="reset" class="btn btn-default">Annuler</button>
				</div>
			 </div>
		  </div><!-- panel-footer -->
                </div><!-- panel -->
                
                </form>
                
            </div><!-- col-sm-9 -->
            
        </div><!-- row -->
    
    </div>
    
    
<jsp:include page="../../views/layout/rightpanel.jsp" />
<jsp:include page="../../views/layout/footer.jsp" />

<script src="<c:url value="/assets/js/chosen.jquery.min.js" />"></script>

<script>

  jQuery(document).ready(function() {
	    jQuery("a#<%= menuActuel %>").parent("li").addClass("active");

    jQuery('#table1').dataTable({
        "sPaginationType": "full_numbers"
      });
    
 // Chosen Select
    jQuery("select").chosen({
      'min-width': '400px',
      'white-space': 'nowrap'
    });


  });
</script>

</body>
</html>