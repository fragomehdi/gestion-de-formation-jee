<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f"%>
<%@taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>

<%! String menuActuel = "menu_messagerie";  %>

<jsp:include page="../../views/layout/header.jsp" />
<jsp:include page="../../views/layout/leftpanel.jsp" />
<jsp:include page="../../views/layout/topmenu.jsp" />


<div class="pageheader">
      <h2><i class="fa fa-envelope"></i> Messagerie <span>Boite de réception</span></h2>
      <div class="breadcrumb-wrapper">
          <span class="label">Vous êtes ici:</span>
        <ol class="breadcrumb">
          <li><a href="<c:url value="/messagerie" />">Messagerie</a></li>
          <li class="active">Boite de réception</li>
        </ol>
      </div>
    </div>
    
    
        <div class="contentpanel panel-email">

        <div class="row">
        	<div class="col-sm-3 col-lg-2">
                <a href="<c:url value="/messagerie/compose" />" class="btn btn-danger btn-block btn-compose-email">Nouveau Message</a>
                
                <ul class="nav nav-pills nav-stacked nav-email">
                    <li class="active">
                    <a href="<c:url value="/messagerie/" />">
                        <!-- <span class="badge pull-right">2</span> -->
                        <i class="glyphicon glyphicon-inbox"></i> Reçus
                    </a>
                    </li>
                    <li><a href="<c:url value="/messagerie/outbox" />"><i class="glyphicon glyphicon-send"></i> Envoyees</a></li>
                   
                </ul>
                
                <div class="mb30"></div>
        	</div><!-- col-sm-3 -->
            <div class="col-sm-9 col-lg-10">
                
                <div class="panel panel-default">
                    <div class="panel-body">

                        <div class="read-panel">
                            
                            <div class="media">
                                <a href="#" class="pull-left">
                                    <img alt="" src="<c:url value="/assets/images/user-uknown.png" />" class="media-object">
                                </a>
                                <div class="media-body">
                                    <h4 class="text-primary">${message.from.username }</h4>
                                    <small class="text-muted"><joda:format pattern="MM/dd/yyyy H:m:s" value="${message.dateMessage}" /></small>
                                </div>
                            </div><!-- media -->
                            
                            <h4 class="email-subject">${message.subject }</h4>
                            
                                                 <hr />
                            
                            <c:forEach var="m" items="${message.messages}">
                            <div class="well" id="message${m.id }">
                            	<span class="media-meta pull-right"><joda:format pattern="MM/dd/yyyy H:m:s" value="${m.dateMessage}" /></span>
                            	<h5 class="text-primary">${m.from.username }</h5>
                            ${m.contenu }
                            </div>
                            </c:forEach>
                            
                     </div>
                                          
                     <hr />
                            
                     <form action="reply" method="post">
                     	<input type="hidden" name="thread_id" value="${message.id }" />
                     	<div class="form-group">
                     		<textarea class="form-control" name="message" placeholder="Répondre ici"></textarea>
                     </div>
                     <p class="text-right">
                     	<input type="submit" value="Envoyer" class="btn btn-success" />
                     </p>
                     
                     
                     </form>
                             
                        
                        
                        
                        
                    </div><!-- panel-body -->
                </div><!-- panel -->
                
            </div><!-- col-sm-9 -->
            
        </div><!-- row -->
    
    </div>
    
    
<jsp:include page="../../views/layout/rightpanel.jsp" />
<jsp:include page="../../views/layout/footer.jsp" />

<script src="<c:url value="/assets/js/chosen.jquery.min.js" />"></script>

<script>

  jQuery(document).ready(function() {
	    jQuery("a#<%= menuActuel %>").parent("li").addClass("active");

    jQuery('#table1').dataTable({
        "sPaginationType": "full_numbers"
      });
    
 // Chosen Select
    jQuery("select").chosen({
      'min-width': '100px',
      'white-space': 'nowrap',
      disable_search_threshold: 10
    });
 
 
 	// Read mail
    jQuery('.table-email .media').click(function(){
    	var id = $(this).data("id");
        location.href="<c:url value="/messagerie/view/" />"+id;
    });
    

  });
</script>

</body>
</html>