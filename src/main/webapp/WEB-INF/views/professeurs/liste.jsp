<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f"%>


<%! String menuActuel = "professeurs";  %>


<jsp:include page="../../views/layout/header.jsp" />
<jsp:include page="../../views/layout/leftpanel.jsp" />
<jsp:include page="../../views/layout/topmenu.jsp" />

<div class="pageheader">
      <h2><i class="fa fa-user"></i> liste des professeurs </h2>
    
    </div>
    
    <div class="contentpanel">
    
      <ul class="letter-list">
        <li><a href="">a</a></li>
        <li><a href="">b</a></li>
        <li><a href="">c</a></li>
        <li><a href="">d</a></li>
        <li><a href="">e</a></li>
        <li><a href="">f</a></li>
        <li><a href="">g</a></li>
        <li><a href="">h</a></li>
        <li><a href="">i</a></li>
        <li><a href="">j</a></li>
        <li><a href="">k</a></li>
        <li><a href="">l</a></li>
        <li><a href="">m</a></li>
        <li><a href="">n</a></li>
        <li><a href="">o</a></li>
        <li><a href="">p</a></li>
        <li><a href="">q</a></li>
        <li><a href="">r</a></li>
        <li><a href="">s</a></li>
        <li><a href="">t</a></li>
        <li><a href="">u</a></li>
        <li><a href="">v</a></li>
        <li><a href="">w</a></li>
        <li><a href="">x</a></li>
        <li><a href="">y</a></li>
        <li><a href="">z</a></li>
      </ul>
      <div class="mb30"></div>
    
    
    <div class="people-list">
        <div class="row">
        
        
                             <c:forEach var="v" items="${liste}">
        
    <div class="col-md-6">
      
            <div class="people-item">
             
  
              <div class="media">
                <a href="#" class="pull-left">
                  <img alt="" src="professeurs/consulter?path=${v.image}" class="thumbnail media-object">
                </a>
                <div class="media-body">
                
                
               
					<h4 class="person-name">${v.nom} ${v.prenom}</h4>
                  <div class="text-muted"><i class="fa fa-map-marker"></i> ${v.adresse }</div>
                  <div class="text-muted"><i class="fa fa-briefcase"></i> ${v.fonction } <a href="">IGA.</a></div>
                  <ul class="social-list">
                    <li><a href="mailto:?email=${v.email }" class="tooltips" data-toggle="tooltip" data-placement="top" title="Email"><i class="fa fa-envelope-o"></i></a></li>
                    <li><a href="${v.facebook }" class="tooltips" data-toggle="tooltip" data-placement="top" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="${v.twiter }" class="tooltips" data-toggle="tooltip" data-placement="top" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="" class="tooltips" data-toggle="tooltip" data-placement="top" title="LinkedIn"><i class="fa fa-linkedin"></i></a></li>
                    <li><a href="" class="tooltips" data-toggle="tooltip" data-placement="top" title="Skype"><i class="fa fa-skype"></i></a></li>
                  </ul>
					
                
                
                
                  
                </div>
              </div>
            </div>
          </div><!-- col-md-6 -->
    
    </c:forEach>
    
    

       </div><!-- ROW -->
       </div><!-- PEOPLE LIST -->
    </div>
    
<jsp:include page="../../views/layout/rightpanel.jsp" />
<jsp:include page="../../views/layout/footer.jsp">
	<jsp:param name="javascripts" value="/assets/js/jquery.mousewheel.js" />
	<jsp:param name="javascripts" value="/assets/js/chosen.jquery.min.js" />
	<jsp:param name="javascripts" value="/assets/js/jquery.validate.min.js" />
</jsp:include>


<script>
  jQuery(document).ready(function() {

	  jQuery("a#<%= menuActuel %>").parent("li").addClass("active");

  });
</script>

</body>
</html>