<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f"%>


<%! String menuActuel = "menu_parametrage";  %>
<%! String sousMenuActuel = "menu_parametrage_journal";  %>


<jsp:include page="../../views/layout/header.jsp" />
<jsp:include page="../../views/layout/leftpanel.jsp" />
<jsp:include page="../../views/layout/topmenu.jsp" />


<div class="pageheader">
      <h2><i class="fa fa-cogs"></i> Paramétrage <span>Journeaux de logs</span></h2>
      <div class="breadcrumb-wrapper">
          <span class="label">Vous êtes ici:</span>
        <ol class="breadcrumb">
          <li><a href="<c:url value="journalisation" />">Paramétrage</a></li>
          <li class="active">Journeaux de logs</li>
        </ol>
      </div>
    </div>
    
    
    
    
    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-btns">
                    <a href="#" class="panel-close">&times;</a>
                    <a href="#" class="minimize">&minus;</a>
                </div><!-- panel-btns -->
                <h3 class="panel-title">Journal de log</h3>
            </div>
            <div class="panel-body">
            
            <c:if test="${error_journal != null}">
							<div class="alert alert-warning" role="alert">
								<strong>Oh snap!</strong> ${error_journal}
							</div>
						</c:if>
            
            

            	<form action="downloadJournal" method="post">
            	
            	<div class="row">
		         <div class="col-md-6">
		             <label>Du:</label>
		             <div class="input-group">
		                 <input type="text" required name="date_start" class="form-control datepicker-multiplee" placeholder="mm/dd/yyyy">
		                 <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
		             </div>
		
		
		         </div>
		         <div class="col-md-6">
		
		             <label>À:</label>
		             <div class="input-group">
		                 <input type="text" required name="date_end" class="form-control datepicker-multiplee" placeholder="mm/dd/yyyy">
		                 <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
		             </div>
		         </div>
	     	</div>
              <p class="text-center">
                <br>
                <button type="submit" class="btn btn-success"> <span class="glyphicon glyphicon-cloud-upload"></span> Telecharger</button>
              </p>
            	
            	</form>
              
         </div>
           
            </div><!-- panel-body -->
          </div><!-- panel -->
          
     


               
        

           
    
    
<jsp:include page="../../views/layout/rightpanel.jsp" />
<jsp:include page="../../views/layout/footer.jsp" />


<script>
  jQuery(document).ready(function() {

    jQuery(".nav-parent > a#<%= menuActuel %>").trigger("click");
    jQuery(".nav-parent > a#<%= menuActuel %>").parent("li").addClass("active");
    jQuery(".nav-parent > ul.children > li#<%= sousMenuActuel %>").addClass("active");

   

    jQuery('.datepicker-multiplee').datepicker({
        numberOfMonths: 3,
        showButtonPanel: true
      });


  });
</script>

</body>
</html>