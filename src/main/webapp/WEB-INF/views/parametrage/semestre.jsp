<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f"%>


<%! String menuActuel = "menu_parametrage";  %>
<%! String sousMenuActuel = "menu_parametrage_semestre";  %>


<jsp:include page="../../views/layout/header.jsp" />
<jsp:include page="../../views/layout/leftpanel.jsp" />
<jsp:include page="../../views/layout/topmenu.jsp" />


<div class="pageheader">
      <h2><i class="fa fa-cogs"></i> Paramétrage <span>Semestres</span></h2>
      <div class="breadcrumb-wrapper">
          <span class="label">Vous êtes ici:</span>
        <ol class="breadcrumb">
          <li><a href="<c:url value="/parametrage/semestre" />">Paramétrage</a></li>
          <li class="active">Semestres</li>
        </ol>
      </div>
    </div>
    
    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-btns">
                    <a href="#" class="panel-close">&times;</a>
                    <a href="#" class="minimize">&minus;</a>
                </div><!-- panel-btns -->
                <h3 class="panel-title">Configuration des Semestres</h3>
            </div>
            <div class="panel-body">

            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <h5 class="subtitle mb5">Semestres</h5>
                    
                    <c:if test="${success_semestre_delete != null}">
							<div class="alert alert-success" role="alert">
								<strong>Well done!</strong> ${success_semestre_delete}
							</div>
						</c:if>
						
						<c:if test="${error_semestre_delete != null}">
							<div class="alert alert-danger" role="alert">
								<strong>Oh snap!</strong> ${error_semestre_delete}
							</div>
						</c:if>
                    <c:if test="${success_semestre != null}">
							<div class="alert alert-success" role="alert">
								<strong>Well done!</strong> ${success_semestre}
							</div>
						</c:if>
						
						
							<c:if test="${error_semestre != null}">
							<div class="alert alert-danger" role="alert">
								<strong>Oh snap!</strong> ${error_semestre}
							</div>
						</c:if>
                    <div class="table-responsive">
                       <table class="table table-hidaction table-bordered mb30">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Intitulé</th>
                                <th>Centre</th>
                                <th>Année</th>
                                 <th>Formation</th>
                                  <th>Filière</th>
                                   <th>Niveaux</th>
                                <th></th>
                            </tr>
                            </thead>
                            
                            <tbody>
                              <c:forEach var="v" items="${liste_semestres}">
                            <tr>
                                    <td>${v.id}</td>
                                    <td>${v.intitule}</td>
                                    <td>${v.centre.intitule}</td>
                                    <td>${v.annee.intitule}</td>
                                    <td>${v.formation.intitule}</td>
                                    <td>${v.filiere.intitule}</td>
                                    <td>${v.niveaux.intitule}</td>
                                    
                                    <td class="table-action-hide">
                                        <a href="editSemestre?id=${v.id}"><i class="fa fa-pencil"></i></a>
                                        <a href="deleteSemestre?id=${v.id}" data-id="${v.id}" class="delete-row"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                         
								</c:forEach>
                            </tbody>
                        </table>
                    </div><!-- table-responsive -->
                </div><!-- col-md-6 -->

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <h5 class="subtitle mb5"></h5>
                    
                    

                    <f:form class="form-horizontal form-bordered configForm" method="post" action="saveSemestre" modelAttribute="semestreForm">
                     		<input type="hidden" name="mode" value="${mode}" />
                     		<f:hidden path="id" />

                        

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Intitulé <span class="asterisk">*</span></label>
                            <div class="col-sm-6">
                                <f:input path="intitule" cssClass="form-control" size="20"/>
                               	<f:errors path="intitule" cssClass="error"></f:errors>
                            </div>
                        </div>
                          
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Centre <span class="asterisk">*</span></label>
                            <div class="col-sm-6">
                                <f:select path="centre" class="form-control chosen-select">
			                    	<f:option value="0"> --SELECTIONNER UN CENTRE--</f:option>
								    <f:options items="${centres}" itemValue="id" itemLabel="intitule" />
								</f:select>
                               	<f:errors path="centre" cssClass="error"></f:errors>
                            </div>
                        </div>
                        
                        
                        
                         <div class="form-group">
                            <label class="col-sm-3 control-label">Année <span class="asterisk">*</span></label>
                            <div class="col-sm-6">
                                <f:select path="annee" class="form-control chosen-select">
			                    	<f:option value="0"> --SELECTIONNER UNE ANNÉE--</f:option>
								    <f:options items="${annees}" itemValue="id" itemLabel="intitule" />
								</f:select>
                               	<f:errors path="annee" cssClass="error"></f:errors>
                            </div>
                        </div>
                        
                      
                        
                        
                        
                             <div class="form-group">
                            <label class="col-sm-3 control-label">Formations <span class="asterisk">*</span></label>
                            <div class="col-sm-6">
                                <f:select path="formation" class="form-control chosen-select">
			                    	<f:option value="0"> --SELECTIONNER UNE FORMATIONS--</f:option>
								    <f:options items="${formations}" itemValue="id" itemLabel="intitule" />
								</f:select>
                               	<f:errors path="formation" cssClass="error"></f:errors>
                            </div>
                        </div>
                        
                        
                        
                        
                        
                        
                             <div class="form-group">
                            <label class="col-sm-3 control-label">Filières <span class="asterisk">*</span></label>
                            <div class="col-sm-6">
                                <f:select path="filiere" class="form-control chosen-select">
			                    	<f:option value="0"> --SELECTIONNER UNE FILIERE--</f:option>
								    <f:options items="${filieres}" itemValue="id" itemLabel="intitule" />
								</f:select>
                               	<f:errors path="filiere" cssClass="error"></f:errors>
                            </div>
                        </div>
                        
                        
                        
                        
                             <div class="form-group">
                            <label class="col-sm-3 control-label">Niveaux <span class="asterisk">*</span></label>
                            <div class="col-sm-6">
                                <f:select path="niveaux" class="form-control chosen-select">
			                    	<f:option value="0"> --SELECTIONNER UN NIVEAUX--</f:option>
								    <f:options items="${niveaux}" itemValue="id" itemLabel="intitule" />
								</f:select>
                               	<f:errors path="niveaux" cssClass="error"></f:errors>
                            </div>
                        </div>
                        
                       
                        
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-10">
                                <button type="submit" class="btn btn-primary">Valider</button>
                            </div>
                        </div>
                    </f:form>


                </div><!-- col-md-6 -->

            </div><!-- row -->
            </div>
            </div> <!-- /PANEL -->

        
                                  
    
<jsp:include page="../../views/layout/rightpanel.jsp" />
<jsp:include page="../../views/layout/footer.jsp">
	<jsp:param name="javascripts" value="/assets/js/jquery.mousewheel.js" />
	<jsp:param name="javascripts" value="/assets/js/chosen.jquery.min.js" />
	<jsp:param name="javascripts" value="/assets/js/jquery.validate.min.js" />
</jsp:include>


<script>
  jQuery(document).ready(function() {

    jQuery(".nav-parent > a#<%= menuActuel %>").trigger("click");
    jQuery(".nav-parent > a#<%= menuActuel %>").parent("li").addClass("active");
    jQuery(".nav-parent > ul.children > li#<%= sousMenuActuel %>").addClass("active");

    // Show aciton upon row hover
    jQuery('.table-hidaction tbody tr').hover(function(){
      jQuery(this).find('.table-action-hide a').animate({opacity: 1});
    },function(){
      jQuery(this).find('.table-action-hide a').animate({opacity: 0});
    });

    // Chosen Select
  jQuery(".chosen-select").chosen({'width':'100%','white-space':'nowrap'});


  // Basic Form
  /*
  jQuery(".configForm").validate({
    highlight: function(element) {
      jQuery(element).closest('.form-group').removeClass('has-success').addClass('has-error');
    },
    success: function(element) {
      jQuery(element).closest('.form-group').removeClass('has-error');
    }
  });*/

  jQuery("a.delete-row").click(function(e){
	  e.preventDefault();
	  var url = $(this).attr("href");
    swal(
    {
        title: "Êtes-vous sure?",
        text: "Vous ne serez pas en mesure de récupérer cet élément",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Oui, supprimez-le!",
        cancelButtonText: "Non, annuler!",
        closeOnConfirm: false,
        closeOnCancel: false
    },
    function(isConfirm)
    {
        if (isConfirm) {
        	window.location = url;
            //swal("Supprimé!", "L'élement a été supprimé.", "success");
        }
        else {
            swal("Annulé", "Aucune opération n'a été effectuer", "error");
        }
    });
    
    return false;
  });


  });
</script>

</body>
</html>