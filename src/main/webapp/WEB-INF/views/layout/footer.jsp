<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script src="<c:url value="/assets/js/jquery-1.10.2.min.js" />"></script>
<script src="<c:url value="/assets/js/jquery-migrate-1.2.1.min.js" />"></script>
<script src="<c:url value="/assets/js/jquery-ui-1.10.3.min.js" />"></script>
<script src="<c:url value="/assets/js/bootstrap.min.js" />"></script>
<script src="<c:url value="/assets/js/modernizr.min.js" />"></script>
<script src="<c:url value="/assets/js/jquery.sparkline.min.js" />"></script>
<script src="<c:url value="/assets/js/toggles.min.js" />"></script>
<script src="<c:url value="/assets/js/retina.min.js" />"></script>
<script src="<c:url value="/assets/js/jquery.cookies.js" />"></script>
<script src="<c:url value="/assets/bower_components/sweetalert/lib/sweet-alert.min.js" />"></script>
<script src="<c:url value="/assets/js/jquery.datatables.min.js" />"></script>

<script src="<c:url value="/assets/js/custom.js" />"></script>

<script>
function guid() {
	  function s4() {
	    return Math.floor((1 + Math.random()) * 0x10000)
	      .toString(16)
	      .substring(1);
	  }
	  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
	    s4() + '-' + s4() + s4() + s4();
	}
	
jQuery(document).ready(function($){
	// Get messages
	
	function getMessages(){
		
		// Update counter
		$.get("<c:url value="/data/get_total_messages" />", function(d){
			var total = d.content;
			
			if(total > 0)
			{
				$("#top_total_messages").text(total).removeClass("hidden");
				$("#left_total_messages").text(total).removeClass("hidden");
				$("#top_total_messages_next").text(total);
			}
			else
			{
				$("#top_total_messages").text("0").removeClass("hidden").addClass("hidden");
				$("#left_total_messages").text("0").removeClass("hidden").addClass("hidden");
				$("#top_total_messages_next").text("0");
			}
			
		});
		
		// Update list
		$.getJSON("<c:url value="/data/get_new_messages" />", function(d){
			var el = $("ul#messages_list_top");
			el.empty();
			
			if(d.length > 0)
			{
				
				$.each(d,function(k,v){
					var isNew = "";
					
					var hash = guid();
					
					if(v.read == 2)
						isNew = '<span class="badge badge-success">new</span></span>';
					var element = '<li class="new">'+
			        '<a href="<c:url value="/messagerie/view?hash='+hash+'&id='+v.messageThread.id+'#message'+v.id+'" />">'+
			        '<span class="thumb"><img src="<c:url value="/assets/images/user-uknown.png" />" alt="" /></span>'+
			        '<span class="desc">'+
			        '  <span class="name">'+v.from.username+' '+isNew+''+
			        '  <span class="msg">'+v.contenu+'</span>'+
			        '</span>'+
			        '</a>'+
			      '</li>';
			      el.append(element);
				});
				
				el.append(' <li class="new"><a href="<c:url value="/messagerie" />">Lire tous les messages</a></li>');
				
			}
			
		});
		
		
	}
	
	/*getMessages();
	
	setInterval(function(){
		getMessages();
	},10000);
	*/
});

</script>

<c:if test="${not empty paramValues.javascripts}">
	<c:forEach var="javascript" items="${paramValues.javascripts}">
		<script src="<c:url value="${javascript}" />"></script>
	</c:forEach>
</c:if>


