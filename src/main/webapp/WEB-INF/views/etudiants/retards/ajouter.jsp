<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f"%>


<%! String menuActuel = "menu_etudiants";  %>
<%! String sousMenuActuel = "menu_etudiants_retards";  %>


<jsp:include page="../../../views/layout/header.jsp">
	<jsp:param name="stylesheets" value="/assets/css/bootstrap-timepicker.min.css" />
</jsp:include>

<jsp:include page="../../../views/layout/leftpanel.jsp" />
<jsp:include page="../../../views/layout/topmenu.jsp" />


<div class="pageheader">
      <h2><i class="fa fa-users"></i> Étudiants <span>Retards</span></h2>
      <div class="breadcrumb-wrapper">
          <span class="label">Vous êtes ici:</span>
        <ol class="breadcrumb">
          <li><a href="<c:url value="etudiants" />">Étudiants</a></li>
          <li class="active">Retards</li>
        </ol>
      </div>
    </div>
    
    <div class="contentpanel">
     <div class="panel panel-default">
        <div class="panel-heading">
          <div class="panel-btns">
            <a href="#" class="panel-close">&times;</a>
            <a href="#" class="minimize">&minus;</a>
          </div><!-- panel-btns -->
          <h3 class="panel-title">Ajouter</h3>
        </div>
        <f:form method="post" action="save" modelAttribute="pForm">
			
        <div class="panel-body">
        <c:if test="${success != null}">
							<div class="alert alert-success" role="alert">
								<strong>Well done!</strong> ${success}
							</div>
						</c:if>
						
						<c:if test="${error != null}">
							<div class="alert alert-danger" role="alert">
								<strong>Oh snap!</strong> ${error}
							</div>
						</c:if>
			
						
        	 <div class="row mb15">
                <div class="col-sm-4">
                  <div class="form-group">
                    <label class="control-label">Date Séance</label>
                    <f:input path="dateSeance" cssClass="form-control datepicker-multiplee"/>
                    <f:errors path="dateSeance" cssClass="error"></f:errors>
                  </div>
                </div><!-- col-sm-4 -->
                <div class="col-sm-4">
                  <div class="form-group">
                    <label class="control-label">Heure Début</label>
                    <div class="input-group mb15">
		                <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
		                <div class="bootstrap-timepicker">
		                <f:input path="heureDebut" cssClass="form-control timepicker"/>
		                </div>
		             </div>
		             <f:errors path="heureDebut" cssClass="error"></f:errors>
                  </div>
                </div><!-- col-sm-4 -->
                 <div class="col-sm-4">
                  <div class="form-group">
                    <label class="control-label">Heure Fin</label>
                    <div class="input-group mb15">
		                <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
		                <div class="bootstrap-timepicker">
		                <f:input path="heureFin" cssClass="form-control timepicker"/>
		                </div>
		             </div>
		             <f:errors path="heureFin" cssClass="error"></f:errors>
                  </div>
                </div><!-- col-sm-4 -->
              </div><!-- row -->
              
              <div class="row mb15">
                <div class="col-sm-4">
                  <div class="form-group">
                    <label class="control-label">Centre</label>
                    <f:select path="centre" class="form-control chosen-select">
                    	<f:option value="0"> --SELECTIONNER UN CENTRE--</f:option>
					    <f:options items="${centres}" itemValue="id" itemLabel="intitule" />
					</f:select>
					<f:errors path="centre" cssClass="error"></f:errors>
                  </div>
                </div><!-- col-sm-4 -->
                <div class="col-sm-4">
                  <div class="form-group">
                    <label class="control-label">Annéee</label>
                    <f:select path="annee" class="form-control chosen-select">
                    	<f:option value="0"> --SELECTIONNER UNE ANNÉE --</f:option>
					    <f:options items="${annees}" itemValue="id" itemLabel="intitule" />
					</f:select>
					<f:errors path="annee" cssClass="error"></f:errors>
                  </div>
                </div><!-- col-sm-4 -->
                 <div class="col-sm-4">
                  <div class="form-group">
                    <label class="control-label">Formation</label>
                    <f:select path="formation" class="form-control chosen-select">
                    	<f:option value="0"> --SELECTIONNER UNE FORMATION--</f:option>
					    <f:options items="${formations}" itemValue="id" itemLabel="intitule" />
					</f:select>
					<f:errors path="formation" cssClass="error"></f:errors>
                  </div>
                </div><!-- col-sm-4 -->
              </div><!-- row -->
              
              <div class="row mb15">
                <div class="col-sm-4">
                  <div class="form-group">
                    <label class="control-label">Filière</label>
                    <f:select path="filiere" class="form-control chosen-select">
                    	<f:option value="0"> --SELECTIONNER UNE FILIÈRE--</f:option>
					    <f:options items="${filieres}" itemValue="id" itemLabel="intitule" />
					</f:select>
					<f:errors path="filiere" cssClass="error"></f:errors>
                  </div>
                </div><!-- col-sm-4 -->
                
                
                <div class="col-sm-4">
                  <div class="form-group">
                    <label class="control-label">Niveau</label>
                    <f:select path="niveau" class="form-control chosen-select">
                    	<f:option value="0"> --SELECTIONNER UN NIVEAU--</f:option>
					    <f:options items="${niveaux}" itemValue="id" itemLabel="intitule" />
					</f:select>
					<f:errors path="niveau" cssClass="error"></f:errors>
                  </div>
                </div><!-- col-sm-4 -->

                 <div class="col-sm-4">
                  <div class="form-group">
                    <label class="control-label">Semestre</label>
                    <f:select path="semestre" class="form-control chosen-select">
                    	<f:option value="0"> --SELECTIONNER UN SEMESTRE--</f:option>
					    <f:options items="${semestres}" itemValue="id" itemLabel="intitule" />
					</f:select>
					<f:errors path="semestre" cssClass="error"></f:errors>
                  </div>
                </div><!-- col-sm-4 -->
              </div><!-- row -->
              
              <div class="row mb15">
                
                  <div class="col-sm-4">
                  <div class="form-group">
                    <label class="control-label">Module</label>
                    <f:select path="module" class="form-control chosen-select">
                    	<f:option value="0"> --SELECTIONNER UN MODULE --</f:option>
					    <f:options items="${modules}" itemValue="id" itemLabel="intitule" />
					</f:select>
					<f:errors path="module" cssClass="error"></f:errors>
                  </div>
                </div><!-- col-sm-4 -->
                
                
                <div class="col-sm-4">
                  <div class="form-group">
                    <label class="control-label">Matière</label>
                    <f:select path="matiere" class="form-control chosen-select">
                    	<f:option value="0"> --SELECTIONNER UNE MATIÈRE --</f:option>
					    <f:options items="${matieres}" itemValue="id" itemLabel="intitule" />
					</f:select>
					<f:errors path="matiere" cssClass="error"></f:errors>
                  </div>
                </div><!-- col-sm-4 -->
                 <div class="col-sm-4">
                  <div class="form-group">
                    <label class="control-label">Étudiant</label>
                    <f:select path="etudiant" class="form-control chosen-select">
                    	<f:option value="0"> --SELECTIONNER UN ÉTUDIANT --</f:option>
					    <f:options items="${etudiants}" itemValue="id" itemLabel="fullName" />
					</f:select>
					<f:errors path="etudiant" cssClass="error"></f:errors>
                  </div>
                </div><!-- col-sm-4 -->
              </div><!-- row -->
              
              <div class="row mb15">
              
              
                <div class="col-sm-4">
                  <div class="form-group">
                    <label class="control-label">Type Absence</label>
                    <c:forEach items="${types_seance}" var="value">
     					<div class="radio"><label><f:radiobutton path="typeSeance" value="${value}" /> ${value}</label></div>
					</c:forEach>
					<f:errors path="typeSeance" cssClass="error"></f:errors>
                  </div>
                </div><!-- col-sm-4 -->
                
                <div class="col-sm-8">
                	<div class="row">
                		<div class="col-sm-3">
		                	<div class="form-group">
			                    <label class="control-label">Justifé?</label>
			                    <div class="radio"><label><f:radiobutton path="justifier" value="true" /> Oui</label></div>
			                    <div class="radio"><label><f:radiobutton path="justifier" value="false" /> Non</label></div>
								<f:errors path="justifier" cssClass="error"></f:errors>
		                  	</div>
                		</div>
                		<div class="col-sm-9">
                			<div class="form-group">
			                    <label class="control-label">Justification</label>
			                    <f:textarea path="justification" cssClass="form-control" rows="5"></f:textarea>
								<f:errors path="justification" cssClass="error"></f:errors>
			                  </div>
                		</div>
                	</div>
                  
                </div><!-- col-sm-4 -->
                
     
               
                 
              </div><!-- row -->

        </div>
         <div class="panel-footer">
			 <div class="row">
				<div class="col-sm-12">
				  <button type="submit" class="btn btn-primary">Valider</button>&nbsp;
				  <button type="reset" class="btn btn-default">Annuler</button>
				</div>
			 </div>
		  </div><!-- panel-footer -->
		  </f:form>
     </div>
     </div>
 
<jsp:include page="../../../views/layout/rightpanel.jsp" />
<jsp:include page="../../../views/layout/footer.jsp">
	<jsp:param name="javascripts" value="/assets/js/chosen.jquery.min.js" />
	<jsp:param name="javascripts" value="/assets/js/bootstrap-timepicker.min.js" />
</jsp:include>


<script>
  jQuery(document).ready(function() {

    jQuery(".nav-parent > a#<%= menuActuel %>").trigger("click");
    jQuery(".nav-parent > a#<%= menuActuel %>").parent("li").addClass("active");
    jQuery(".nav-parent > ul.children > li#<%= sousMenuActuel %>").addClass("active");
    
    jQuery('.datepicker-multiplee').datepicker({
        numberOfMonths: 3,
        showButtonPanel: true
      });
    
    jQuery(".chosen-select").chosen({'width':'100%','white-space':'nowrap'});
    
    
    jQuery('.timepicker').timepicker({showMeridian: false});
 

  });
</script>

</body>
</html>