<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f"%>
<%@taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>


<%! String menuActuel = "menu_etudiants";  %>
<%! String sousMenuActuel = "menu_etudiants_inscription";  %>


<jsp:include page="../../../views/layout/header.jsp" />
<jsp:include page="../../../views/layout/leftpanel.jsp" />
<jsp:include page="../../../views/layout/topmenu.jsp" />


<div class="pageheader">
      <h2><i class="fa fa-users"></i> Etudiants <span>Inscription</span></h2>
      <div class="breadcrumb-wrapper">
          <span class="label">Vous êtes ici:</span>
        <ol class="breadcrumb">
          <li><a href="<c:url value="etudiant" />">Etudiants</a></li>
          <li class="active">Inscription</li>
        </ol>
      </div>
    </div>
    
    <div class="contentpanel">
     <div class="panel panel-default">
        <div class="panel-heading">
          <div class="panel-btns">
            <a href="#" class="panel-close">&times;</a>
            <a href="#" class="minimize">&minus;</a>
          </div><!-- panel-btns -->
          <h3 class="panel-title">Ajouter</h3>
        </div>
        <f:form method="post" action="saveEtudiant" modelAttribute="etudiantForm">
			
        <div class="panel-body">
        <c:if test="${success_etudiant != null}">
							<div class="alert alert-success" role="alert">
								<strong>Well done!</strong> ${success_etudiant}
							</div>
						</c:if>
						
						<c:if test="${error_etudiant != null}">
							<div class="alert alert-danger" role="alert">
								<strong>Oh snap!</strong> ${error_etudiant}
							</div>
						</c:if>
			
						
        	 <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Prénom <span class="asterisk">*</span></label>
                    <f:input path="prenom" cssClass="form-control" size="20"/>
                    <f:errors path="prenom" cssClass="error"></f:errors>
                  </div>
                </div><!-- col-sm-6 -->
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Nom <span class="asterisk">*</span></label>
                    <f:input path="nom" cssClass="form-control" size="20"/>
                    <f:errors path="nom" cssClass="error"></f:errors>
                  </div>
                </div><!-- col-sm-6 -->
              </div><!-- row -->
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Email</label>
                    <f:input path="email" cssClass="form-control" size="20"/>
                    <f:errors path="email" cssClass="errors"></f:errors>
                  </div>
                </div><!-- col-sm-6 -->
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Téléphone</label>
                    <f:input path="telephone" cssClass="form-control" size="20"/>
                    <f:errors path="telephone" cssClass="errors"></f:errors>
                  </div>
                </div><!-- col-sm-6 -->
              </div><!-- row -->
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Date </label>
                    <f:input path="date" cssClass="form-control datepicker-multiplee"/>
                    <f:errors path="date" cssClass="errors"></f:errors>

                  </div>
                </div><!-- col-sm-6 -->
               
              </div><!-- row -->
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">CIN</label>
                    <f:input path="CIN" cssClass="form-control" size="20"/>
                    <f:errors path="CIN" cssClass="errors"></f:errors>
                  </div>
                </div><!-- col-sm-6 -->
                <div class="col-sm-6">
                
                
                
                  
                  
                 
                      <div class="form-group">
                    <label class="control-label">Centre </label>
				<f:select path="centre" cssClass="form-control chosen-select">
					   <f:option value="0" label="--- Selectionnez ---"/>
					   					   <f:options items="${liste_centres}" />
					   
					</f:select>
	
                  </div>
                  
  
                </div><!-- col-sm-6 -->
              </div><!-- row -->
              <div class="row">
                <div class="col-sm-4">
                  <div class="form-group">
                    <label class="control-label">Lieu de naissance</label>
                    <f:input path="lieu" cssClass="form-control" size="20"/>
                    <f:errors path="lieu" cssClass="errors"></f:errors>
                  </div>
                </div><!-- col-sm-4 -->
                

                   <div class="col-sm-4">
                  <div class="form-group">
                    <label class="control-label"> Utilisateur</label>
                    <f:select path="utilisateur" cssClass="form-control chosen-select">
					   <f:option value="0" label="--- Selectionnez ---"/>
						<f:options items="${liste_users}" />
					
					</f:select>
                    
                  </div>
                </div><!-- col-sm-4 -->
  
           
              </div><!-- row -->
              
        </div>
         <div class="panel-footer">
			 <div class="row">
				<div class="col-sm-12">
				  <button type="submit" class="btn btn-primary">Valider</button>&nbsp;
				  <button type="reset" class="btn btn-default">Annuler</button>
				</div>
			 </div>
		  </div><!-- panel-footer -->
		  </f:form>
     </div>
     </div>
 
<jsp:include page="../../../views/layout/rightpanel.jsp" />
<jsp:include page="../../../views/layout/footer.jsp">
	<jsp:param name="javascripts" value="/assets/js/chosen.jquery.min.js" />
</jsp:include>


<script>
  jQuery(document).ready(function() {

    jQuery(".nav-parent > a#<%= menuActuel %>").trigger("click");
    jQuery(".nav-parent > a#<%= menuActuel %>").parent("li").addClass("active");
    jQuery(".nav-parent > ul.children > li#<%= sousMenuActuel %>").addClass("active");
    
    jQuery('.datepicker-multiplee').datepicker({
        numberOfMonths: 3,
        showButtonPanel: true
      });
    
    jQuery(".chosen-select").chosen({'width':'100%','white-space':'nowrap'});
 

  });
</script>

</body>
</html>