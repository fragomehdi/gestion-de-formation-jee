<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f"%>
<%@taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>


<%! String menuActuel = "menu_etudiants";  %>
<%! String sousMenuActuel = "menu_etudiants_documents";  %>

<jsp:include page="../../layout/header.jsp" />
<jsp:include page="../../layout/leftpanel.jsp" />
<jsp:include page="../../layout/topmenu.jsp" />


<div class="pageheader">
      <h2><i class="fa fa-graduation-cap"></i> Étudiants <span>Documents</span></h2>
      <div class="breadcrumb-wrapper">
          <span class="label">Vous êtes ici:</span>
        <ol class="breadcrumb">
          <li><a href="#.">Étudiants</a></li>
          <li class="active">Documens</li>
        </ol>
      </div>
    </div>
    
    <div class="contentpanel">

			<ul class="nav nav-tabs nav-dark">
	          <li class="<c:if test="${active_diplome != null && active_diplome}">active</c:if>"><a href="#diplomes" data-toggle="tab"><strong>Diplomes</strong></a></li>
	          <li class="<c:if test="${active_attestation != null && active_attestation}">active</c:if>"><a href="#attestations" data-toggle="tab"><strong>Attestations</strong></a></li>
	        </ul>
	        <!-- Tab panes -->
        	<div class="tab-content mb30">
	          <div class="tab-pane <c:if test="${active_diplome != null && active_diplome}">active</c:if>" id="diplomes">
	            <p><a href="<c:url value="/etudiants/documents/diplomes/ajouter" />" class="btn btn-primary-alt">Ajouter</a></p>
	            
	            
	            <c:if test="${success_diplome_delete != null}">
					<div class="alert alert-success" role="alert">
						<strong>Well done!</strong> ${success_diplome_delete}
					</div>
				</c:if>
				
				<c:if test="${error_diplome_delete != null}">
					<div class="alert alert-danger" role="alert">
						<strong>Oh snap!</strong> ${error_diplome_delete}
					</div>
				</c:if>
				
				<hr />
		
				<div class="table-responsive">
	            <table class="table" id="table1">
	              <thead>
	                 <tr>
	                 	<th class="no-sort">&amp;</th>
	                 	<th class="no-sort"><span class="glyphicon glyphicon-eye-open"></span></th>
	                 	<th class="no-sort"><span class="glyphicon glyphicon-folder-close"></span></th>
	                    <th width="45%">Intitulé</th>
	                    <th width="45%">Date Création</th>
	                 </tr>
	              </thead>
	              <tbody>
	              <c:forEach var="v" items="${liste_diplomes}">
								<tr>
         							<td><a href="<c:url value="/etudiants/documents/diplomes/delete?id=${v.id}"/>" class="column-delete delete" style="  color: #D9534F;"><i class="fa fa-trash-o"></i></a> </td>
                                    <td><a href="<c:url value="/etudiants/documents/consulter?type=diplomes&amp;path=${v.path}"/>"><span class="glyphicon glyphicon-export" title="Voir le fichier"></span></a></td>
                                    <td><a href="#." class="downloadBnt" data-id="${v.id}" data-toggle="modal" data-target="#diplome-modal-search"><span class="glyphicon glyphicon-folder-open" title="Télécharger"></span></a></td>
                                    
                                    <td width="45%"><a href="<c:url value="/etudiants/documents/diplomes/modifier?id=${v.id}"/>">${v.intitule}</a></td>
                                    <td width="45%"><joda:format pattern="MM/dd/yyyy" value="${v.date_creation}" /></td>
                                 </tr>
                  </c:forEach>
	              </tbody>
              </table>
              </div>
	            
	          </div> <!-- ./ fin diplomes -->
	          
	          <div class="tab-pane <c:if test="${active_attestation != null && active_attestation}">active</c:if>" id="attestations">
	          	<p><a href="<c:url value="/etudiants/documents/attestations/ajouter" />" class="btn btn-primary-alt">Ajouter</a></p>
	            <c:if test="${success_attestation_delete != null}">
					<div class="alert alert-success" role="alert">
						<strong>Well done!</strong> ${success_attestation_delete}
					</div>
				</c:if>
				
				<c:if test="${error_attestation_delete != null}">
					<div class="alert alert-danger" role="alert">
						<strong>Oh snap!</strong> ${error_attestation_delete}
					</div>
				</c:if>
				
				<hr />
	          
	          	<div class="table-responsive">
	            <table class="table" id="table2">
	              <thead>
	                 <tr>
	                 	<th class="no-sort">&amp;</th>
	                 	<th class="no-sort"><span class="glyphicon glyphicon-eye-open"></span></th>
	                 	<th class="no-sort"><span class="glyphicon glyphicon-folder-close"></span></th>
	                    <th width="45%">Intitulé</th>
	                    <th width="45%">Date Création</th>
	                 </tr>
	              </thead>
	              <tbody>
	              <c:forEach var="v" items="${liste_attestations}">
								<tr>
         							<td><a href="<c:url value="/etudiants/documents/attestations/delete?id=${v.id}"/>" class="column-delete delete" style="  color: #D9534F;"><i class="fa fa-trash-o"></i></a> </td>
                                    <td><a href="<c:url value="/etudiants/documents/consulter?type=attestations&amp;path=${v.path}"/>"><span class="glyphicon glyphicon-export" title="Télécharger"></span></a></td>
                                     <td><a href="#." class="downloadBnt2" data-id="${v.id}" data-toggle="modal" data-target="#attestation-modal-search"><span class="glyphicon glyphicon-folder-open" title="Télécharger"></span></a></td>
                                    
                                    <td width="45%"><a href="<c:url value="/etudiants/documents/attestations/modifier?id=${v.id}"/>">${v.intitule}</a></td>
                                    <td width="45%"><joda:format pattern="MM/dd/yyyy" value="${v.date_creation}" /></td>
                                 </tr>
                  </c:forEach>
	              </tbody>
              </table>
              </div>
	          
	          </div> <!-- ./ fin attestations  -->
	          </div>
	</div>
	
	
<div class="modal fade" id="diplome-modal-search" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
        <form action="<c:url value="/etudiants/documents/diplomes/download"/>" method="post" id="diplome-form-search">
        	<input type="hidden" name="diplome_id" id="diplome_id" value="0" />
	        <div class="panel panel-dark panel-alt">
			    <div class="panel-heading">
			        <div class="panel-btns">
			            <a  aria-hidden="true" data-dismiss="modal" class="close">&times;</a>
			        </div><!-- panel-btns -->
			        <h3 class="panel-title">Télécharger</h3>
			        <p>Télécharger le diplome pour un étudiant</p>
			    </div>
			    <div class="panel-body">
			         <div class="form-group">
			         		<label class="col-sm-3 control-label">Étudiant</label>
			              <div class="col-sm-9">
			               		<select name="etudiant_id" class="form-control" id="">
			               			<option value=""> -- SELECTIONNER UN ETUDIANT -- </option>
			               			<c:forEach var="e" items="${liste_etudiants}">
			               				<option value="${e.id}">${e.nom} ${e.prenom}</option>
			               			</c:forEach>
			               			
			               		</select>
			              </div>
			            </div>
			    </div>
			    <div class="panel-footer text-right">
			    	<button type="submit" class="btn btn-success submit-justification-form"> <span class="glyphicon glyphicon-floppy-save"></span> Télécharger</button>
			    	<button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
			    </div>
			</div>
        </form>
        
    </div>
    
  </div>
</div>
        
        
<div class="modal fade" id="attestation-modal-search" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
        <form action="<c:url value="/etudiants/documents/attestations/download"/>" method="post" id="attestation-form-search">
        	<input type="hidden" name="attestation_id" id="attestation_id" value="0" />
	        <div class="panel panel-dark panel-alt">
			    <div class="panel-heading">
			        <div class="panel-btns">
			            <a  aria-hidden="true" data-dismiss="modal" class="close">&times;</a>
			        </div><!-- panel-btns -->
			        <h3 class="panel-title">Télécharger</h3>
			        <p>Télécharger l'attestation pour un étudiant</p>
			    </div>
			    <div class="panel-body">
			         <div class="form-group">
			         		<label class="col-sm-3 control-label">Étudiant</label>
			              <div class="col-sm-9">
			               		<select name="etudiant_id" class="form-control" id="">
			               			<option value=""> -- SELECTIONNER UN ETUDIANT -- </option>
			               			<c:forEach var="e" items="${liste_etudiants}">
			               				<option value="${e.id}">${e.nom} ${e.prenom}</option>
			               			</c:forEach>
			               			
			               		</select>
			              </div>
			            </div>
			    </div>
			    <div class="panel-footer text-right">
			    	<button type="submit" class="btn btn-success submit-justification-form"> <span class="glyphicon glyphicon-floppy-save"></span> Télécharger</button>
			    	<button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
			    </div>
			</div>
        </form>
        
    </div>
    
  </div>
</div>

        
<jsp:include page="../../layout/rightpanel.jsp" />
<jsp:include page="../../layout/footer.jsp" />
<script src="<c:url value="/assets/js/chosen.jquery.min.js" />"></script>

<script>

jQuery(document).ready(function() {
	
    jQuery(".nav-parent > a#<%= menuActuel %>").trigger("click");
    jQuery(".nav-parent > a#<%= menuActuel %>").parent("li").addClass("active");
    jQuery(".nav-parent > ul.children > li#<%= sousMenuActuel %>").addClass("active");
    
    jQuery('#table1, #table2').dataTable({
        "sPaginationType": "full_numbers",
        "aoColumnDefs" : [ {
    	    "bSortable" : false,
    	    "aTargets" : [ "no-sort" ]
    	} ]
      });
    
    // Chosen Select
    jQuery("select").not(".form-control").chosen({
      'min-width': '100px',
      'white-space': 'nowrap',
      disable_search_threshold: 10
    });
    
    jQuery("a.delete").click(function(e){
	  	  e.preventDefault();
	  	  var url = $(this).attr("href");
	      swal(
	      {
	          title: "Êtes-vous sure?",
	          text: "Vous ne serez pas en mesure de récupérer cet élément",
	          type: "warning",
	          showCancelButton: true,
	          confirmButtonColor: "#DD6B55",
	          confirmButtonText: "Oui, supprimez-le!",
	          cancelButtonText: "Non, annuler!",
	          closeOnConfirm: false,
	          closeOnCancel: false
	      },
	      function(isConfirm)
	      {
	          if (isConfirm) {
	          	window.location = url;
	          	swal("Suppression!", "L'élement va être supprimé dans quelques instants.", "success");
	          }
	          else {
	              swal("Annulé", "Aucune opération n'a été effectuer", "error");
	          }
	      });
	      
	      return false;
	    });
    
    jQuery("a.downloadBnt").click(function(e){
    	e.preventDefault();
    	var id = $(this).data("id");
    	jQuery("#diplome-form-search #diplome_id").val(id);
    });
    
    jQuery("a.downloadBnt2").click(function(e){
    	e.preventDefault();
    	var id = $(this).data("id");
    	jQuery("#attestation-form-search #attestation_id").val(id);
    });

  });

</script>

</body>
</html>