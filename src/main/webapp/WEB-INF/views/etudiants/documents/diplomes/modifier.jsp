<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f"%>

<%! String menuActuel = "menu_etudiants";  %>
<%! String sousMenuActuel = "menu_etudiants_documents";  %>

<jsp:include page="../../../layout/header.jsp" />
<jsp:include page="../../../layout/leftpanel.jsp" />
<jsp:include page="../../../layout/topmenu.jsp" />



<div class="pageheader">
      <h2><i class="fa fa-graduation-cap"></i> Étudiants <span>Documents</span></h2>
      <div class="breadcrumb-wrapper">
          <span class="label">Vous êtes ici:</span>
        <ol class="breadcrumb">
          <li><a href="#.">Étudiants</a></li>
          <li class="active">Documens</li>
        </ol>
      </div>
    </div>
    
    <div class="contentpanel">
     <div class="panel panel-default">
        <div class="panel-heading">
          <div class="panel-btns">
            <a href="#" class="panel-close">&times;</a>
            <a href="#" class="minimize">&minus;</a>
          </div><!-- panel-btns -->
          <h3 class="panel-title">Modification</h3>
        </div>
        
        
        
      
        <form method="post" class="form-horizontal" action="update">
        <input type="hidden" name="id" value="${diplome.id}" />
			
			<div class="panel-body">
			
				<c:if test="${success != null}">
					<div class="alert alert-success" role="alert">
						<strong>Well done!</strong> ${success}
					</div>
				</c:if>
		
				<c:if test="${error != null}">
					<div class="alert alert-danger" role="alert">
						<strong>Oh snap!</strong> ${error}
					</div>
				</c:if>
              
                <div class="form-group">
                  <label class="col-sm-3 control-label">Intitulé</label>
                  <div class="col-sm-4">
                		<input type="text" name="intitule" value="${diplome.intitule}" class="form-control" required size="20"/>
                  </div>
                </div>
                
                <div class="form-group">
                  <label class="col-sm-3 control-label">Correspondance des champs</label>
                  <div class="col-sm-8">
                  		<c:forEach var="v" items="${diplome.fields}">
	                  		<div class="row">
	                  			<div class="col-sm-2">
	                  				${v.field}
	                  			</div>
	                  			<div class="col-sm-6">
	                  				<select name="values[]" id="">
	                  					<option value="" <c:if test="${v.value == ''}">selected</c:if>> -- Selectioner une valeur -- </option>
	                  					<option value="date" <c:if test="${v.value == 'date'}">selected</c:if>> Date </option>
	                  					<option value="nom_etudiant" <c:if test="${v.value == 'nom_etudiant'}">selected</c:if>> Nom d'etudiant </option>
	                  					<option value="prenom_etudiant" <c:if test="${v.value == 'prenom_etudiant'}">selected</c:if>> Prénom d'etudiant </option>
	                  					<option value="fullname_etudiant" <c:if test="${v.value == 'fullname_etudiant'}">selected</c:if>> Nom &amp; Prénom d'etudiant </option>
	                  				</select>
	                  			</div>
	                  		</div><br />
                 		</c:forEach>
                  </div>
                </div>
                
                
              </div><!-- panel-body -->
              <div class="panel-footer">
                <button type="submit" class="btn btn-primary">Valider</button>
            <button type="reset" class="btn btn-default">Annuler</button>
              </div><!-- panel-footer -->
		
        
	
		  </form>
     </div>
 </div>
<jsp:include page="../../../layout/rightpanel.jsp" />
<jsp:include page="../../../layout/footer.jsp" />
<script src="<c:url value="/assets/js/chosen.jquery.min.js" />"></script>


<script>
  jQuery(document).ready(function($) {

    jQuery(".nav-parent > a#<%= menuActuel %>").trigger("click");
    jQuery(".nav-parent > a#<%= menuActuel %>").parent("li").addClass("active");
    jQuery(".nav-parent > ul.children > li#<%= sousMenuActuel %>").addClass("active");
    
    
 // Chosen Select
    jQuery("select").chosen({
      'min-width': '100px',
      'white-space': 'nowrap',
      disable_search_threshold: 10
    });
    
    
  });
  
  
</script>


</body>
</html>