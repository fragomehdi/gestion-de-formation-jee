package com.app.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javassist.NotFoundException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.app.model.Enseignant;
import com.app.model.Matiere;
import com.app.model.User;
import com.app.propertyeditors.UserEditor;
import com.app.service.EnseignantService;
import com.app.service.LogTrackerService;
import com.app.service.MatiereService;
import com.app.service.UserService;

@Controller
public class EnseignantController {
	@Autowired
	LogTrackerService logSvc;
	@Autowired
	EnseignantService enseignantService;
	@Autowired
	MatiereService matieretService;
	@Autowired
	UserService userService;
	private	String [] validExtensions = {"jpg","png","jpeg"};

	@InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(User.class, new UserEditor());
    }
	
	
	 private static String getFileExtension(String fileName) {
	        if(fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0)
	        return fileName.substring(fileName.lastIndexOf(".")+1);
	        else return "";
	    }
	 
	 public static boolean useList(String[] arr, String ext) {
			return Arrays.asList(arr).contains(ext);
		}
	 
		@RequestMapping(value="/employees/enseignant/dowload")
		public void getLogFile(HttpSession session,HttpServletResponse response,
				@RequestParam(value = "file", required = true, defaultValue = "0") String fileName

				) throws Exception {
		    try {
		    	 String rootPath = System.getProperty("catalina.base");
		            File dir = new File(rootPath + File.separator + "uploads/emploie");
		    	
		        String filePathToBeServed = dir.getAbsolutePath()+ File.separator + fileName;
		        File fileToDownload = new File(filePathToBeServed);
		        InputStream inputStream = new FileInputStream(fileToDownload);
		        
		        response.setContentType("application/force-download");
		        response.setHeader("Content-Disposition", "attachment; filename="+fileName); 
		        IOUtils.copy(inputStream, response.getOutputStream());
		        response.flushBuffer();
		        inputStream.close();
		    } catch (Exception e){
		        e.printStackTrace();
		    }

		}
	
	@PreAuthorize("hasRole('ENSEIGNANT_READ')")
	@RequestMapping(value = "/employees/enseignant", method = RequestMethod.GET)
	public String index(Model model) 
	{
		model.addAttribute("liste", enseignantService.getAll());
		return "employees/enseignant/liste";
	}
	
	@PreAuthorize("hasRole('ENSEIGNANT_EDIT')")
	@RequestMapping(value = "/employees/enseignant/ajouter", method = RequestMethod.GET)
	public String ajouter(Model model) 
	{
	
		model.addAttribute("personnelForm", new Enseignant());
		Map<Integer,String> paiementList = new LinkedHashMap<Integer,String>();
		paiementList.put(1, "Chèque de paiement");
		paiementList.put(2, "Chèque visé");
		paiementList.put(3, "Chèque certifié");
		paiementList.put(4, "Chèque de banque");
		paiementList.put(5, "Espèce");
		paiementList.put(6, "Carte bancaire");
		model.addAttribute("paiementList", paiementList);
		
		Map<String,String> sexeList = new LinkedHashMap<String,String>();
		sexeList.put("Femme", "Femme");
		sexeList.put("Homme", "Homme");
		model.addAttribute("sexeList", sexeList);
		
		model.addAttribute("utilisateurs", userService.getAll()); // Change this to getAvailableUsers()
		
		return "employees/enseignant/ajouter";
	}
	
	@PreAuthorize("hasRole('ENSEIGNANT_EDIT')")
	@RequestMapping(value = "/employees/enseignant/savePersonnel", method = RequestMethod.POST)
	public String savePersonnel(
			@Valid @ModelAttribute("personnelForm") Enseignant p,
			BindingResult bindingResult,
			Model model,
			final RedirectAttributes redirectAttributes,
			HttpServletRequest request,
			@RequestParam("file") MultipartFile file
			) throws Exception {
		if (bindingResult.hasErrors()) {
			System.out.println("Erreur");
			System.out.println(bindingResult.toString());
			model.addAttribute("personnelForm", p);
			Map<Integer,String> paiementList = new LinkedHashMap<Integer,String>();
			paiementList.put(1, "Chèque de paiement");
			paiementList.put(2, "Chèque visé");
			paiementList.put(3, "Chèque certifié");
			paiementList.put(4, "Chèque de banque");
			paiementList.put(5, "Espèce");
			paiementList.put(6, "Carte bancaire");
			model.addAttribute("paiementList", paiementList);
			
			Map<String,String> sexeList = new LinkedHashMap<String,String>();
			sexeList.put("Femme", "Femme");
			sexeList.put("Homme", "Homme");
			model.addAttribute("sexeList", sexeList);
			model.addAttribute("utilisateurs", userService.getAll());
			
			
			return "employees/enseignant/ajouter";
		} else {
			

			
			if (file.isEmpty()) {
				redirectAttributes.addFlashAttribute("error", "Veuilez charger un fichier avec l'une des extensions : doc,docx,pdf,jpg,png,jpeg");
				return "redirect:/etudiants/emploie/ajouter";
			}
			else
			{
					// Creation des dossiers si inexsistant
		            String rootPath = System.getProperty("catalina.base");
		            File dir = new File(rootPath + File.separator + "uploads/enseignant");
		            if (!dir.exists())
		                dir.mkdirs();
		            /*System.out.println("*****************************************************");
		            System.out.println(rootPath + File.separator + "uploads/emploie");*/
		            
					// Nom aleatoire
					SecureRandom random = new SecureRandom();
					String filename = new BigInteger(130, random).toString(32)+"."+getFileExtension(file.getOriginalFilename());
					//System.out.println("AAAAAAAAAAAAAAAA"+getFileExtension(file.getOriginalFilename()));
					Enseignant dpUpdated = null;
					String extension = getFileExtension(file.getOriginalFilename());

					if(useList(validExtensions,extension)){
					try {
						// MD5 FILE
		                byte[] bytes = file.getBytes();
		                BufferedOutputStream stream =
		                        new BufferedOutputStream(new FileOutputStream(new File(dir.getAbsolutePath()
		                                + File.separator + filename)));
		                stream.write(bytes);
		                stream.close();
		                
		            p.setImage(filename);
		                
		                enseignantService.add(p);
		    			logSvc.store("Ajout Enseignant: "+p, request);
		    			redirectAttributes.addFlashAttribute("success_personnel", p.getPrenom() + " a ete bien ajouter");
		    			return "redirect:/employees/enseignant/ajouter";
		                
					  } catch (Exception e) {
			            	redirectAttributes.addFlashAttribute("error", "Erreur:" + e.getMessage());
			    			return "redirect:/employees/enseignant/ajouter";
			            }
						
					//	System.out.println(dpUpdated.getNom()+" Created! ");
						   
				}
	   
	          
				
				
				else
				{
					redirectAttributes.addFlashAttribute("error", "FORMAT du fichier non reconnue ! " );
	    			return "redirect:/employees/enseignant/ajouter";
				}
				}
				
		}
	}
	@PreAuthorize("hasRole('ENSEIGNANT_EDIT')")
	@RequestMapping(value = "/employees/enseignant/editPerson", method = RequestMethod.GET)
	public String editPerson(
			@RequestParam(value = "id", required = true, defaultValue = "0") int id,
			Model model
			) throws NotFoundException {
		Enseignant p = enseignantService.findById(id);
		System.out.println("Editing "+p);
		model.addAttribute("personnelForm", p);
		Map<Integer,String> paiementList = new LinkedHashMap<Integer,String>();
		paiementList.put(1, "Chèque de paiement");
		paiementList.put(2, "Chèque visé");
		paiementList.put(3, "Chèque certifié");
		paiementList.put(4, "Chèque de banque");
		paiementList.put(5, "Espèce");
		paiementList.put(6, "Carte bancaire");
		model.addAttribute("paiementList", paiementList);
		
		Map<String,String> sexeList = new LinkedHashMap<String,String>();
		sexeList.put("Femme", "Femme");
		sexeList.put("Homme", "Homme");
		model.addAttribute("sexeList", sexeList);
		model.addAttribute("utilisateurs", userService.getAll());

		List<Matiere> liste = matieretService.findByMatiere(p);

		model.addAttribute("liste_matieres", liste);

		return "employees/enseignant/modifier";
	}
	
	@PreAuthorize("hasRole('ENSEIGNANT_EDIT')")
	@RequestMapping(value = "/employees/enseignant/updatePersonnel", method = RequestMethod.POST)
	public String updatePersonnel(
			@Valid @ModelAttribute("personnelForm") Enseignant p,
			BindingResult bindingResult,
			Model model,
			final RedirectAttributes redirectAttributes,
			HttpServletRequest request
			) throws Exception {
		if (bindingResult.hasErrors()) {
			//System.out.println("Erreur");
			System.out.println(bindingResult.toString());
			model.addAttribute("personnelForm", p);
			Map<Integer,String> paiementList = new LinkedHashMap<Integer,String>();
			paiementList.put(1, "Chèque de paiement");
			paiementList.put(2, "Chèque visé");
			paiementList.put(3, "Chèque certifié");
			paiementList.put(4, "Chèque de banque");
			paiementList.put(5, "Espèce");
			paiementList.put(6, "Carte bancaire");
			model.addAttribute("paiementList", paiementList);
			
			Map<String,String> sexeList = new LinkedHashMap<String,String>();
			sexeList.put("Femme", "Femme");
			sexeList.put("Homme", "Homme");
			model.addAttribute("sexeList", sexeList);
			
			model.addAttribute("utilisateurs", userService.getAll());
			
			return "employees/enseignant/modifier";
		} else {
			
			try {
				enseignantService.update(p);
				logSvc.store("Mise a jour Enseignant: "+p, request);
				redirectAttributes.addFlashAttribute("success_personnel", p.getPrenom() + " a ete bien modifier");
				return "redirect:/employees/enseignant/editPerson?id="+p.getId();
			} catch (NotFoundException e) {
				redirectAttributes.addFlashAttribute("error_personnel", "Entity not found");
				return "redirect:/employees/enseignant/editPerson?id="+p.getId();
			}
			
		}
	}
	
	@PreAuthorize("hasRole('ENSEIGNANT_EDIT')")
	@RequestMapping(value = "/employees/enseignant/deletePerson", method = RequestMethod.GET)
	public String deleteVille(
			@RequestParam(value = "id", required = true, defaultValue = "0") int id,
			final RedirectAttributes redirectAttributes,
			HttpServletRequest request
			) {
		try {
			Enseignant p = enseignantService.findById(id);
			enseignantService.delete(id);
			logSvc.store("Suppression enseignant: "+p, request);

			redirectAttributes.addFlashAttribute("success_person_delete", p.getPrenom()
					+ " a ete supprimer");

			return "redirect:/employees/enseignant";
		} catch (NotFoundException e) {
			redirectAttributes.addFlashAttribute("error_person_delete", "Entity not found");
			return "redirect:/employees/enseignant";
		}
		
	}
	
	
	
	
	
	
	
	@PreAuthorize("hasRole('ENSEIGNANT_EDIT')")
	@RequestMapping(value = "/employees/enseignant/listeMatiere", method = RequestMethod.GET)
	public String listeMatiere(
			@RequestParam(value = "id", required = true, defaultValue = "0") int id,
			final RedirectAttributes redirectAttributes,
			HttpServletRequest request
			) {
		try {
			Enseignant ens = new Enseignant();
			ens = enseignantService.findById(id);
			List<Matiere> liste = matieretService.findByMatiere(ens);
			
			System.out.println("********************************** LIST :  *******************************"+liste);
			System.out.println("******************************** **********************"+ens);

			

			return "redirect:/employees/enseignant";
		} catch (NotFoundException e) {
			redirectAttributes.addFlashAttribute("error_person_delete", "Entity not found");
			return "redirect:/employees/enseignant";
		}
		
	}
	
	
	@RequestMapping(value = "/professeurs/consulter", method = RequestMethod.GET)
	public @ResponseBody  void consulter(
			@RequestParam(value = "path", required = true, defaultValue = "") String path,
			HttpServletResponse response
			) {
		
		String rootPath = System.getProperty("catalina.base");
		File dir = new File(rootPath + File.separator + "uploads/enseignant");;
		
		
		 try
	        {
	            // get your file as InputStream
			 System.out.println(dir.getAbsolutePath()
				        + File.separator + path);
	            InputStream is = new FileInputStream(dir.getAbsolutePath()
				        + File.separator + path);
	            // copy it to response's OutputStream
	            IOUtils.copy( is, response.getOutputStream() );
	            response.flushBuffer();
	        }
	        catch( IOException ex )
	        {
	            throw new RuntimeException( "IOError writing file to output stream" );
	        }
	
	}
	
	

}
