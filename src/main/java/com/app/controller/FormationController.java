package com.app.controller;

import javassist.NotFoundException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.app.model.Annee;
import com.app.model.Centre;
import com.app.model.Formation;
import com.app.propertyeditors.AnneeEditor;
import com.app.propertyeditors.CentreEditor;
import com.app.service.AnneeService;
import com.app.service.CentreService;
import com.app.service.FormationService;
import com.app.service.LogTrackerService;

@Controller
public class FormationController {
	@Autowired
	LogTrackerService logSvc;
	@Autowired
	FormationService formationSvc;
	@Autowired
	AnneeService anneeService;
	@Autowired
	CentreService centreService;
	
	
	@InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Annee.class, new AnneeEditor());
        binder.registerCustomEditor(Centre.class, new CentreEditor());
    }
	
	@PreAuthorize("hasRole('FORMATION_READ')")
	@RequestMapping(value = "/parametrage/formations", method = RequestMethod.GET)
	public String index(Model model) {
		model.addAttribute("mode", "create");
		model.addAttribute("formationForm", new Formation());
		model.addAttribute("liste_formations", formationSvc.getAll());
		model.addAttribute("centres", centreService.getAll());
		model.addAttribute("annees", anneeService.getAll());
		

		
		return "parametrage/formations";
	}
	
	@PreAuthorize("hasRole('FORMATION_EDIT')")
	@RequestMapping(value = "/parametrage/saveFormation", method = RequestMethod.POST)
	public String savePerson(
			@Valid @ModelAttribute("formationForm") Formation v,
			BindingResult bindingResult,HttpServletRequest request,
			@RequestParam(value = "mode", required = true, defaultValue = "create") String mode,
			Model model,
			final RedirectAttributes redirectAttributes
			) throws Exception {
		if (bindingResult.hasErrors()) {
			System.out.println(bindingResult.toString());
			model.addAttribute("formationForm", new Formation());
			model.addAttribute("liste_formations", formationSvc.getAll());
			model.addAttribute("centres", centreService.getAll());
			model.addAttribute("annees", anneeService.getAll());
			return "parametrage/formations";
		} else {
			
			if(mode.equals("create"))
			{
				formationSvc.add(v);
				logSvc.store("Ajout Formation: "+v, request);
				redirectAttributes.addFlashAttribute("success_formation", v.getIntitule() + " a ete bien ajouter");
				return "redirect:/parametrage/formations";
			}
			else{
				System.out.println("Updating "+v);
				try {
					formationSvc.update(v);
					logSvc.store("Mise a jour  Formation: "+v, request);
					redirectAttributes.addFlashAttribute("success_formation", v.getIntitule() + " a ete bien modifier");
					return "redirect:/parametrage/formations";
				} catch (NotFoundException e) {
					redirectAttributes.addFlashAttribute("error_formation", "Entity not found");
					return "redirect:/parametrage/formations";
				}
				
			}
			
		}
	}
	

	
	@PreAuthorize("hasRole('FORMATION_EDIT')")
	@RequestMapping(value = "/parametrage/editFormation", method = RequestMethod.GET)
	public String editFormation(
			@RequestParam(value = "id", required = true, defaultValue = "0") int id,
			Model model
			) {
		Formation p = formationSvc.findById(id);
		System.out.println("Editing "+p);
		model.addAttribute("mode", "edit");
		model.addAttribute("formationForm", p);
		model.addAttribute("/*Form", new Formation());
		
		
		model.addAttribute("liste_formations", formationSvc.getAll());
		model.addAttribute("centres", centreService.getAll());
		model.addAttribute("annees", anneeService.getAll());
		return "parametrage/formations";
	}
	

	@PreAuthorize("hasRole('FORMATION_EDIT')")
	@RequestMapping(value = "/parametrage/deleteFormation", method = RequestMethod.GET)
	public String deleteFormation(
			@RequestParam(value = "id", required = true, defaultValue = "0") int id,
			final RedirectAttributes redirectAttributes,HttpServletRequest request) {
		try {
			Formation p = formationSvc.findById(id);
			formationSvc.delete(id);
			logSvc.store("Suppression Formation: "+p, request);
			redirectAttributes.addFlashAttribute("success_formation_delete", p.getIntitule()
					+ " a ete supprimer");

			return "redirect:/parametrage/formations";
		} catch (NotFoundException e) {
			redirectAttributes.addFlashAttribute("error_formation_delete", "Entity not found");
			return "redirect:/parametrage/formations";
		}
		
	}
	
	

	
}
