package com.app.controller;

import java.util.LinkedHashMap;
import java.util.Map;

import javassist.NotFoundException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.app.model.Personnel;
import com.app.model.User;
import com.app.propertyeditors.UserEditor;
import com.app.service.LogTrackerService;
import com.app.service.PersonnelService;
import com.app.service.UserService;

@Controller
public class PersonnelController {
	@Autowired
	LogTrackerService logSvc;
	@Autowired
	PersonnelService personnelService;
	@Autowired
	UserService userService;
	
	@InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(User.class, new UserEditor());
    }
	
	
	@PreAuthorize("hasRole('PERSONNEL_READ')")
	@RequestMapping(value = "/employees/personnel", method = RequestMethod.GET)
	public String index(Model model) 
	{
		model.addAttribute("liste", personnelService.getAll());
		return "employees/personnel/liste";
	}
	
	@PreAuthorize("hasRole('PERSONNEL_EDIT')")
	@RequestMapping(value = "/employees/personnel/ajouter", method = RequestMethod.GET)
	public String ajouter(Model model) 
	{
	
		model.addAttribute("personnelForm", new Personnel());
		Map<Integer,String> paiementList = new LinkedHashMap<Integer,String>();
		paiementList.put(1, "Chèque de paiement");
		paiementList.put(2, "Chèque visé");
		paiementList.put(3, "Chèque certifié");
		paiementList.put(4, "Chèque de banque");
		paiementList.put(5, "Espèce");
		paiementList.put(6, "Carte bancaire");
		model.addAttribute("paiementList", paiementList);
		
		Map<String,String> sexeList = new LinkedHashMap<String,String>();
		sexeList.put("Femme", "Femme");
		sexeList.put("Homme", "Homme");
		model.addAttribute("sexeList", sexeList);
		
		model.addAttribute("utilisateurs", userService.getAll());
		
		return "employees/personnel/ajouter";
	}
	
	@PreAuthorize("hasRole('PERSONNEL_EDIT')")
	@RequestMapping(value = "/employees/personnel/savePersonnel", method = RequestMethod.POST)
	public String savePersonnel(
			@Valid @ModelAttribute("personnelForm") Personnel p,
			BindingResult bindingResult,
			Model model,
			final RedirectAttributes redirectAttributes,
			HttpServletRequest request
			) throws Exception {
		if (bindingResult.hasErrors()) {
			//System.out.println("Erreur");
			//System.out.println(bindingResult.toString());
			model.addAttribute("personnelForm", p);
			Map<Integer,String> paiementList = new LinkedHashMap<Integer,String>();
			paiementList.put(1, "Chèque de paiement");
			paiementList.put(2, "Chèque visé");
			paiementList.put(3, "Chèque certifié");
			paiementList.put(4, "Chèque de banque");
			paiementList.put(5, "Espèce");
			paiementList.put(6, "Carte bancaire");
			model.addAttribute("paiementList", paiementList);
			
			Map<String,String> sexeList = new LinkedHashMap<String,String>();
			sexeList.put("Femme", "Femme");
			sexeList.put("Homme", "Homme");
			model.addAttribute("sexeList", sexeList);
			
			model.addAttribute("utilisateurs", userService.getAll());
			
			
			return "employees/personnel/ajouter";
		} else {
			
			personnelService.add(p);
			logSvc.store("Ajout Personnel: "+p, request);
			redirectAttributes.addFlashAttribute("success_personnel", p.getPrenom() + " a ete bien ajouter");
			return "redirect:/employees/personnel/ajouter";

		}
	}
	
	@RequestMapping(value = "/employees/personnel/editPerson", method = RequestMethod.GET)
	public String editPerson(
			@RequestParam(value = "id", required = true, defaultValue = "0") int id,
			Model model
			) {
		Personnel p = personnelService.findById(id);
		System.out.println("Editing "+p);
		model.addAttribute("personnelForm", p);
		Map<Integer,String> paiementList = new LinkedHashMap<Integer,String>();
		paiementList.put(1, "Chèque de paiement");
		paiementList.put(2, "Chèque visé");
		paiementList.put(3, "Chèque certifié");
		paiementList.put(4, "Chèque de banque");
		paiementList.put(5, "Espèce");
		paiementList.put(6, "Carte bancaire");
		model.addAttribute("paiementList", paiementList);
		
		Map<String,String> sexeList = new LinkedHashMap<String,String>();
		sexeList.put("Femme", "Femme");
		sexeList.put("Homme", "Homme");
		model.addAttribute("sexeList", sexeList);
		
		model.addAttribute("utilisateurs", userService.getAll());

		return "employees/personnel/modifier";
	}
	
	@PreAuthorize("hasRole('PERSONNEL_EDIT')")
	@RequestMapping(value = "/employees/personnel/updatePersonnel", method = RequestMethod.POST)
	public String updatePersonnel(
			@Valid @ModelAttribute("personnelForm") Personnel p,
			BindingResult bindingResult,
			Model model,
			final RedirectAttributes redirectAttributes,
			HttpServletRequest request
			) throws Exception {
		if (bindingResult.hasErrors()) {
			//System.out.println("Erreur");
			System.out.println(bindingResult.toString());
			model.addAttribute("personnelForm", p);
			Map<Integer,String> paiementList = new LinkedHashMap<Integer,String>();
			paiementList.put(1, "Chèque de paiement");
			paiementList.put(2, "Chèque visé");
			paiementList.put(3, "Chèque certifié");
			paiementList.put(4, "Chèque de banque");
			paiementList.put(5, "Espèce");
			paiementList.put(6, "Carte bancaire");
			model.addAttribute("paiementList", paiementList);
			
			
			Map<String,String> sexeList = new LinkedHashMap<String,String>();
			sexeList.put("Femme", "Femme");
			sexeList.put("Homme", "Homme");
			model.addAttribute("sexeList", sexeList);
			
			model.addAttribute("utilisateurs", userService.getAll());
			
			return "employees/personnel/modifier";
		} else {
			
			try {
				personnelService.update(p);
				logSvc.store("Mise a jour Personnel: "+p, request);
				redirectAttributes.addFlashAttribute("success_personnel", p.getPrenom() + " a ete bien modifier");
				return "redirect:/employees/personnel/editPerson?id="+p.getId();
			} catch (NotFoundException e) {
				redirectAttributes.addFlashAttribute("error_personnel", "Entity not found");
				return "redirect:/employees/personnel/editPerson?id="+p.getId();
			}
			


		}
	}
	
	@PreAuthorize("hasRole('PERSONNEL_EDIT')")
	@RequestMapping(value = "/employees/personnel/deletePerson", method = RequestMethod.GET)
	public String deleteVille(
			@RequestParam(value = "id", required = true, defaultValue = "0") int id,
			final RedirectAttributes redirectAttributes,
			HttpServletRequest request
			) {
		try {
			Personnel p = personnelService.findById(id);
			personnelService.delete(id);
			logSvc.store("Suppression personnel: "+p, request);

			redirectAttributes.addFlashAttribute("success_person_delete", p.getPrenom()
					+ " a ete supprimer");

			return "redirect:/employees/personnel";
		} catch (NotFoundException e) {
			redirectAttributes.addFlashAttribute("error_person_delete", "Entity not found");
			return "redirect:/employees/personnel";
		}
		
	}
	

}
