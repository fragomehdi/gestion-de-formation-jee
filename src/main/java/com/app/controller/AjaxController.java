package com.app.controller;

import java.util.ArrayList;
import java.util.List;

import javassist.NotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.app.json.JEtudiantNote;
import com.app.json.JResponse;
import com.app.model.*;
import com.app.service.*;

@Controller
public class AjaxController {
	@Autowired
	MessageService messageService;
	@Autowired
	UserService userService;
	@Autowired
	AnneeService anneeService;
	@Autowired
	FormationService formationService;
	@Autowired
	FiliereService filiereService;
	@Autowired
	NiveauxService niveauxService;
	@Autowired
	SemestreService semestreService;
	@Autowired
	ModuleService moduleService;
	@Autowired
	MatiereService matiereService;
	@Autowired
	ControleService controleService;
	@Autowired
	EtudiantService etudiantService;
	@Autowired
	Controle_noteService controle_noteSvc;
	
	@RequestMapping(value = "/data/get_total_messages", method = RequestMethod.GET)
	public @ResponseBody JResponse getTotalMessages() {
		UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal(); 
		try {
			User u = userService.findByUsername(userDetails.getUsername());
			if(u != null)
				return new JResponse("200",""+messageService.count(u));
			else
				return new JResponse("504", "user not found");
		} catch (NotFoundException e) {
			return new JResponse("504", "user not found");
		}
		

	}
	
	@RequestMapping(value = "/data/get_new_messages", method = RequestMethod.GET)
	public @ResponseBody List<Message> getNewMessages() {
		UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal(); 
		try {
			User u = userService.findByUsername(userDetails.getUsername());
			return messageService.getLast(u);
		} catch (NotFoundException e) {
			List<Message> m = new ArrayList<Message>();
			return m;
		}

	}
	
	@RequestMapping(value = "/data/getAnnee", method = RequestMethod.GET)
	public @ResponseBody List<Annee> getAnnees(@RequestParam(value = "centre_id", required = true, defaultValue = "0") int centre_id ) {
		Centre c = new Centre();
		c.setId(centre_id);
		return anneeService.findByCentre(c);
	}
	
	@RequestMapping(value = "/data/getFormation", method = RequestMethod.GET)
	public @ResponseBody List<Formation> getFormation(
			@RequestParam(value = "centre_id", required = true, defaultValue = "0") int centre_id,
			@RequestParam(value = "annee_id", required = true, defaultValue = "0") int annee_id
		) {
		Centre c = new Centre();
		c.setId(centre_id);
		
		Annee a = new Annee();
		a.setId(annee_id);
		
		return formationService.findByCentreAnnee(c,a);
	}
	
	
	@RequestMapping(value = "/data/getFiliere", method = RequestMethod.GET)
	public @ResponseBody List<Filiere> getFiliere(
			@RequestParam(value = "centre_id", required = true, defaultValue = "0") int centre_id,
			@RequestParam(value = "annee_id", required = true, defaultValue = "0") int annee_id,
			@RequestParam(value = "formation_id", required = true, defaultValue = "0") int formation_id
		) {
		Centre c = new Centre();
		c.setId(centre_id);
		
		Annee a = new Annee();
		a.setId(annee_id);
		
		Formation f = new Formation();
		f.setId(formation_id);
		
		return filiereService.findByCentreAnneeFormation(c,a,f);
	}
	
	@RequestMapping(value = "/data/getNiveau", method = RequestMethod.GET)
	public @ResponseBody List<Niveaux> getNiveau(
			@RequestParam(value = "centre_id", required = true, defaultValue = "0") int centre_id,
			@RequestParam(value = "annee_id", required = true, defaultValue = "0") int annee_id,
			@RequestParam(value = "formation_id", required = true, defaultValue = "0") int formation_id,
			@RequestParam(value = "filiere_id", required = true, defaultValue = "0") int filiere_id
		) {
		Centre c = new Centre();
		c.setId(centre_id);
		
		Annee a = new Annee();
		a.setId(annee_id);
		
		Formation f = new Formation();
		f.setId(formation_id);
		
		Filiere fi = new Filiere();
		fi.setId(filiere_id);
		
		return niveauxService.findByCentreAnneeFormationFiliere(c,a,f,fi);
	}
	
	
	@RequestMapping(value = "/data/getSemestre", method = RequestMethod.GET)
	public @ResponseBody List<Semestre> getSemestre(
			@RequestParam(value = "centre_id", required = true, defaultValue = "0") int centre_id,
			@RequestParam(value = "annee_id", required = true, defaultValue = "0") int annee_id,
			@RequestParam(value = "formation_id", required = true, defaultValue = "0") int formation_id,
			@RequestParam(value = "filiere_id", required = true, defaultValue = "0") int filiere_id,
			@RequestParam(value = "niveaux_id", required = true, defaultValue = "0") int niveaux_id
		) {
		Centre c = new Centre();
		c.setId(centre_id);
		
		Annee a = new Annee();
		a.setId(annee_id);
		
		Formation f = new Formation();
		f.setId(formation_id);
		
		Filiere fi = new Filiere();
		fi.setId(filiere_id);
		
		Niveaux n = new Niveaux();
		n.setId(niveaux_id);
		
		return semestreService.findByCentreAnneeFormationFiliereNiveau(c,a,f,fi,n);
	}
	
	
	@RequestMapping(value = "/data/getModule", method = RequestMethod.GET)
	public @ResponseBody List<Module> getModule(
			@RequestParam(value = "centre_id", required = true, defaultValue = "0") int centre_id,
			@RequestParam(value = "annee_id", required = true, defaultValue = "0") int annee_id,
			@RequestParam(value = "formation_id", required = true, defaultValue = "0") int formation_id,
			@RequestParam(value = "filiere_id", required = true, defaultValue = "0") int filiere_id,
			@RequestParam(value = "niveaux_id", required = true, defaultValue = "0") int niveaux_id,
			@RequestParam(value = "semestre_id", required = true, defaultValue = "0") int semestre_id
		) {
		Centre c = new Centre();
		c.setId(centre_id);
		
		Annee a = new Annee();
		a.setId(annee_id);
		
		Formation f = new Formation();
		f.setId(formation_id);
		
		Filiere fi = new Filiere();
		fi.setId(filiere_id);
		
		Niveaux n = new Niveaux();
		n.setId(niveaux_id);
		
		Semestre s = new Semestre();
		s.setId(semestre_id);
		
		return moduleService.findByCentreAnneeFormationFiliereNiveauSemestre(c,a,f,fi,n,s);
	}
	
	
	@RequestMapping(value = "/data/getMatiere", method = RequestMethod.GET)
	public @ResponseBody List<Matiere> getMatiere(
			@RequestParam(value = "centre_id", required = true, defaultValue = "0") int centre_id,
			@RequestParam(value = "annee_id", required = true, defaultValue = "0") int annee_id,
			@RequestParam(value = "formation_id", required = true, defaultValue = "0") int formation_id,
			@RequestParam(value = "filiere_id", required = true, defaultValue = "0") int filiere_id,
			@RequestParam(value = "niveaux_id", required = true, defaultValue = "0") int niveaux_id,
			@RequestParam(value = "semestre_id", required = true, defaultValue = "0") int semestre_id,
			@RequestParam(value = "module_id", required = true, defaultValue = "0") int module_id
		) {
		Centre c = new Centre();
		c.setId(centre_id);
		
		Annee a = new Annee();
		a.setId(annee_id);
		
		Formation f = new Formation();
		f.setId(formation_id);
		
		Filiere fi = new Filiere();
		fi.setId(filiere_id);
		
		Niveaux n = new Niveaux();
		n.setId(niveaux_id);
		
		Semestre s = new Semestre();
		s.setId(semestre_id);
		
		Module m = new Module();
		m.setId(module_id);
		
		return matiereService.findByCentreAnneeFormationFiliereNiveauSemestreModule(c,a,f,fi,n,s,m);
	}
	
	@RequestMapping(value = "/data/getEtudiantsNoteModule", method = RequestMethod.GET)
	public @ResponseBody List<JEtudiantNote> getEtudiantsNoteModule(
			@RequestParam(value = "module_id", required = true, defaultValue = "0") int module_id
		) throws NotFoundException {
		
		List<JEtudiantNote> list = new  ArrayList<JEtudiantNote>();
		
		
		Module m = new Module();
		m.setId(module_id);
		
		List<Controle> ct_list = controleService.findByModule(m);
		List<Etudiant> etudiant_list = etudiantService.findByControle(ct_list.get(0));
		
		
		List<Matiere> matieresList = matiereService.findByModule(m);
		
		for(Etudiant e : etudiant_list){
			
			List<Double> tableNotes = new ArrayList<Double>();
			List<Integer> tableCoef = new ArrayList<Integer>();
			int nbModuleCoef = 0;
			
			
			JEtudiantNote jt = new JEtudiantNote();
			jt.setEtudiant(e.getFullName());
			jt.setEtudiant_id(e.getId());
			jt.setNote((double) 0.0);
			
			System.out.println("- Etudiant: "+e.getFullName());
			
			for(Matiere ma : matieresList){
				
				nbModuleCoef = 0;
				Double note1=null;
				Double note2=null;
				Double note1Coef=0.0;
				Double note2Coef=0.0;
				Double moyen = null;
				
				
				System.out.println("-- Matiere: "+ma.getIntitule());
				tableCoef.add(ma.getCoeff());
				List<Controle> controlesList = controleService.findByMatiere(ma);
				
				for(Controle c : controlesList){
					if(c.getNiveaux().getId() == e.getNiveaux().getId() && c.getModule().getId() == m.getId())
					{ // Only this niveau & matiere
						nbModuleCoef += ma.getCoeff();
						System.out.println("Controle :"+c.getIntitule());
						
						Controle_note bnote = controle_noteSvc.findByControleEtudiant(c, e);
						Double note = (bnote == null) ? 0.0 : bnote.getNote();
						
						System.out.println("Coef: "+ma.getCoeff());
						
						
						if(c.getNumControle().ordinal() == 0)
						{
							note1 = note;
							note1Coef = note*ma.getCoeff();
							System.out.println("Note 1 avec Coef: "+note1Coef);
						}
						if(c.getNumControle().ordinal() == 1)
						{
							note2 = note;
							note2Coef = note*ma.getCoeff();
							System.out.println("Note 2 avec Coef: "+note2Coef);
						}
						
						
						
					}
				}
				
				Double nnote1 = 0.0;
				if(note1Coef != null)
					nnote1 = note1Coef;
					
				Double nnote2 = 0.0;
				if(note1Coef != null)
					nnote2 = note2Coef;
				
				System.out.println("Note1: "+nnote1+" / Note2: "+nnote2);
				
			
				
				if(nbModuleCoef > 0){
					tableNotes.add(((nnote1+nnote2)/nbModuleCoef));
					System.out.println("Table Note Moyen :"+ ((nnote1+nnote2)/nbModuleCoef));
					//jt.setNote((nnote1+nnote2)/nbModuleCoef);
					
				}
				else{
					tableNotes.add(nnote1+nnote2);
					System.out.println("Table Note sans moyen: "+(nnote1+nnote2));
					//jt.setNote(nnote1+nnote2);
				}
			}
			
			Double sommeNotes = 0.0;
			//System.out.println("******** Module : "+m.getIntitule());
			
			//System.out.println("nbcoef module: "+nbModuleCoef);
			nbModuleCoef = 0;
			for(int i=0;i<tableNotes.size();i++){
				System.out.println("Coef "+tableCoef.get(i));
				System.out.println("Note "+tableNotes.get(i));
				System.out.println("Mul "+tableNotes.get(i)*tableCoef.get(i));
				nbModuleCoef += tableCoef.get(i);
				sommeNotes += tableNotes.get(i)*tableCoef.get(i);
			}
			System.out.println("Total Moen: "+nbModuleCoef);
			System.out.println("Somme : "+sommeNotes);
			System.out.println("Moyen :" +sommeNotes/nbModuleCoef);
			System.out.println("////// *******");
			jt.setNote(sommeNotes/nbModuleCoef);
			list.add(jt);
			
		}
		
		/*
		for(Etudiant e : etudiant_list){
			
			JEtudiantNote jt = new JEtudiantNote();
			jt.setEtudiant(e.getFullName());
			jt.setEtudiant_id(e.getId());
			jt.setNote((double) 0.0);
			int nbCoef = 0;
			for(Controle c : ct_list){
				nbCoef += c.getMatiere().getCoeff();
				Controle_note bnote = controle_noteSvc.findByControleEtudiant(c, e);
				
				Double note = (bnote == null) ? 0.0 : bnote.getNote();
				System.out.println("***************************");
				System.out.println(e.getFullName()+ "Avent Note: "+(jt.getNote())+" Note Apres: "+note+" Coeff: "+c.getMatiere().getCoeff());
				System.out.println((double) (jt.getNote()+(note*c.getMatiere().getCoeff())));
				System.out.println("***************************");
				jt.setNote((double) (jt.getNote()+(note*c.getMatiere().getCoeff())));
			}
			
			if(nbCoef > 0)
				jt.setNote((double)jt.getNote()/nbCoef);
			else
				jt.setNote((double)jt.getNote());
			list.add(jt);
		}*/
		
		
		
		return list;
	}
	
	@RequestMapping(value = "/data/getEtudiantsNoteMatiere", method = RequestMethod.GET)
	public @ResponseBody List<JEtudiantNote> getEtudiantsNoteMatiere(
			@RequestParam(value = "module_id", required = true, defaultValue = "0") int module_id,
			@RequestParam(value = "matiere_id", required = true, defaultValue = "0") int matiere_id
		) throws NotFoundException {
		
		List<JEtudiantNote> list = new  ArrayList<JEtudiantNote>();
		
		
		Module m = new Module();
		m.setId(module_id);
		
		Matiere ma = new Matiere();
		ma.setId(matiere_id);
		
		List<Controle> ct_list = controleService.findByModule(m);
		List<Etudiant> etudiant_list = etudiantService.findByControle(ct_list.get(0));
		
		
		for(Etudiant e : etudiant_list){
			
			JEtudiantNote jt = new JEtudiantNote();
			jt.setEtudiant(e.getFullName());
			jt.setEtudiant_id(e.getId());
			jt.setNote((double) 0.0);
			int nbCoef = 0;
			for(Controle c : ct_list){
				if(c.getMatiere().getId() == ma.getId())
				{
					nbCoef += c.getMatiere().getCoeff();
					Controle_note bnote = controle_noteSvc.findByControleEtudiant(c, e);
					
					Double note = (bnote == null) ? 0.0 : bnote.getNote();
					System.out.println("***************************");
					System.out.println(e.getFullName()+ "Avent Note: "+(jt.getNote())+" Note Apres: "+note+" Coeff: "+c.getMatiere().getCoeff());
					System.out.println((double) (jt.getNote()+(note*c.getMatiere().getCoeff())));
					System.out.println("***************************");
					jt.setNote((double) (jt.getNote()+(note*c.getMatiere().getCoeff())));
				}
			}
			
			if(nbCoef > 0)
				jt.setNote((double)jt.getNote()/nbCoef);
			else
				jt.setNote((double)jt.getNote());
			list.add(jt);
		}
		
		
		
		return list;
	}
	

	@RequestMapping(value = "/data/getEtudiantsNiveau", method = RequestMethod.GET)
	public @ResponseBody List<Etudiant> getEtudiantsNiveau(
			@RequestParam(value = "niveau_id", required = true, defaultValue = "0") int niveau_id
		) throws NotFoundException {
		
		Niveaux n = new Niveaux();
		n.setId(niveau_id);
		return etudiantService.findByNiveau(n);
		
		
	}
	
	
	
}
