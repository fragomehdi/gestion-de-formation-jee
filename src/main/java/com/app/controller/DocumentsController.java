package com.app.controller;

import javassist.NotFoundException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.security.SecureRandom;
import java.util.List;
import java.util.Set;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentCatalog;
import org.apache.pdfbox.pdmodel.interactive.form.*;
import org.apache.commons.io.IOUtils;

import com.app.model.Diplome;
import com.app.model.DiplomeField;
import com.app.model.Attestation;
import com.app.model.AttestationField;
import com.app.model.Etudiant;
import com.app.service.AttestationService;
import com.app.service.DiplomeService;
import com.app.service.EtudiantService;
import com.app.service.LogTrackerService;


@Controller
public class DocumentsController {
	@Autowired
	DiplomeService diplomeService;
	@Autowired
	AttestationService attestationService;
	@Autowired
	EtudiantService etudiantService;
	@Autowired
	LogTrackerService logSvc;
	
	@PreAuthorize("hasRole('DOCUMENT_READ')")
	@RequestMapping(value = "/etudiants/documents", method = RequestMethod.GET)
	public String index(Model model, final RedirectAttributes redirectAttributes) {
		if(redirectAttributes.containsAttribute("active_attestation"))
			model.addAttribute("active_attestation", true);
		else if(redirectAttributes.containsAttribute("active_diplome"))
			model.addAttribute("active_diplome", true);
		else
			model.addAttribute("active_diplome", true);
		
		model.addAttribute("liste_diplomes", diplomeService.getAll());
		model.addAttribute("liste_attestations", attestationService.getAll());
		model.addAttribute("liste_etudiants", etudiantService.getAll());
		return "etudiants/documents/home";
	}
	
	@PreAuthorize("hasRole('DOCUMENT_EDIT')")
	@RequestMapping(value = "/etudiants/documents/diplomes/ajouter", method = RequestMethod.GET)
	public String ajouterDiplome() {
		return "etudiants/documents/diplomes/ajouter";
	}
	
	@PreAuthorize("hasRole('DOCUMENT_EDIT')")
	@RequestMapping(value = "/etudiants/documents/diplomes/save", method = RequestMethod.POST)
	public String saveDiplome(
			@RequestParam("intitule") String intitule,
			@RequestParam("file") MultipartFile file,
			final RedirectAttributes redirectAttributes,
			HttpServletRequest request
			) {
		if (file.isEmpty()) {
			redirectAttributes.addFlashAttribute("error", "Veuilez charger un fichier PDF remplisable");
			return "redirect:/etudiants/documents/diplomes/ajouter";
		}
		else
		{
			// Creation des dossiers si inexsistant
            String rootPath = System.getProperty("catalina.base");
            File dir = new File(rootPath + File.separator + "uploads/diplomes");
            if (!dir.exists())
                dir.mkdirs();
            
            //System.out.println(rootPath + File.separator + "uploads/diplomes");
            
            
			// Nom aleatoire
			SecureRandom random = new SecureRandom();
			String filename = new BigInteger(130, random).toString(32)+".pdf";
			Diplome dpUpdated = null;
			try {
				// MD5 FILE
                byte[] bytes = file.getBytes();
                BufferedOutputStream stream =
                        new BufferedOutputStream(new FileOutputStream(new File(dir.getAbsolutePath()
                                + File.separator + filename)));
                stream.write(bytes);
                stream.close();
                
                Diplome dp = new Diplome();
                dp.setIntitule(intitule);
                dp.setDate_creation(new DateTime());
                dp.setPath(filename);
                dpUpdated = diplomeService.add(dp);
            	logSvc.store("Ajout diplome: "+dp, request);
                
                
                
                
            } catch (Exception e) {
            	redirectAttributes.addFlashAttribute("error", "Erreur:" + e.getMessage());
    			return "redirect:/etudiants/documents/diplomes/ajouter";
            }
			
			System.out.println(dpUpdated.getId()+" Created! ");
			
			/**
             * Lecture des inputs form depuis le fichier PDF
             */
            PDDocument doc;
		
				File file2 = new File(dir.getAbsolutePath()
				        + File.separator + filename);
				try {
					doc = PDDocument.load(file2);
					System.out.println(file2.canRead());
					
					System.out.println(dir.getAbsolutePath()
					        + File.separator + filename);
					System.out.println(" File readed! ");
					
					PDDocumentCatalog catalog = doc.getDocumentCatalog();
		            PDAcroForm form = catalog.getAcroForm();
		    		List<PDField> fields = form.getFields();

		            for(PDField field: fields) {
		            	//if(field != null && field.getFullyQualifiedName() != null)
		            	//{
		            		//Object value = field.getValue();
			            	String name = field.getFullyQualifiedName();
			            	DiplomeField df = new DiplomeField();
			            	df.setDiplome(dpUpdated);
			            	df.setField(name);
			            	//df.setValue(value.toString());
			            	dpUpdated.addField(df); 
			            	System.out.print(name);
			                System.out.print(" = ");
			                //System.out.print(value);
			                System.out.println();
		            	//}
		            	
		            }
		            
		            System.out.println("Hello");
		            
		            doc.close();
				} catch (IOException e1) {
					redirectAttributes.addFlashAttribute("error", "Erreur:" + e1.getMessage());
	    			return "redirect:/etudiants/documents/diplomes/ajouter";
				}
				
				
				
			
            
            
            
            try {
				diplomeService.update(dpUpdated);
				logSvc.store("Modifier diplome: "+dpUpdated, request);
				redirectAttributes.addFlashAttribute("success", "Le fichier "+intitule+ "a ete charger avec success");
				return "redirect:/etudiants/documents/diplomes/modifier?id="+dpUpdated.getId();
				
			} catch (NotFoundException e) {
				redirectAttributes.addFlashAttribute("error", "Erreur:" + e.getMessage());
    			return "redirect:/etudiants/documents/diplomes/ajouter";
			}

            
            
            

			
		}
		
	}
	
	@PreAuthorize("hasRole('DOCUMENT_EDIT')")
	
	@RequestMapping(value = "/etudiants/documents/diplomes/modifier", method = RequestMethod.GET)
	public String modifierDiplome(
			@RequestParam(value = "id", required = true, defaultValue = "0") int id,
			Model model
			) {
		Diplome dp = diplomeService.findById(id);
		
		
		model.addAttribute("diplome", dp);
		return "etudiants/documents/diplomes/modifier";
	}
	
	
	@RequestMapping(value = "/etudiants/documents/diplomes/update", method = RequestMethod.POST)
	public String updateDiplome(
			@RequestParam(value = "id", required = true, defaultValue = "0") int id,
			@RequestParam(value = "intitule", required = true, defaultValue = "") String intitule,
			@RequestParam(value = "values[]", required = true, defaultValue = "") String[] values,
			Model model,
			final RedirectAttributes redirectAttributes,
			HttpServletRequest request
			) {
		Diplome dp = diplomeService.findById(id);
		Set<DiplomeField> fields = (Set<DiplomeField>) dp.getFields();
		
		System.out.println(values);
		
		int i = 0;
		for(DiplomeField field : fields){
			String value = values[i];
			System.out.println(field.getField());
			System.out.println(value);
			field.setValue(value);
			i++;
		}
		
		/*for(int i=0; i<fields.size(); i++){
			String value = values[i];
			fields.
			fields.get(i).setValue(value);
		}
		*/
		dp.setIntitule(intitule);
		
		try {
			diplomeService.update(dp);
			logSvc.store("Modification diplome: "+dp, request);
			//model.addAttribute("diplome", diplomeService.findById(id));
			redirectAttributes.addFlashAttribute("success", "L'element "+intitule+" a ete mis a jour ");
			return "redirect:/etudiants/documents/diplomes/modifier?id="+id;
		} catch (NotFoundException e) {
			redirectAttributes.addFlashAttribute("error", "Erreur:" + e.getMessage());
			return "redirect:/etudiants/documents/diplomes/modifier?id="+id;
		}
		
		
	}
	
	@PreAuthorize("hasRole('DOCUMENT_READ')")
	@RequestMapping(value = "/etudiants/documents/diplomes/download", method = RequestMethod.POST)
	public @ResponseBody void downloadDiplome(
			@RequestParam(value = "etudiant_id", required = true, defaultValue = "0") int etudiant_id,
			@RequestParam(value = "diplome_id", required = true, defaultValue = "0") int diplome_id,
			HttpServletResponse response,HttpServletRequest request
			) throws IOException, COSVisitorException {
			System.out.println(etudiant_id);
			System.out.println(diplome_id);
			Etudiant e = etudiantService.findById(etudiant_id);
			Diplome dp = diplomeService.findById(diplome_id);
			
			Set<DiplomeField> dp_fields = (Set<DiplomeField>) dp.getFields();
			
			String filename = dp.getPath();
			String rootPath = System.getProperty("catalina.base");
            File dir = new File(rootPath + File.separator + "uploads/diplomes");
			
            File file = new File(dir.getAbsolutePath()
			        + File.separator + filename);
            
            PDDocument doc = PDDocument.load(file);
            
            PDDocumentCatalog catalog = doc.getDocumentCatalog();
            PDAcroForm form = catalog.getAcroForm();
            @SuppressWarnings("unchecked")
    		List<PDField> fields = form.getFields();

            for(PDField field: fields) {
                String name = field.getFullyQualifiedName();
                //field.setValue("Hello");
                for(DiplomeField f : dp_fields){
        			if(f.getField().equals(name))
        			{
        				String v = "";
        				if(f.getValue().equals("date"))
        				{
        					DateTime dt = new DateTime();
        					 DateTimeFormatter fmt = DateTimeFormat.forPattern("MM/dd/yyyy");
        					 String str = fmt.print(dt);
        					v = str;
        				}
        				else if(f.getValue().equals("nom_etudiant")){
        					v = e.getNom();
        				}
						else if(f.getValue().equals("prenom_etudiant")){
        					v = e.getPrenom();
        				}
						else if(f.getValue().equals("fullname_etudiant")){
        					v = e.getFullName();
        				}
        				field.setValue(v);
        				break;
        			}
        		}
            }
            
            filename = "out_"+filename;
            
            System.out.println(dir.getAbsolutePath()
			        + File.separator + filename);

            doc.save(dir.getAbsolutePath()
			        + File.separator + filename);
            doc.close();
            
            File downloadFile = new File(dir.getAbsolutePath()
			        + File.separator + filename);
            
            FileInputStream inputStream = null;
    		OutputStream outStream = null;
    		
    		inputStream = new FileInputStream(downloadFile);
    		 
			response.setContentLength((int) downloadFile.length());
			response.setContentType("application/pdf");			
			response.setHeader("Content-Disposition", "attachment; filename=diplome_"+etudiant_id+".pdf"); 
 
			// Write response
			outStream = response.getOutputStream();
			IOUtils.copy(inputStream, outStream);
			logSvc.store("telecharger  emploie: "+dp, request);

			
	}
	
	@PreAuthorize("hasRole('DOCUMENT_EDIT')")
	@RequestMapping(value = "/etudiants/documents/diplomes/delete", method = RequestMethod.GET)
	public String deleteDiplome(
			@RequestParam(value = "id", required = true, defaultValue = "0") int id,
			final RedirectAttributes redirectAttributes,
			HttpServletRequest request
			) {
		
		redirectAttributes.addFlashAttribute("active_diplome", true);
		
		try {
			Diplome p = diplomeService.findById(id);
			diplomeService.delete(id);
			logSvc.store("Suppression diplome: "+p, request);
			

			redirectAttributes.addFlashAttribute("success_diplome_delete", p.getIntitule()
					+ " a ete supprimer");

			return "redirect:/etudiants/documents/";
		} catch (NotFoundException e) {
			redirectAttributes.addFlashAttribute("error_diplome_delete", "Entity not found");
			return "redirect:/etudiants/documents/";
		}
		
	}
	
	@PreAuthorize("hasRole('DOCUMENT_EDIT')")
	@RequestMapping(value = "/etudiants/documents/attestations/ajouter", method = RequestMethod.GET)
	public String ajouterAttestation() {
		return "etudiants/documents/attestations/ajouter";
	}
	
	@PreAuthorize("hasRole('DOCUMENT_EDIT')")
	@RequestMapping(value = "/etudiants/documents/attestations/save", method = RequestMethod.POST)
	public String saveAttestation(
			@RequestParam("intitule") String intitule,
			@RequestParam("file") MultipartFile file,
			final RedirectAttributes redirectAttributes,
			HttpServletRequest request
			) {
		if (file.isEmpty()) {
			redirectAttributes.addFlashAttribute("error", "Veuilez charger un fichier PDF remplisable");
			return "redirect:/etudiants/documents/attestations/ajouter";
		}
		else
		{
			// Creation des dossiers si inexsistant
            String rootPath = System.getProperty("catalina.base");
            File dir = new File(rootPath + File.separator + "uploads/attestations");
            if (!dir.exists())
                dir.mkdirs();
            
            //System.out.println(rootPath + File.separator + "uploads/diplomes");
            
            
			// Nom aleatoire
			SecureRandom random = new SecureRandom();
			String filename = new BigInteger(130, random).toString(32)+".pdf";
			Attestation dpUpdated = null;
			try {
				// MD5 FILE
                byte[] bytes = file.getBytes();
                BufferedOutputStream stream =
                        new BufferedOutputStream(new FileOutputStream(new File(dir.getAbsolutePath()
                                + File.separator + filename)));
                stream.write(bytes);
                stream.close();
                
                Attestation dp = new Attestation();
                dp.setIntitule(intitule);
                dp.setDate_creation(new DateTime());
                dp.setPath(filename);
                dpUpdated = attestationService.add(dp);
                
                
                
                
            } catch (Exception e) {
            	redirectAttributes.addFlashAttribute("error", "Erreur:" + e.getMessage());
    			return "redirect:/etudiants/documents/attestations/ajouter";
            }
			
			System.out.println(dpUpdated.getId()+" Created! ");
			
			/**
             * Lecture des inputs form depuis le fichier PDF
             */
            PDDocument doc;
		
				File file2 = new File(dir.getAbsolutePath()
				        + File.separator + filename);
				try {
					doc = PDDocument.load(file2);
					System.out.println(file2.canRead());
					
					System.out.println(dir.getAbsolutePath()
					        + File.separator + filename);
					System.out.println(" File readed! ");
					
					PDDocumentCatalog catalog = doc.getDocumentCatalog();
		            PDAcroForm form = catalog.getAcroForm();
		    		List<PDField> fields = form.getFields();

		            for(PDField field: fields) {
		            	//if(field != null && field.getFullyQualifiedName() != null)
		            	//{
		            		//Object value = field.getValue();
			            	String name = field.getFullyQualifiedName();
			            	AttestationField df = new AttestationField();
			            	df.setAttestation(dpUpdated);
			            	df.setField(name);
			            	//df.setValue(value.toString());
			            	dpUpdated.addField(df); 
			            	System.out.print(name);
			                System.out.print(" = ");
			                //System.out.print(value);
			                System.out.println();
		            	//}
		            	
		            }
		            
		            System.out.println("Hello");
		            
		            doc.close();
				} catch (IOException e1) {
					redirectAttributes.addFlashAttribute("error", "Erreur:" + e1.getMessage());
	    			return "redirect:/etudiants/documents/attestations/ajouter";
				}
				
				
            try {
            	attestationService.update(dpUpdated);
				redirectAttributes.addFlashAttribute("success", "Le fichier "+intitule+ "a ete charger avec success");
				return "redirect:/etudiants/documents/attestations/modifier?id="+dpUpdated.getId();
				
			} catch (NotFoundException e) {
				redirectAttributes.addFlashAttribute("error", "Erreur:" + e.getMessage());
    			return "redirect:/etudiants/documents/attestations/ajouter";
			}

            
            
            

			
		}
		
	}
	@PreAuthorize("hasRole('DOCUMENT_EDIT')")
	@RequestMapping(value = "/etudiants/documents/attestations/modifier", method = RequestMethod.GET)
	public String modifierAttestation(
			@RequestParam(value = "id", required = true, defaultValue = "0") int id,
			Model model
			) {
		Attestation dp = attestationService.findById(id);
		
		model.addAttribute("attestation", dp);
		return "etudiants/documents/attestations/modifier";
	}
	@PreAuthorize("hasRole('CONTROLE_NOTE_READ')")
	
	@RequestMapping(value = "/etudiants/documents/attestations/update", method = RequestMethod.POST)
	public String updateAttestation(
			@RequestParam(value = "id", required = true, defaultValue = "0") int id,
			@RequestParam(value = "intitule", required = true, defaultValue = "") String intitule,
			@RequestParam(value = "values[]", required = true, defaultValue = "") String[] values,
			Model model,
			final RedirectAttributes redirectAttributes,
			HttpServletRequest request
			) {
		Attestation dp = attestationService.findById(id);
		Set<AttestationField> fields = (Set<AttestationField>) dp.getFields();
		
		System.out.println(values);
		
		int i = 0;
		for(AttestationField field : fields){
			String value = values[i];
			System.out.println(field.getField());
			System.out.println(value);
			field.setValue(value);
			i++;
		}
		
		/*for(int i=0; i<fields.size(); i++){
			String value = values[i];
			fields.
			fields.get(i).setValue(value);
		}
		*/
		dp.setIntitule(intitule);
		
		try {
			attestationService.update(dp);
			//model.addAttribute("diplome", diplomeService.findById(id));
			redirectAttributes.addFlashAttribute("success", "L'element "+intitule+" a ete mis a jour ");
			return "redirect:/etudiants/documents/attestations/modifier?id="+id;
		} catch (NotFoundException e) {
			redirectAttributes.addFlashAttribute("error", "Erreur:" + e.getMessage());
			return "redirect:/etudiants/documents/attestations/modifier?id="+id;
		}
		
		
	}
	
	@PreAuthorize("hasRole('DOCUMENT_READ')")
	@RequestMapping(value = "/etudiants/documents/attestations/download", method = RequestMethod.POST)
	public @ResponseBody void downloadAttestation(
			@RequestParam(value = "etudiant_id", required = true, defaultValue = "0") int etudiant_id,
			@RequestParam(value = "attestation_id", required = true, defaultValue = "0") int attestation_id,
			HttpServletResponse response
			) throws IOException, COSVisitorException {
			System.out.println(etudiant_id);
			System.out.println(attestation_id);
			Etudiant e = etudiantService.findById(etudiant_id);
			Attestation dp = attestationService.findById(attestation_id);
			
			Set<AttestationField> dp_fields = (Set<AttestationField>) dp.getFields();
			
			String filename = dp.getPath();
			String rootPath = System.getProperty("catalina.base");
            File dir = new File(rootPath + File.separator + "uploads/attestations");
			
            File file = new File(dir.getAbsolutePath()
			        + File.separator + filename);
            
            PDDocument doc = PDDocument.load(file);
            
            PDDocumentCatalog catalog = doc.getDocumentCatalog();
            PDAcroForm form = catalog.getAcroForm();
            @SuppressWarnings("unchecked")
    		List<PDField> fields = form.getFields();

            for(PDField field: fields) {
                String name = field.getFullyQualifiedName();
                //field.setValue("Hello");
                for(AttestationField f : dp_fields){
        			if(f.getField().equals(name))
        			{
        				String v = "";
        				if(f.getValue().equals("date"))
        				{
        					DateTime dt = new DateTime();
        					 DateTimeFormatter fmt = DateTimeFormat.forPattern("MM/dd/yyyy");
        					 String str = fmt.print(dt);
        					v = str;
        				}
        				else if(f.getValue().equals("nom_etudiant")){
        					v = e.getNom();
        				}
						else if(f.getValue().equals("prenom_etudiant")){
        					v = e.getPrenom();
        				}
						else if(f.getValue().equals("fullname_etudiant")){
        					v = e.getFullName();
        				}
						else if(f.getValue().equals("annee")){
        					v = e.getAnnee().getIntitule();
        				}
						else if(f.getValue().equals("filiere")){
        					v = e.getFiliere().getIntitule();
        				}
						else if(f.getValue().equals("option")){
        					v = e.getNiveaux().getIntitule();
        				}
        				field.setValue(v);
        				break;
        			}
        		}
            }
            
            filename = "out_"+filename;
            
            System.out.println(dir.getAbsolutePath()
			        + File.separator + filename);

            doc.save(dir.getAbsolutePath()
			        + File.separator + filename);
            doc.close();
            
            File downloadFile = new File(dir.getAbsolutePath()
			        + File.separator + filename);
            
            FileInputStream inputStream = null;
    		OutputStream outStream = null;
    		
    		inputStream = new FileInputStream(downloadFile);
    		 
			response.setContentLength((int) downloadFile.length());
			response.setContentType("application/pdf");			
			response.setHeader("Content-Disposition", "attachment; filename=attestation_"+etudiant_id+".pdf"); 
 
			// Write response
			outStream = response.getOutputStream();
			IOUtils.copy(inputStream, outStream);

			
	}
	
	@PreAuthorize("hasRole('DOCUMENT_EDIT')")
	@RequestMapping(value = "/etudiants/documents/attestations/delete", method = RequestMethod.GET)
	public String deleteAttestation(
			@RequestParam(value = "id", required = true, defaultValue = "0") int id,
			final RedirectAttributes redirectAttributes,
			HttpServletRequest request
			) {
		
		redirectAttributes.addFlashAttribute("active_attestation", true);
		
		try {
			System.out.println("hello");
			Attestation p = attestationService.findById(id);
			attestationService.delete(id);
			logSvc.store("Suppression attestation: "+p, request);
			

			redirectAttributes.addFlashAttribute("success_attestation_delete", p.getIntitule()
					+ " a ete supprimer");

			return "redirect:/etudiants/documents/";
		} catch (NotFoundException e) {
			redirectAttributes.addFlashAttribute("error_attestation_delete", "Entity not found");
			return "redirect:/etudiants/documents/";
		}
		
	}
	@PreAuthorize("hasRole('DOCUMENT_READ')")
	@RequestMapping(value = "/etudiants/documents/consulter", method = RequestMethod.GET)
	public @ResponseBody  void consulter(
			@RequestParam(value = "type", required = true, defaultValue = "") String type,
			@RequestParam(value = "path", required = true, defaultValue = "") String path,
			HttpServletResponse response
			) {
		
		String rootPath = System.getProperty("catalina.base");
		File dir = null;
		if(type.equals("attestations"))
			 dir = new File(rootPath + File.separator + "uploads/attestations");
		else if(type.equals("diplomes")){
			 dir = new File(rootPath + File.separator + "uploads/diplomes");
		}
		
		 try
	        {
	            // get your file as InputStream
			 System.out.println(dir.getAbsolutePath()
				        + File.separator + path);
	            InputStream is = new FileInputStream(dir.getAbsolutePath()
				        + File.separator + path);
	            // copy it to response's OutputStream
	            IOUtils.copy( is, response.getOutputStream() );
	            response.flushBuffer();
	        }
	        catch( IOException ex )
	        {
	            throw new RuntimeException( "IOError writing file to output stream" );
	        }
	
	}
	
}
