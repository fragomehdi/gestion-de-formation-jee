package com.app.controller;

import javassist.NotFoundException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.app.model.Annee;
import com.app.model.Centre;
import com.app.model.Filiere;
import com.app.model.Formation;
import com.app.model.Niveaux;
import com.app.model.Semestre;
import com.app.propertyeditors.AnneeEditor;
import com.app.propertyeditors.CentreEditor;
import com.app.propertyeditors.FiliereEditor;
import com.app.propertyeditors.FormationEditor;
import com.app.propertyeditors.NiveauEditor;
import com.app.service.AnneeService;
import com.app.service.CentreService;
import com.app.service.FiliereService;
import com.app.service.FormationService;
import com.app.service.LogTrackerService;
import com.app.service.NiveauxService;
import com.app.service.SemestreService;

@Controller
public class SemestreController {
	@Autowired
	SemestreService semestreSvc;
	@Autowired
	LogTrackerService logSvc;
	@Autowired
	FormationService formationSvc;
	@Autowired
	AnneeService anneeService;
	@Autowired
	CentreService centreService;
	@Autowired
	FiliereService filiereService;
	@Autowired
	NiveauxService niveauxService;
	
	
	@InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Annee.class, new AnneeEditor());
        binder.registerCustomEditor(Centre.class, new CentreEditor());
        binder.registerCustomEditor(Formation.class, new FormationEditor());
        binder.registerCustomEditor(Filiere.class, new FiliereEditor());
        binder.registerCustomEditor(Niveaux.class, new NiveauEditor());
    }
	
	@PreAuthorize("hasRole('SEMESTRE_READ')")
	@RequestMapping(value = "/parametrage/semestre", method = RequestMethod.GET)
	public String index(Model model) {
		model.addAttribute("mode", "create");
		model.addAttribute("semestreForm", new Semestre());
		model.addAttribute("liste_semestres", semestreSvc.getAll());
		model.addAttribute("centres", centreService.getAll());
		model.addAttribute("annees", anneeService.getAll());
		model.addAttribute("formations", formationSvc.getAll());
		model.addAttribute("filieres", filiereService.getAll());
		model.addAttribute("niveaux", niveauxService.getAll());
		

		
		return "parametrage/semestre";
	}
	
	@PreAuthorize("hasRole('SEMESTRE_EDIT')")
	@RequestMapping(value = "/parametrage/saveSemestre", method = RequestMethod.POST)
	public String savePerson(
			@Valid @ModelAttribute("semestreForm") Semestre v,
			BindingResult bindingResult,
			@RequestParam(value = "mode", required = true, defaultValue = "create") String mode,
			Model model,
			final RedirectAttributes redirectAttributes,
			HttpServletRequest request
			) throws Exception {
		if (bindingResult.hasErrors()) {
			System.out.println(bindingResult.toString());
			
			model.addAttribute("semestreForm", new Semestre());
			model.addAttribute("liste_semestres", semestreSvc.getAll());
			
			model.addAttribute("centres", centreService.getAll());
			model.addAttribute("annees", anneeService.getAll());
			model.addAttribute("formations", formationSvc.getAll());
			model.addAttribute("filieres", filiereService.getAll());
			model.addAttribute("niveaux", niveauxService.getAll());
			
			return "parametrage/semestre";
		} else {
			
			if(mode.equals("create"))
			{
				semestreSvc.add(v);
				logSvc.store("Ajout semestre: "+v, request);
				redirectAttributes.addFlashAttribute("success_semestre", v.getIntitule() + " a ete bien ajouter");
				return "redirect:/parametrage/semestre";
			}
			else{
				System.out.println("Updating "+v);
				try {
					semestreSvc.update(v);
					logSvc.store("Mise a jour semestre: "+v, request);
					redirectAttributes.addFlashAttribute("success_semestre", v.getIntitule() + " a ete bien modifier");
					return "redirect:/parametrage/semestre";
				} catch (NotFoundException e) {
					redirectAttributes.addFlashAttribute("error_semestre", "Entity not found");
					return "redirect:/parametrage/semestre";
				}
				
			}
			
		}
	}
	

	
	@PreAuthorize("hasRole('SEMESTRE_EDIT')")
	@RequestMapping(value = "/parametrage/editSemestre", method = RequestMethod.GET)
	public String editSemestre(
			@RequestParam(value = "id", required = true, defaultValue = "0") int id,
			Model model
			) {
		Semestre p = semestreSvc.findById(id);
		System.out.println("Editing "+p);
		model.addAttribute("mode", "edit");
		model.addAttribute("semestreForm", p);
		model.addAttribute("/*Form", new Semestre());
		
		model.addAttribute("liste_semestres", semestreSvc.getAll());
		model.addAttribute("centres", centreService.getAll());
		model.addAttribute("annees", anneeService.getAll());
		model.addAttribute("formations", formationSvc.getAll());
		model.addAttribute("filieres", filiereService.getAll());
		model.addAttribute("niveaux", niveauxService.getAll());
		
		
		return "parametrage/semestre";
	}
	

	@PreAuthorize("hasRole('SEMESTRE_EDIT')")
	@RequestMapping(value = "/parametrage/deleteSemestre", method = RequestMethod.GET)
	public String deleteSemestre(
			@RequestParam(value = "id", required = true, defaultValue = "0") int id,
			final RedirectAttributes redirectAttributes,
			HttpServletRequest request) {
		try {
			Semestre p = semestreSvc.findById(id);
			semestreSvc.delete(id);
			logSvc.store("Suppression semestre: "+p, request);
			redirectAttributes.addFlashAttribute("success_semestre_delete", p.getIntitule()
					+ " a ete supprimer");

			return "redirect:/parametrage/semestre";
		} catch (NotFoundException e) {
			redirectAttributes.addFlashAttribute("error_semestre_delete", "Entity not found");
			return "redirect:/parametrage/semestre";
		}
		
	}
	
	

	
}
