package com.app.controller;


import javassist.NotFoundException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;  

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.app.model.Role;
import com.app.model.User;
import com.app.propertyeditors.RoleEditor;
import com.app.service.LogTrackerService;
import com.app.service.RoleService;
import com.app.service.UserService;

@Controller
public class UtilisateurController {
	@Autowired
	LogTrackerService logSvc;
	@Autowired
	UserService userService;
	@Autowired
	RoleService roleService;
	
	@InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Role.class, new RoleEditor());
    }
	
	@PreAuthorize("hasRole('CAN_ACCESS_PARAMETRAGE')")
	@RequestMapping(value = "/parametrage/utilisateurs", method = RequestMethod.GET)
	public String index(Model model) 
	{
		model.addAttribute("liste", userService.getAll());
		return "parametrage/utilisateurs/liste";
	}
	

	@PreAuthorize("hasRole('CAN_ACCESS_PARAMETRAGE')")
	@RequestMapping(value = "/parametrage/utilisateurs/ajouter", method = RequestMethod.GET)
	public String ajouter(Model model) 
	{
		User u = new User();
		u.setActivated(true);
		model.addAttribute("userForm", u);
		model.addAttribute("listeRoles", roleService.getAll());
		return "parametrage/utilisateurs/ajouter";
	}
	
	@PreAuthorize("hasRole('CAN_ACCESS_PARAMETRAGE')")
	@RequestMapping(value = "/parametrage/utilisateurs/save", method = RequestMethod.POST)
	public String save(
			@Valid @ModelAttribute("userForm") User p,
			BindingResult bindingResult,
			Model model,
			final RedirectAttributes redirectAttributes,
			HttpServletRequest request
			) throws Exception {
		if (bindingResult.hasErrors()) {
			//System.out.println("Erreur");
			//System.out.println(bindingResult.toString());
			model.addAttribute("userForm", p);
			model.addAttribute("listeRoles", roleService.getAll());
			
			return "parametrage/utilisateurs/ajouter";
		} else {
			Md5PasswordEncoder encoder = new Md5PasswordEncoder();
		    String hashedPass = encoder.encodePassword(p.getPassword(), null);
		    p.setPassword(hashedPass);
		    
			userService.add(p);
			logSvc.store("Ajout Utilisateur: "+p, request);
			redirectAttributes.addFlashAttribute("success_user", p.getUsername() + " a ete bien ajouter");
			return "redirect:/parametrage/utilisateurs/ajouter";

		}
	}
	
	@PreAuthorize("hasRole('CAN_ACCESS_PARAMETRAGE')")
	@RequestMapping(value = "/parametrage/utilisateurs/edit", method = RequestMethod.GET)
	public String edit(
			@RequestParam(value = "id", required = true, defaultValue = "0") int id,
			Model model
			) {
		User p = userService.findById(id);
		System.out.println("Editing "+p);
		model.addAttribute("userForm", p);
		model.addAttribute("listeRoles", roleService.getAll());
		
		return "parametrage/utilisateurs/modifier";
	}
	
	@PreAuthorize("hasRole('CAN_ACCESS_PARAMETRAGE')")
	@RequestMapping(value = "/parametrage/utilisateurs/update", method = RequestMethod.POST)
	public String update(
			@Valid @ModelAttribute("userForm") User p,
			BindingResult bindingResult,
			Model model,
			final RedirectAttributes redirectAttributes,
			HttpServletRequest request
			) throws Exception {
		if (bindingResult.hasErrors()) {
			//System.out.println("Erreur");
			//System.out.println(bindingResult.toString());
			model.addAttribute("userForm", p);
			model.addAttribute("listeRoles", roleService.getAll());

			return "parametrage/utilisateurs/modifier";
		} else {
			
			try {
				User oldUser = userService.findById(p.getId());
				if(p.getPassword().trim().equals("")){ // pas besoin de mettre a jour le mot de passe
					p.setPassword(oldUser.getPassword());
				}
				else
				{ // Mettre a jour le mot de passe
					Md5PasswordEncoder encoder = new Md5PasswordEncoder();
				    String hashedPass = encoder.encodePassword(p.getPassword(), null);
				    p.setPassword(hashedPass);
				}
				
				//System.out.println("Update password");
				//System.out.println(p.getPassword());
				userService.update(p);
				logSvc.store("Mise a jour Utilisateur: "+p, request);
				redirectAttributes.addFlashAttribute("success_user", p.getUsername() + " a ete bien modifier");
				return "redirect:/parametrage/utilisateurs/edit?id="+p.getId();
			} catch (NotFoundException e) {
				redirectAttributes.addFlashAttribute("error_user", "Entity not found");
				return "redirect:/parametrage/utilisateurs/edit?id="+p.getId();
			}
			


		}
	}
	
	@PreAuthorize("hasRole('CAN_ACCESS_PARAMETRAGE')")
	@RequestMapping(value = "/parametrage/utilisateurs/delete", method = RequestMethod.GET)
	public String delete(
			@RequestParam(value = "id", required = true, defaultValue = "0") int id,
			final RedirectAttributes redirectAttributes,
			HttpServletRequest request
			) {
		try {
			User p = userService.findById(id);
			userService.delete(id);
			logSvc.store("Suppression Utilisateur: "+p, request);

			redirectAttributes.addFlashAttribute("success_user_delete", p.getUsername()
					+ " a ete supprimer");

			return "redirect:/parametrage/utilisateurs";
		} catch (NotFoundException e) {
			redirectAttributes.addFlashAttribute("error_user_delete", "Entity not found");
			return "redirect:/parametrage/utilisateurs";
		}
		
	}
	

}
