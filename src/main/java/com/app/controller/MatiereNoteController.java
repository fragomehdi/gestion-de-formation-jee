package com.app.controller;


import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.app.model.Centre;
import com.app.service.CentreService;

@Controller
public class MatiereNoteController {
	@Autowired
	CentreService centreSvc;


	
	@PreAuthorize("hasRole('MATIERE_NOTE_READ')")
	@RequestMapping(value = "/notes/matieres", method = RequestMethod.GET)
	public String index(Model model ) 
	{
		ObjectMapper mapper = new ObjectMapper();
		List<Centre> list = centreSvc.getAll();
		
		String json = "";
		try {
			json = mapper.writeValueAsString(list);
		} catch (Exception e) {
			e.printStackTrace();
		}
		 
		model.addAttribute("liste_centre_json", json);
		return "/notes/matieres/liste";
	}
	
	

}
