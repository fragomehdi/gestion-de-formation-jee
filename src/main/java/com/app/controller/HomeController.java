package com.app.controller;



import java.util.Calendar;

import javassist.NotFoundException;

import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpSession;


//import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.app.enums.AbsenceType;
import com.app.model.Etudiant;
import com.app.model.User;
//import com.app.model.Etudiant;
import com.app.service.AbsenceService;
import com.app.service.EtudiantService;
import com.app.service.LogTrackerService;
import com.app.service.UserService;
import com.app.service.VersementService;


@Controller
public class HomeController {
	@Autowired
	LogTrackerService logTrackerService;
	@Autowired
	AbsenceService absenceService;
	@Autowired
	EtudiantService etudiantService;
	@Autowired
	VersementService versementService;
	@Autowired
	UserService userservice;
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView home(HttpServletRequest request) {
		
		UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal(); 
		User u;
		try {
			u = userservice.findByUsername(userDetails.getUsername());
			Etudiant e = etudiantService.findByUser(u);
			System.out.println(e);
			if(e != null)
			{
				return etudiant(e);
			}
			else
				return admin();
		} catch (NotFoundException e1) {
			return admin();
		}
		
		
		
		
		
	}
	
	private ModelAndView etudiant(Etudiant e)
	{
		ModelAndView mv = new ModelAndView("homeEtudiant");
		mv.addObject("e", e);
		return mv;
	}
	
	private ModelAndView admin()
	{
		ModelAndView mv = new ModelAndView("home");
		
		Calendar now = Calendar.getInstance();
		int year = now.get(Calendar.YEAR);
		String yearInString = String.valueOf(year);
		
		int month = now.get(Calendar.MONTH)+1;
		/*String monthInString;
		if(month < 10)
			monthInString = "0"+String.valueOf(month);
		else
			monthInString = String.valueOf(month);*/
		
		String [] months = {"01","02","03","04","05","06","07","08","09","10","11","12"};
		for(int i=0;i<months.length;i++){
			mv.addObject("absences_m"+months[i], absenceService.totalByMonthYear(months[i],yearInString, AbsenceType.Absence.ordinal()));
		}
	
		
		for(int x=0;x<months.length;x++){
			mv.addObject("retards_m"+months[x], absenceService.totalByMonthYear(months[x],yearInString, AbsenceType.Retard.ordinal()));
		}
		
		
		mv.addObject("total_etudiants", etudiantService.total());
		mv.addObject("users", userservice.getAll());
		
		mv.addObject("total_versements", versementService.getBenificeByMonthYear(month, year));
		
		mv.addObject("logs_liste", logTrackerService.getAll(0, 5));
		
		return mv;
	}

}