package com.app.controller;

import javassist.NotFoundException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.app.enums.AbsenceType;
import com.app.enums.TypeSeance;
import com.app.json.JResponse;
import com.app.model.Absence;
import com.app.model.Annee;
import com.app.model.Centre;
import com.app.model.Etudiant;
import com.app.model.Filiere;
import com.app.model.Formation;
import com.app.model.Matiere;
import com.app.model.Module;
import com.app.model.Niveaux;
import com.app.model.Semestre;
import com.app.propertyeditors.AbsenceTypeEditor;
import com.app.propertyeditors.AnneeEditor;
import com.app.propertyeditors.CentreEditor;
import com.app.propertyeditors.EtudiantEditor;
import com.app.propertyeditors.FiliereEditor;
import com.app.propertyeditors.FormationEditor;
import com.app.propertyeditors.MatiereEditor;
import com.app.propertyeditors.ModuleEditor;
import com.app.propertyeditors.NiveauEditor;
import com.app.propertyeditors.SemestreEditor;
import com.app.propertyeditors.TypeSeanceEditor;
import com.app.service.AbsenceService;
import com.app.service.AnneeService;
import com.app.service.CentreService;
import com.app.service.EtudiantService;
import com.app.service.FiliereService;
import com.app.service.FormationService;
import com.app.service.LogTrackerService;
import com.app.service.MatiereService;
import com.app.service.ModuleService;
import com.app.service.NiveauxService;
import com.app.service.SemestreService;

@Controller
public class RetardController {
	@Autowired
	LogTrackerService logSvc;
	@Autowired
	AbsenceService absenceService;
	@Autowired
	EtudiantService etudiantService;
	@Autowired
	FiliereService filiereService;
	@Autowired
	NiveauxService niveauxService;
	@Autowired
	AnneeService anneeService;
	@Autowired
	CentreService centreService;
	@Autowired
	FormationService formationService;
	@Autowired
	ModuleService moduleService;
	@Autowired
	MatiereService matiereService;
	@Autowired
	SemestreService semestreService;
	
	@InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Etudiant.class, new EtudiantEditor());
        binder.registerCustomEditor(Niveaux.class, new NiveauEditor());
        binder.registerCustomEditor(Filiere.class, new FiliereEditor());
        binder.registerCustomEditor(Annee.class, new AnneeEditor());
        binder.registerCustomEditor(Centre.class, new CentreEditor());
        binder.registerCustomEditor(Formation.class, new FormationEditor());
        binder.registerCustomEditor(Module.class, new ModuleEditor());
        binder.registerCustomEditor(Matiere.class, new MatiereEditor());
        binder.registerCustomEditor(Semestre.class, new SemestreEditor());
        binder.registerCustomEditor(AbsenceType.class, new AbsenceTypeEditor());
        binder.registerCustomEditor(TypeSeance.class, new TypeSeanceEditor());
    }
	
	@PreAuthorize("hasRole('RETARDS_READ')")
	@RequestMapping(value = "/etudiants/retards", method = RequestMethod.GET)
	public String index(Model model) 
	{
		model.addAttribute("liste", absenceService.getAllByType(AbsenceType.Retard));
		return "etudiants/retards/liste";
	}
	
	@PreAuthorize("hasRole('RETARDS_EDIT')")
	@RequestMapping(value = "/etudiants/retards/nonjustifier", method = RequestMethod.POST)
	public @ResponseBody JResponse nonjustifier(
			@RequestParam(value = "id", required = true, defaultValue = "0") int id) 
	{
		Absence ab = absenceService.findById(id);
		ab.setJustification("");
		ab.setJustifier(false);
		try {
			absenceService.update(ab);
			return new JResponse("200", "nice work");
		} catch (NotFoundException e) {
			return new JResponse("504", "error");
		}

	}
	
	@PreAuthorize("hasRole('RETARDS_EDIT')")
	@RequestMapping(value = "/etudiants/retards/justifier", method = RequestMethod.POST)
	public @ResponseBody JResponse justifier(
			@RequestParam(value = "id", required = true, defaultValue = "0") int id,
			@RequestParam(value = "msg", required = true, defaultValue = "0") String msg) 
	{
		Absence ab = absenceService.findById(id);
		ab.setJustification(msg);
		ab.setJustifier(true);
		try {
			absenceService.update(ab);
			return new JResponse("200", "nice work");
		} catch (NotFoundException e) {
			return new JResponse("504", "error");
		}

	}
	
	
	@PreAuthorize("hasRole('RETARDS_EDIT')")
	@RequestMapping(value = "/etudiants/retards/ajouter", method = RequestMethod.GET)
	public String ajouter(Model model) 
	{
		Absence a = new Absence();
		a.setType(AbsenceType.Retard);
		model.addAttribute("pForm", a);
		
		model.addAttribute("etudiants", etudiantService.getAll());
		model.addAttribute("filieres", filiereService.getAll());
		model.addAttribute("niveaux", niveauxService.getAll());
		model.addAttribute("annees", anneeService.getAll());
		model.addAttribute("centres", centreService.getAll());
		model.addAttribute("formations", formationService.getAll());
		model.addAttribute("modules", moduleService.getAll());
		model.addAttribute("matieres", matiereService.getAll());
		model.addAttribute("semestres", semestreService.getAll());
		model.addAttribute("types_seance", TypeSeance.values());
		
		return "etudiants/retards/ajouter";
	}
	
	@PreAuthorize("hasRole('RETARDS_EDIT')")
	@RequestMapping(value = "/etudiants/retards/save", method = RequestMethod.POST)
	public String save(
			@Valid @ModelAttribute("pForm") Absence p,
			BindingResult bindingResult,
			Model model,
			final RedirectAttributes redirectAttributes,
			HttpServletRequest request
			) throws Exception {
		if (bindingResult.hasErrors()) {
			System.out.println(bindingResult.toString());
			model.addAttribute("pForm", p);
			
			model.addAttribute("etudiants", etudiantService.getAll());
			model.addAttribute("filieres", filiereService.getAll());
			model.addAttribute("niveaux", niveauxService.getAll());
			model.addAttribute("annees", anneeService.getAll());
			model.addAttribute("centres", centreService.getAll());
			model.addAttribute("formations", formationService.getAll());
			model.addAttribute("modules", moduleService.getAll());
			model.addAttribute("matieres", matiereService.getAll());
			model.addAttribute("semestres", semestreService.getAll());
			model.addAttribute("types_seance", TypeSeance.values());
			
			
			return "etudiants/retards/ajouter";
		} else {
			p.setType(AbsenceType.Retard);
			absenceService.add(p);
			logSvc.store("Ajout retard: "+p, request);
			Etudiant e = etudiantService.findById(p.getEtudiant().getId());
			redirectAttributes.addFlashAttribute("success", "L'retard pour \""+e.getFullName()+"\" a ete bien ajouter");
			return "redirect:/etudiants/retards/ajouter";

		}
	}
	
	@PreAuthorize("hasRole('RETARDS_EDIT')")
	@RequestMapping(value = "/etudiants/retards/delete", method = RequestMethod.GET)
	public String delete(
			@RequestParam(value = "id", required = true, defaultValue = "0") int id,
			final RedirectAttributes redirectAttributes,
			HttpServletRequest request
			) {
		
		try {
			Absence p = absenceService.findById(id);
			absenceService.delete(id);
			logSvc.store("Suppression retard: "+p, request);
			Etudiant e = etudiantService.findById(p.getEtudiant().getId());
			redirectAttributes.addFlashAttribute("success_delete", "L'retard pour \""+e.getFullName()+"\" a ete bien supprimer");

			return "redirect:/etudiants/retards";
		} catch (NotFoundException e) {
			redirectAttributes.addFlashAttribute("error_delete", "Entity not found");
			return "redirect:/etudiants/retards";
		}
		
	}
	

}
