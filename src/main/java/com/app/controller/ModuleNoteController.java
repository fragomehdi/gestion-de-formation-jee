package com.app.controller;


import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


import com.app.model.Centre;
import com.app.service.CentreService;

@Controller
public class ModuleNoteController {
	/*@Autowired
	ModuleService ModuleSvc;*/
	@Autowired
	CentreService centreSvc;
	/*@Autowired
	AnneeService anneeSvc;
	@Autowired
	NiveauxService niveauSvc;
	@Autowired
	FormationService formationSvc;
	@Autowired
	FiliereService filiereSvc;
	@Autowired
	SemestreService semestreSvc;
	@Autowired
	LogTrackerService logSvc;
	*/
	/*
	@InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Centre.class, new CentreEditor());
        binder.registerCustomEditor(Annee.class, new AnneeEditor());
        binder.registerCustomEditor(Niveaux.class, new NiveauEditor());
        binder.registerCustomEditor(Filiere.class, new FiliereEditor());
        binder.registerCustomEditor(Formation.class, new FormationEditor());
        binder.registerCustomEditor(Semestre.class, new SemestreEditor());
        binder.registerCustomEditor(Module.class, new ModuleEditor());
    }
	*/
	
	@PreAuthorize("hasRole('MODULE_NOTE_READ')")
	@RequestMapping(value = "/notes/modules", method = RequestMethod.GET)
	public String index(Model model) 
	{
		
		ObjectMapper mapper = new ObjectMapper();
		List<Centre> list = centreSvc.getAll();
		
		String json = "";
		try {
			json = mapper.writeValueAsString(list);
		} catch (Exception e) {
			e.printStackTrace();
		}
		 
		model.addAttribute("liste_centre_json", json);
	
		
		return "/notes/modules/liste";
	}
	
	

}
