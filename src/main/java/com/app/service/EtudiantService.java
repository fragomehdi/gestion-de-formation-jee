package com.app.service;

import java.util.List;

import javassist.NotFoundException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.transaction.annotation.Transactional;

import com.app.model.Controle;
import com.app.model.Etudiant;
import com.app.model.Niveaux;
import com.app.model.User;

public class EtudiantService {
	@PersistenceContext
	private EntityManager em;

	@Transactional
	public List<Etudiant> getAll() {
		List<Etudiant> result = em.createQuery("SELECT p FROM Etudiant p",
				Etudiant.class).getResultList();
		return result;
	}
	
	public int total() {
		Object result = em.createNativeQuery("SELECT count(p.id_etudiant) FROM Etudiant p")
			.getSingleResult();
		return Integer.parseInt(result.toString());
	}
	
	

	@Transactional
	public void add(Etudiant p) {
		System.out.println("***** SERVICE *****");
		System.out.println(p);
		System.out.println("***** /SERVICE *****");
		em.persist(p);
	}
	
	@Transactional(rollbackFor=NotFoundException.class)
	public void delete(int id) throws NotFoundException {
		Etudiant p = findById(id);
		if (p == null)
            throw new NotFoundException(null);
	   em.remove(p);
	}

	@Transactional(rollbackFor=NotFoundException.class)
	public Etudiant update(Etudiant p) throws NotFoundException{
		Etudiant up = findById(p.getId());
		if (up == null)
            throw new NotFoundException(null);
		
		em.merge(p);
		return findById(p.getId());
	}

	public Etudiant findById(int id) {
		return em.find(Etudiant.class, id);
	}
	
	@Transactional
	public Etudiant findByUser(User user)
            throws NotFoundException {
		List<Etudiant> result = em.createQuery("SELECT p FROM Etudiant p WHERE utilisateur = :user",
				Etudiant.class).setParameter("user", user).getResultList();
		
		if(result == null || result.size() <= 0 )
			return null;
	    
	    return result.get(0);
	}
	
	@Transactional
	public List<Etudiant> findByNiveau(Niveaux n)
            throws NotFoundException {
		List<Etudiant> result = em.createQuery("SELECT p FROM Etudiant p WHERE niveaux = :niveaux",
				Etudiant.class).setParameter("niveaux", n).getResultList();
		
		if(result == null || result.size() <= 0 )
			return null;
	    
	    return result;
	}
	
	/**/
	@Transactional
	public List<Etudiant> findByControle(Controle controle)
            throws NotFoundException {
		List<Etudiant> result = em.createQuery("select p from Etudiant p, Controle c,Niveaux n where p.niveaux = n and c.niveaux=n and c= :controle",
				Etudiant.class).setParameter("controle", controle).getResultList();
		
		if(result == null || result.size() <= 0 )
			return null;
	    
	    return result;
	}
	
}