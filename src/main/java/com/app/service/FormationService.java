package com.app.service;

import java.util.List;

import javassist.NotFoundException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.transaction.annotation.Transactional;

import com.app.model.Annee;
import com.app.model.Centre;
import com.app.model.Formation;

public class FormationService {
	@PersistenceContext
	private EntityManager em;

	@Transactional
	public List<Formation> getAll() {
		List<Formation> result = em.createQuery("SELECT p FROM Formation p",
				Formation.class).getResultList();
		return result;
	}

	@Transactional
	public void add(Formation p) {
		System.out.println("***** SERVICE *****");
		System.out.println(p);
		System.out.println("***** /SERVICE *****");
		em.persist(p);
		


	}
	
	@Transactional(rollbackFor=NotFoundException.class)
	public void delete(int id) throws NotFoundException {
		Formation p = findById(id);
		if (p == null)
            throw new NotFoundException(null);
		
	   em.remove(p);
	}

	@Transactional(rollbackFor=NotFoundException.class)
	public Formation update(Formation p) throws NotFoundException{
		Formation up = findById(p.getId());
		if (up == null)
            throw new NotFoundException(null);
		
		em.merge(p);
		return findById(p.getId());
	}

	public Formation findById(int id) {
		return em.find(Formation.class, id);
	}
	
	@Transactional
	public List<Formation> findByCentreAnnee(Centre c, Annee a) {
		List<Formation> result = em.createQuery("SELECT p FROM Formation p where  centre=:centre and annee=:annee",
				Formation.class)
				.setParameter("centre", c)
				.setParameter("annee", a)
				.getResultList();
		return result;
	}
	
}