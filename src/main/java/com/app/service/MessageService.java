package com.app.service;

import java.util.List;

import javassist.NotFoundException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Session;
import org.springframework.transaction.annotation.Transactional;

import com.app.model.Message;
import com.app.model.MessageThread;
import com.app.model.User;


public class MessageService {
	@PersistenceContext
	private EntityManager em;

	

	@Transactional
	public List<MessageThread> getAll() {
		List<MessageThread> result = em.createQuery("SELECT p FROM MessageThread p",
				MessageThread.class).getResultList();
		return result;
	}
	
	@Transactional
	public List<MessageThread> getInbox(User user)
            throws NotFoundException {
		List<MessageThread> result = em.createQuery("SELECT p FROM MessageThread p WHERE p.to = :user ORDER BY p.dateMessage DESC",
				MessageThread.class).setParameter("user", user).getResultList();
		
	    return result;
	}
	
	@Transactional
	public List<MessageThread> getOutbox(User user)
            throws NotFoundException {
		List<MessageThread> result = em.createQuery("SELECT p FROM MessageThread p WHERE p.from = :user ORDER BY p.dateMessage DESC",
				MessageThread.class).setParameter("user", user).getResultList();
		
	    return result;
	}
	
	@Transactional
	public List<Message> getLast(User user)
            throws NotFoundException {
		List<Message> result = em.createQuery("SELECT NEW com.app.model.Message(p.id, p.read,p.from.id,p.from.username,p.dateMessage,p.messageThread.id, p.contenu) FROM Message p WHERE p.to = :user ORDER BY p.dateMessage DESC",
				Message.class).setParameter("user", user).setFirstResult(0).setMaxResults(6).getResultList();
		
	    return result;
	}
	
	@Transactional
	public int count(User u){
		Session session = em.unwrap(Session.class);
		
		int c = ((Long)session.createQuery("select count(*) from Message where to_user = :user and is_read = 2").setParameter("user", u.getId()).uniqueResult()).intValue();

		return c;
		/*System.out.println(u.getPassword());
		Object result = em.createNativeQuery("SELECT count(*) FROM messages_thread  where to_user = :user and is_read = 0", MessageThread.class)
				.setParameter("user", u.getId())
				.getSingleResult();
		//em.flush();
		
		return Integer.parseInt(result.toString());*/
			
		
		
	}
	
	@Transactional
	public void add(MessageThread p) {
		System.out.println("***** SERVICE *****");
		System.out.println(p);
		System.out.println("***** /SERVICE *****");
		em.persist(p);
		


	}
	
	@Transactional(rollbackFor=NotFoundException.class)
	public void delete(int id) throws NotFoundException {
		MessageThread p = findById(id);
		if (p == null)
            throw new NotFoundException(null);
		
	   em.remove(p);
	}

	@Transactional(rollbackFor=NotFoundException.class)
	public MessageThread update(MessageThread p) throws NotFoundException{
		MessageThread up = findById(p.getId());
		if (up == null)
            throw new NotFoundException(null);
		
		em.merge(p);
		return findById(p.getId());
	}
	
	@Transactional(rollbackFor=NotFoundException.class)
	public void updateChild(Message p){
		em.merge(p);
	}

	public MessageThread findById(int id) {
		return em.find(MessageThread.class, id);
	}
}