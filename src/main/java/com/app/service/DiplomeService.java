package com.app.service;

import java.util.List;

import javassist.NotFoundException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.transaction.annotation.Transactional;

import com.app.model.Diplome;

public class DiplomeService {
	@PersistenceContext
	private EntityManager em;

	@Transactional
	public List<Diplome> getAll() {
		List<Diplome> result = em.createQuery("SELECT p FROM Diplome p",
				Diplome.class).getResultList();
		return result;
	}

	@Transactional
	public Diplome add(Diplome p) {
		System.out.println("***** SERVICE *****");
		System.out.println(p);
		System.out.println("***** /SERVICE *****");
		em.persist(p);
		return p;


	}
	
	@Transactional(rollbackFor=NotFoundException.class)
	public void delete(int id) throws NotFoundException {
		Diplome p = findById(id);
		if (p == null)
            throw new NotFoundException(null);
		
	   em.remove(p);
	}

	@Transactional(rollbackFor=NotFoundException.class)
	public Diplome update(Diplome p) throws NotFoundException{
		Diplome up = findById(p.getId());
		if (up == null)
            throw new NotFoundException(null);
		
		em.merge(p);
		return findById(p.getId());
	}

	public Diplome findById(int id) {
		return em.find(Diplome.class, id);
	}
}