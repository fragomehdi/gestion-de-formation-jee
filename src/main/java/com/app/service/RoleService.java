package com.app.service;

import java.util.List;

import javassist.NotFoundException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.transaction.annotation.Transactional;

import com.app.model.Role;

//@Service
public class RoleService {
	@PersistenceContext
	private EntityManager em;
	
	@Transactional
	public List<Role> getAll() {
		List<Role> result = em.createQuery("SELECT p FROM Role p",
				Role.class).getResultList();
		return result;
	}

	
	@Transactional
	public void add(Role p) {
		em.persist(p);
	}
	
	@Transactional(rollbackFor=NotFoundException.class)
	public void delete(int id) throws NotFoundException {
		Role p = findById(id);
		if (p == null)
            throw new NotFoundException(null);
		
	   em.remove(p);
	}

	@Transactional(rollbackFor=NotFoundException.class)
	public Role update(Role p) throws NotFoundException{
		Role up = findById(p.getId());
		if (up == null)
            throw new NotFoundException(null);
		
		em.merge(p);
		return findById(p.getId());
	}

	public Role findById(int id) {
		return em.find(Role.class, id);
	}
	
	@Transactional
	public Role findByName(String name) {
		List<Role> result = em.createQuery("SELECT p FROM Role p WHERE name = :name",
				Role.class).setParameter("name", name).getResultList();
	    
	    return result.get(0);
	}

}
