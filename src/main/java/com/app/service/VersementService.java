package com.app.service;

import java.util.List;

import javassist.NotFoundException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


import org.springframework.transaction.annotation.Transactional;

import com.app.enums.TypeVersement;
import com.app.model.Versement;


public class VersementService {
	@PersistenceContext
	private EntityManager em;

	@Transactional
	public List<Versement> getAll() {
		List<Versement> result = em.createQuery("SELECT p FROM Versement p",
				Versement.class).getResultList();
		return result;
	}
	
	@Transactional
	public List<Versement> getAllByType(TypeVersement versement) {
		List<Versement> result = em.createQuery("SELECT p FROM Versement p WHERE type = :type",
				Versement.class).setParameter("type", versement).getResultList();
		return result;
	}
	
	@Transactional
	public Number getBenificeByMonthYear(int month, int year)
	{
		System.out.println(month);
		System.out.println(year);
		Query query = em.createQuery("select sum(p.montant) from Versement p where MONTH(p.dateSeance)=:month and year(p.dateSeance)=:year")
				.setParameter("month", month)
				.setParameter("year", year);
		  return (Number) query.getSingleResult();

		/*System.out.println(month);
		System.out.println(year);
		Object result = em.createNativeQuery("SELECT SUM(p.montant) FROM Versement p where month(p.date_seance)=:month and year(p.date_seance)=:year")
				.setParameter("month", month)
				.setParameter("year", year)
				.getSingleResult();
		if(result == null)
			return 0;
		System.out.println(result);
		System.out.println(result.toString());
		return Double.parseDouble(result.toString());*/
	}

	@Transactional
	public void add(Versement p) {
		System.out.println("***** SERVICE *****");
		System.out.println(p);
		System.out.println("***** /SERVICE *****");
		em.persist(p);
		


	}
	
	@Transactional(rollbackFor=NotFoundException.class)
	public void delete(int id) throws NotFoundException {
		Versement p = findById(id);
		if (p == null)
            throw new NotFoundException(null);
		
	   em.remove(p);
	}

	@Transactional(rollbackFor=NotFoundException.class)
	public Versement update(Versement p) throws NotFoundException{
		Versement up = findById(p.getId());
		if (up == null)
            throw new NotFoundException(null);
		
		em.merge(p);
		return findById(p.getId());
	}

	public Versement findById(int id) {
		return em.find(Versement.class, id);
	}
}