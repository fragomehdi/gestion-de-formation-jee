package com.app.service;

import java.util.List;

import javassist.NotFoundException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.transaction.annotation.Transactional;

import com.app.model.Annee;
import com.app.model.Centre;
import com.app.model.Controle;
import com.app.model.Filiere;
import com.app.model.Formation;
import com.app.model.Module;
import com.app.model.Niveaux;
import com.app.model.Semestre;

public class ModuleService {
	@PersistenceContext
	private EntityManager em;

	@Transactional
	public List<Module> getAll() {
		List<Module> result = em.createQuery("SELECT p FROM Module p",
				Module.class).getResultList();
		return result;
	}

	@Transactional
	public void add(Module p) {
		System.out.println("***** SERVICE *****");
		System.out.println(p);
		System.out.println("***** /SERVICE *****");
		em.persist(p);
		


	}
	
	@Transactional(rollbackFor=NotFoundException.class)
	public void delete(int id) throws NotFoundException {
		Module p = findById(id);
		if (p == null)
            throw new NotFoundException(null);
		
	   em.remove(p);
	}

	@Transactional(rollbackFor=NotFoundException.class)
	public Module update(Module p) throws NotFoundException{
		Module up = findById(p.getId());
		if (up == null)
            throw new NotFoundException(null);
		
		em.merge(p);
		return findById(p.getId());
	}

	public Module findById(int id) {
		return em.find(Module.class, id);
	}
	
	@Transactional
	public List<Module> findByCentreAnneeFormationFiliereNiveauSemestre(Centre c, Annee a, Formation f, Filiere fi, Niveaux n, Semestre s) {
		List<Module> result = em.createQuery("SELECT p FROM Module p where  centre=:centre and annee=:annee and formation=:formation and filiere=:filiere and niveau=:niveaux and semestre=:semestre",
				Module.class)
				.setParameter("centre", c)
				.setParameter("annee", a)
				.setParameter("formation", f)
				.setParameter("filiere", fi)
				.setParameter("niveaux", n)
				.setParameter("semestre", s)
				.getResultList();
		return result;
	}
	
	@Transactional
	public List<Module> findBySemestre(Semestre s) {
		List<Module> result = em.createQuery("SELECT p FROM Module p where  semestre=:semestre",
				Module.class).setParameter("semestre", s).getResultList();
		return result;
	}
	
}