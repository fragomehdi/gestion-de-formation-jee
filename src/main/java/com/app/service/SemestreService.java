package com.app.service;

import java.util.List;

import javassist.NotFoundException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.transaction.annotation.Transactional;

import com.app.model.Annee;
import com.app.model.Centre;
import com.app.model.Controle;
import com.app.model.Filiere;
import com.app.model.Formation;
import com.app.model.Niveaux;
import com.app.model.Semestre;

public class SemestreService {
	@PersistenceContext
	private EntityManager em;

	@Transactional
	public List<Semestre> getAll() {
		List<Semestre> result = em.createQuery("SELECT p FROM Semestre p",
				Semestre.class).getResultList();
		return result;
	}

	@Transactional
	public void add(Semestre p) {
		System.out.println("***** SERVICE *****");
		System.out.println(p);
		System.out.println("***** /SERVICE *****");
		em.persist(p);
		


	}
	
	@Transactional(rollbackFor=NotFoundException.class)
	public void delete(int id) throws NotFoundException {
		Semestre p = findById(id);
		if (p == null)
            throw new NotFoundException(null);
		
	   em.remove(p);
	}

	@Transactional(rollbackFor=NotFoundException.class)
	public Semestre update(Semestre p) throws NotFoundException{
		Semestre up = findById(p.getId());
		if (up == null)
            throw new NotFoundException(null);
		
		em.merge(p);
		return findById(p.getId());
	}

	public Semestre findById(int id) {
		return em.find(Semestre.class, id);
	}
	
	
	@Transactional
	public List<Semestre> findByCentreAnneeFormationFiliereNiveau(Centre c, Annee a, Formation f, Filiere fi, Niveaux n) {
		List<Semestre> result = em.createQuery("SELECT p FROM Semestre p where  centre=:centre and annee=:annee and formation=:formation and filiere=:filiere and niveaux=:niveaux",
				Semestre.class)
				.setParameter("centre", c)
				.setParameter("annee", a)
				.setParameter("formation", f)
				.setParameter("filiere", fi)
				.setParameter("niveaux", n)
				.getResultList();
		return result;
	}
	
	@Transactional
	public List<Semestre> findByAnnee(Annee s) {
		List<Semestre> result = em.createQuery("SELECT p FROM Semestre p where  annee=:annee ORDER BY id ASC",
				Semestre.class).setParameter("annee", s).getResultList();
		return result;
	}
	
}