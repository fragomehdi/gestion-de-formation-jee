package com.app.service;

import java.util.List;

import javassist.NotFoundException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.transaction.annotation.Transactional;

import com.app.model.User;

public class UserService {
	@PersistenceContext
	private EntityManager em;
	
	@Transactional
	public List<User> getAll() {
		List<User> result = em.createQuery("SELECT p FROM User p",
				User.class).getResultList();
		return result;
	}

	@Transactional
	public void add(User p) {
		em.persist(p);
	}
	
	@Transactional(rollbackFor=NotFoundException.class)
	public void delete(int id) throws NotFoundException {
		User p = findById(id);
		if (p == null)
            throw new NotFoundException(null);
		
	   em.remove(p);
	}

	@Transactional(rollbackFor=NotFoundException.class)
	public User update(User p) throws NotFoundException{
		User up = findById(p.getId());
		if (up == null)
            throw new NotFoundException(null);
		
		em.merge(p);
		return findById(p.getId());
	}

	public User findById(int id) {
		return em.find(User.class, id);
	}
	
	@Transactional
	public User findByUsername(String username)
            throws NotFoundException {
		List<User> result = em.createQuery("SELECT p FROM User p WHERE username = :username",
				User.class).setParameter("username", username).getResultList();
		
		if(result != null && result.size() > 0)
			return result.get(0);
		else
			return null;
	}
	
}
