package com.app.propertyeditors;

import java.beans.PropertyEditorSupport;

import com.app.model.Versement;

public class VersementEditor extends PropertyEditorSupport {
	
	// Converts a String to a Permission (when submitting form)
    @Override
    public void setAsText(String text) {
    	int id=Integer.valueOf(text);
    	Versement c = new Versement();
    	c.setId(id);
        this.setValue(c);
    }

    // Converts a Permission to a String (when displaying form)
    public String getAsText() {
    	if(getValue() == null){
    	    return null;
    	   }
    	Versement c = (Versement) this.getValue();
    	if (c != null)
            return c.getDateSeance().toString();
        return null;
    }

}
