package com.app.propertyeditors;

import java.beans.PropertyEditorSupport;


import com.app.model.Filiere;

public class FiliereEditor extends PropertyEditorSupport {
	
	// Converts a String to a Permission (when submitting form)
    @Override
    public void setAsText(String text) {
    	int id=Integer.valueOf(text);
    	Filiere c = new Filiere();
    	c.setId(id);
        this.setValue(c);
    }

    // Converts a Permission to a String (when displaying form)
    public String getAsText() {
    	if(getValue() == null){
    	    return null;
    	   }
    	Filiere c = (Filiere) this.getValue();
    	if (c != null)
            return c.getIntitule();
        return null;
    }

}
