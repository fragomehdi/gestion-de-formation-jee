package com.app.propertyeditors;

import java.beans.PropertyEditorSupport;

import com.app.model.Etudiant;

public class EtudiantEditor extends PropertyEditorSupport {
	
	// Converts a String to a Etudiant (when submitting form)
    @Override
    public void setAsText(String text) {
    	int id=Integer.valueOf(text);
    	Etudiant c = new Etudiant();
    	c.setId(id);
        this.setValue(c);
    }

    // Converts a Etudiant to a String (when displaying form)
    public String getAsText() {
    	if(getValue() == null){
    	    return null;
    	   }
    	Etudiant c = (Etudiant) this.getValue();
    	if (c != null)
            return c.getFullName();
        return null;
    }

}
