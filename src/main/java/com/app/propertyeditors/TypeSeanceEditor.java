package com.app.propertyeditors;

import java.beans.PropertyEditorSupport;

import com.app.enums.TypeSeance;



public class TypeSeanceEditor extends PropertyEditorSupport {
	
	// Converts a String to a Permission (when submitting form)
    @Override
    public void setAsText(String text) {
        this.setValue(TypeSeance.valueOf(text));
    }

    // Converts a Permission to a String (when displaying form)
    public String getAsText() {
    	if(getValue() == null){
    	    return null;
    	   }
    	TypeSeance c = (TypeSeance) this.getValue();
    	if (c != null)
            return c.name();
        return null;
    }

}
