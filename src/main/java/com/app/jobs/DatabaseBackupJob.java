package com.app.jobs;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.security.CodeSource;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import javassist.bytecode.stackmap.TypeData.ClassName;

public class DatabaseBackupJob {
	
	private static final Logger log = Logger.getLogger( ClassName.class.getName() );

	public void startBackup() {
		log.info("Backing up database");
		
		// start();
		
	}
	
	public void start(){
		/*NOTE: Getting path to the Jar file being executed*/
        /*NOTE: YourImplementingClass-> replace with the class executing the code*/
		CodeSource codeSource = DatabaseBackupJob.class.getProtectionDomain().getCodeSource();
        File jarFile = null;
		try {
			jarFile = new File(codeSource.getLocation().toURI().getPath());
		} catch (URISyntaxException e) {
			log.log(Level.SEVERE, e.getMessage());
		}
        String jarDir = jarFile.getParentFile().getPath();
        
        /*NOTE: Creating Path Constraints for folder saving*/
        /*NOTE: Here the backup folder is created for saving inside it*/
        String folderPath = jarDir + "/backup";
        
        /*NOTE: Creating Folder if it does not exist*/
        File f1 = new File(folderPath);
        f1.mkdirs();
        
        /*NOTE: Creating Path Constraints for backup saving*/
        /*NOTE: Here the backup is saved in a folder called backup with the name backup.sql*/
         String savePath = jarDir + "/backup/";
         
		this.backupDataWithOutDatabase(
				"/usr/local/mysql/bin/mysqldump", 
				"localhost", 
				"3306",
				"root",
				"qwerty", 
				"testdb", 
				savePath
		);
	}

	public boolean backupDataWithOutDatabase(String dumpExePath, String host,
			String port, String user, String password, String database,
			String backupPath) {
		boolean status = false;
		try {
			Process p = null;

			DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy-h-i-s");
			Date date = new Date();
			String filepath = "backup-" + database + "-" + host
					+ "-(" + dateFormat.format(date) + ").sql";

			String batchCommand = "";
			if (password != "") {
				// only backup the data not included create database
				batchCommand = dumpExePath + " -h " + host + " --port " + port
						+ " -u " + user + " --password=" + password + " "
						+ database + " -r \"" + backupPath + "" + filepath
						+ "\"";
			} else {
				batchCommand = dumpExePath + " -h " + host + " --port " + port
						+ " -u " + user + " " + database + " -r \""
						+ backupPath + "" + filepath + "\"";
			}
			
			
			log.info(batchCommand);

			Runtime runtime = Runtime.getRuntime();
			p = runtime.exec(batchCommand);
			int processComplete = p.waitFor();

			if (processComplete == 0) {
				status = true;
				log.info("Backup created successfully for without DB "
						+ database + " in " + host + ":" + port);
			} else {
				status = false;
				log.info("Could not create the backup for without DB "
						+ database + " in " + host + ":" + port);
			}

		} catch (IOException e1) {
			log.log(Level.SEVERE, e1.getMessage());
		} catch (Exception e2) {
			log.log(Level.SEVERE, e2.getMessage());
		}
		return status;
	}

}
