package com.app.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

@Entity
@Table(name = "messages")
public class Message {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_message")
	private int id;
	
	@ManyToOne
	@JoinColumn(name="from_user")
	private User from;
	

	@ManyToOne
	@JoinColumn(name="to_user")
	private User to;
	
	@Type(type="text")
	private String contenu;
	
	@Column(name="date_message")
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	private DateTime dateMessage;
	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private MessageThread messageThread;
	
	@Column(name="is_read")
	private int read = 2; // not read by default

	public Message() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Message(int id, User from, String contenu,
			DateTime dateMessage, MessageThread messageThread) {
		super();
		this.id = id;
		this.from = from;
		this.contenu = contenu;
		this.dateMessage = dateMessage;
		this.messageThread = messageThread;
	}
	
	public Message(int id, int read, int uid, String username, DateTime dateMessage,
			int messageThread, String contenu) {
		super();
		this.id = id;
		this.read = read;
		User u = new User();
		u.setId(uid);
		u.setUsername(username);
		this.from = u;
		this.dateMessage = dateMessage;
		MessageThread mt = new MessageThread();
		mt.setId(messageThread);
		this.messageThread = mt;
		String value = "-";
		 if (contenu.length() <= 38) {
		    value = contenu;
		} else { 
		    value = contenu.substring(0, 38);
		}
		this.contenu = value;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public User getFrom() {
		return from;
	}

	public void setFrom(User from) {
		this.from = from;
	}


	public String getContenu() {
		return contenu;
	}

	public void setContenu(String contenu) {
		this.contenu = contenu;
	}

	public DateTime getDateMessage() {
		return dateMessage;
	}

	public void setDateMessage(DateTime dateMessage) {
		this.dateMessage = dateMessage;
	}

	public MessageThread getMessageThread() {
		return messageThread;
	}

	public void setMessageThread(MessageThread messageThread) {
		this.messageThread = messageThread;
	}

	

	public int getRead() {
		return read;
	}

	public void setRead(int read) {
		this.read = read;
	}

	public User getTo() {
		return to;
	}

	public void setTo(User to) {
		this.to = to;
	}

	@Override
	public String toString() {
		return "Message [id=" + id + ", from=" + from + ", to=" + to
				+ ", contenu=" + contenu + ", dateMessage=" + dateMessage
				+ ", messageThread=" + messageThread + ", read=" + read + "]";
	}
	
	
	
	
	
	
}
