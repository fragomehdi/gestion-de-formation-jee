package com.app.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

@Entity
@Table(name = "messages_thread")
public class MessageThread {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_thread")
	private int id;
	
	@ManyToOne
	@JoinColumn(name="from_user")
	private User from;
	
	@ManyToOne
	@JoinColumn(name="to_user")
	private User to;
	
	private String subject;
	
	@Column(name="date_message")
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	private DateTime dateMessage;
	
	@OneToMany(mappedBy = "messageThread", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@OrderBy("id_message ASC")
    private Set<Message> messages = new HashSet<Message>();
	
	@Column(name="is_read")
	private Boolean read = false;

	

	public MessageThread() {
		super();
		// TODO Auto-generated constructor stub
	}

	

	



	public MessageThread(int id, User from, User to, String subject,
			DateTime dateMessage, Set<Message> messages, Boolean read) {
		super();
		this.id = id;
		this.from = from;
		this.to = to;
		this.subject = subject;
		this.dateMessage = dateMessage;
		this.messages = messages;
		this.read = read;
	}







	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public User getFrom() {
		return from;
	}

	public void setFrom(User from) {
		this.from = from;
	}

	public User getTo() {
		return to;
	}

	public void setTo(User to) {
		this.to = to;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public DateTime getDateMessage() {
		return dateMessage;
	}

	public void setDateMessage(DateTime dateMessage) {
		this.dateMessage = dateMessage;
	}

	public Set<Message> getMessages() {
		return messages;
	}

	public void setMessages(Set<Message> messages) {
		this.messages = messages;
	}
	
	public void addMessage(Message msg) {
		  if (messages == null) {
			  messages = new HashSet<Message>();
		  }
		  messages.add(msg);
		  msg.setMessageThread(this);
	}


	public Boolean getRead() {
		return read;
	}
	
	public Boolean isNotRead(int uid) {
		for(Message m : messages)
		{
			if(m.getTo() != null && m.getTo().getId() == uid && m.getRead() == 2) // if the msg was sent to me and not market as read
				return true;
		}
		return false;
	}


	public void setRead(Boolean read) {
		this.read = read;
	}


	
	
	
}
