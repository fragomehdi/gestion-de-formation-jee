package com.app.model;



import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


import org.joda.time.DateTime;

@Entity
@DiscriminatorValue("emploie")
public class Emploie_Temps extends Document{

	@ManyToOne
	@JoinColumn(name="id_annee")
	private Annee annee;
	
	
	@ManyToOne
	@JoinColumn(name="id_niveau")
	private Niveaux niveau;
	
	
	@ManyToOne
	@JoinColumn(name="id_formation")
	private Formation formation;
	
	@ManyToOne
	@JoinColumn(name="id_filiere")
	private Filiere filiere;
	
	@ManyToOne
	@JoinColumn(name="id_semestre")
	private Semestre semestre;
	
	
	
	
	
	
	
	
	public Emploie_Temps() {
		super();
	}

	

	



	public Emploie_Temps(int id, String intitule, String path,
			DateTime date_creation, Annee annee,
			Niveaux niveau, Formation formation, Filiere filiere,
			Semestre semestre) {
		super(id, intitule, path, date_creation);
		this.annee = annee;
		this.niveau = niveau;
		this.formation = formation;
		this.filiere = filiere;
		this.semestre = semestre;
	}







	public Emploie_Temps(int id, String intitule, String path, DateTime date_creation) {
		super(id, intitule, path, date_creation);
		// TODO Auto-generated constructor stub
	}


	public Annee getAnnee() {
		return annee;
	}







	public void setAnnee(Annee annee) {
		this.annee = annee;
	}







	public Niveaux getNiveau() {
		return niveau;
	}







	public void setNiveau(Niveaux niveau) {
		this.niveau = niveau;
	}







	public Formation getFormation() {
		return formation;
	}







	public void setFormation(Formation formation) {
		this.formation = formation;
	}







	public Filiere getFiliere() {
		return filiere;
	}


	public void setFiliere(Filiere filiere) {
		this.filiere = filiere;
	}


	public Semestre getSemestre() {
		return semestre;
	}



	public void setSemestre(Semestre semestre) {
		this.semestre = semestre;
	}


	
}
