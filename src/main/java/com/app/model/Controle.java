package com.app.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.NotEmpty;
import org.joda.time.DateTime;
import org.springframework.format.annotation.DateTimeFormat;

import com.app.enums.Controlenum;
import com.app.enums.PaiementType;
import com.app.enums.TypeControle;

@Entity
@Table(name = "controle")
public class Controle {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_controle")
	private int id;
	@NotEmpty(message = "Veuillez entrer un nom.")
	@Size(min = 4, max = 20, message = "Le nom doit être compris entre 4 et 20 caractères")
	private String intitule;
	@Column(name="dateControle")
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	@DateTimeFormat(pattern = "MM/dd/yyyy")
	private DateTime dateControle;
	@Column(name="heure_debut")
	private String heureDebut;
	
	@Column(name="heure_fin")
	private String heureFin;
	private int coeff = 1;
	@ManyToOne
	@JoinColumn(name="id_matiere")
	private Matiere matiere;
	@ManyToOne
	@JoinColumn(name="id_semestre")
	private Semestre semestre;
	@ManyToOne
	@JoinColumn(name="id_enseignant")
	private Enseignant enseignant;
	@ManyToOne
	@JoinColumn(name="id_salle")
	private Salle salle;

	@ManyToOne
	@JoinColumn(name="id_formation")
	private Formation formation;

	@ManyToOne
	@JoinColumn(name="id_annee")
	private Annee annee;
	@ManyToOne
	@JoinColumn(name="id_niveau")
	private Niveaux niveaux;
	@Column(name="type")
	@Enumerated(EnumType.ORDINAL)
	private TypeControle type;
	@Column(name="numControle")
	@Enumerated(EnumType.ORDINAL)
	private Controlenum numControle;
	@ManyToOne
	@JoinColumn(name="id_module")
	private Module module;
	
	

	
	public Controle(int id, String intitule, DateTime dateControle,
			String heureDebut, String heureFin, int coeff, Matiere matiere,
			Semestre semestre, Enseignant enseignant, Salle salle,
			Formation formation, Annee annee, Niveaux niveaux,
			TypeControle type, Controlenum numControle, Module module) {
		super();
		this.id = id;
		this.intitule = intitule;
		this.dateControle = dateControle;
		this.heureDebut = heureDebut;
		this.heureFin = heureFin;
		this.coeff = coeff;
		this.matiere = matiere;
		this.semestre = semestre;
		this.enseignant = enseignant;
		this.salle = salle;
		this.formation = formation;
		this.annee = annee;
		this.niveaux = niveaux;
		this.type = type;
		this.numControle = numControle;
		this.module = module;
	}
	
	public Controlenum getNumControle() {
		return numControle;
	}

	public void setNumControle(Controlenum numControle) {
		this.numControle = numControle;
	}

	public Formation getFormation() {
		return formation;
	}
	public void setFormation(Formation formation) {
		this.formation = formation;
	}
	public Annee getAnnee() {
		return annee;
	}
	public void setAnnee(Annee annee) {
		this.annee = annee;
	}
	public Module getModule() {
		return module;
	}
	public void setModule(Module module) {
		this.module = module;
	}
	public Niveaux getNiveaux() {
		return niveaux;
	}
	public void setNiveaux(Niveaux niveaux) {
		this.niveaux = niveaux;
	}

	public TypeControle getType() {
		return type;
	}
	public void setType(TypeControle type) {
		this.type = type;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getIntitule() {
		return intitule;
	}
	public void setIntitule(String intitule) {
		this.intitule = intitule;
	}
	public DateTime getDateControle() {
		return dateControle;
	}
	public void setDateControle(DateTime dateControle) {
		this.dateControle = dateControle;
	}
	public String getHeureDebut() {
		return heureDebut;
	}
	public void setHeureDebut(String heureDebut) {
		this.heureDebut = heureDebut;
	}
	public String getHeureFin() {
		return heureFin;
	}
	public void setHeureFin(String heureFin) {
		this.heureFin = heureFin;
	}
	public int getCoeff() {
		return coeff;
	}
	public void setCoeff(int coeff) {
		this.coeff = coeff;
	}
	public Matiere getMatiere() {
		return matiere;
	}
	public void setMatiere(Matiere matiere) {
		this.matiere = matiere;
	}
	public Semestre getSemestre() {
		return semestre;
	}
	public void setSemestre(Semestre semestre) {
		this.semestre = semestre;
	}
	public Enseignant getEnseignant() {
		return enseignant;
	}
	public void setEnseignant(Enseignant enseignant) {
		this.enseignant = enseignant;
	}
	public Salle getSalle() {
		return salle;
	}
	public void setSalle(Salle salle) {
		this.salle = salle;
	}
	public Controle(int id, String intitule, DateTime dateControle,
			String heureDebut, String heureFin, int coeff, Matiere matiere,
			Semestre semestre, Enseignant enseignant, Salle salle) {
		super();
		this.id = id;
		this.intitule = intitule;
		this.dateControle = dateControle;
		this.heureDebut = heureDebut;
		this.heureFin = heureFin;
		this.coeff = coeff;
		this.matiere = matiere;
		this.semestre = semestre;
		this.enseignant = enseignant;
		this.salle = salle;
	}
	public Controle() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return intitule;
	}
	

}
