package com.app.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "affecterMatiere")
public class AffecterMatiere {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_affecterMatiere")
	private int id;

	
	@ManyToOne
	@JoinColumn(name="id_annee")
	private Annee annee;

	
	@ManyToOne
	@JoinColumn(name="id_filiere")
	private Filiere filiere;
	
	@ManyToOne
	@JoinColumn(name="id_niveaux")
	private Niveaux niveaux;
	
	@ManyToOne
	@JoinColumn(name="id_semestre")
	private Semestre semestre;

	
	@ManyToOne
	@JoinColumn(name="id_module")
	private Module module;
	
	@ManyToOne
	@JoinColumn(name="id_matiere")
	private Matiere matiere;
	
	@ManyToOne
	@JoinColumn(name="id_enseignant")
	private Enseignant enseignant;
	@ManyToOne
	@JoinColumn(name="id_formation")
	private Formation formation;
	public AffecterMatiere() {
		super();
	}


	
	public AffecterMatiere(int id, Annee annee, Filiere filiere,
			Niveaux niveaux, Semestre semestre, Module module, Matiere matiere,
			Enseignant enseignant, Formation formation) {
		super();
		this.id = id;
		this.annee = annee;
		this.filiere = filiere;
		this.niveaux = niveaux;
		this.semestre = semestre;
		this.module = module;
		this.matiere = matiere;
		this.enseignant = enseignant;
		this.formation = formation;
	}



	public Formation getFormation() {
		return formation;
	}

	public void setFormation(Formation formation) {
		this.formation = formation;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Annee getAnnee() {
		return annee;
	}

	public void setAnnee(Annee annee) {
		this.annee = annee;
	}

	public Filiere getFiliere() {
		return filiere;
	}

	public void setFiliere(Filiere filiere) {
		this.filiere = filiere;
	}

	public Niveaux getNiveaux() {
		return niveaux;
	}

	public void setNiveaux(Niveaux niveaux) {
		this.niveaux = niveaux;
	}

	public Semestre getSemestre() {
		return semestre;
	}

	public void setSemestre(Semestre semestre) {
		this.semestre = semestre;
	}

	public Module getModule() {
		return module;
	}

	public void setModule(Module module) {
		this.module = module;
	}

	public Matiere getMatiere() {
		return matiere;
	}

	public void setMatiere(Matiere matiere) {
		this.matiere = matiere;
	}

	public Enseignant getEnseignant() {
		return enseignant;
	}

	public void setEnseignant(Enseignant enseignant) {
		this.enseignant = enseignant;
	}

	@Override
	public String toString() {
		return "AffecterMatiere [id=" + id + ", annee=" + annee + ", filiere="
				+ filiere + ", niveaux=" + niveaux + ", semestre=" + semestre
				+ ", module=" + module + ", matiere=" + matiere
				+ ", enseignant=" + enseignant + "]";
	}


	
}
