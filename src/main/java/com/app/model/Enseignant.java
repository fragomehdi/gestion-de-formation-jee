package com.app.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import org.joda.time.DateTime;

@Entity
@DiscriminatorValue("enseignant")
public class Enseignant extends Person{

	private String diplome;
	private String adresse;
	private String facebook;
	private	String twiter;
	private String image;
	private String fonction;

	public Enseignant() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Enseignant(int id, String prenom, String nom, String sexe,
			String cin, String telephone, String email, DateTime date_entree,
			DateTime date_sortie, String banque, String rib, int type_paiement, String diplome,String adresse,String facebook,String twiter,String image) {
		super(id, prenom, nom, sexe, cin, telephone, email, date_entree, date_sortie,
				banque, rib, type_paiement);
		this.diplome = diplome;
		this.adresse = adresse;
		this.facebook = facebook;
		this.twiter = twiter;
		this.image = image;
		
	}

	
	public String getFonction() {
		return fonction;
	}

	public void setFonction(String fonction) {
		this.fonction = fonction;
	}

	public String getDiplome() {
		return diplome;
	}

	public void setDiplome(String diplome) {
		this.diplome = diplome;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getFacebook() {
		return facebook;
	}

	public void setFacebook(String facebook) {
		this.facebook = facebook;
	}

	public String getTwiter() {
		return twiter;
	}

	public void setTwiter(String twiter) {
		this.twiter = twiter;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	@Override
	public String toString() {
		return this.getFullname();
	}

	
	
	
	
}
