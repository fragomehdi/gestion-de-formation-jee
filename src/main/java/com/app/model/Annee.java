package com.app.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "annee")
public class Annee {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_annee")
	private int id;
	@NotEmpty(message = "Veuillez entrer un intitule.")
	@Size(min = 4, max = 20, message = "Le nom doit respecter la forme suivante : 20XX/20YY")
	private String intitule;
	
	@ManyToOne
	@JoinColumn(name="id_centre")
	private Centre centre;
	
	private Boolean actived = false;
	public Annee() {
		super();
	}


	





	public Annee(int id, String intitule, Centre centre, Boolean actived) {
		super();
		this.id = id;
		this.intitule = intitule;
		this.centre = centre;
		this.actived = actived;
	}








	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getIntitule() {
		return intitule;
	}


	public void setIntitule(String intitule) {
		this.intitule = intitule;
	}





	public Boolean getActived() {
		return actived;
	}





	public void setActived(Boolean actived) {
		this.actived = actived;
	}





	public Centre getCentre() {
		return centre;
	}








	public void setCentre(Centre centre) {
		this.centre = centre;
	}








	@Override
	public String toString() {
		return intitule;
	}


	


	
	
	
	
	

}
