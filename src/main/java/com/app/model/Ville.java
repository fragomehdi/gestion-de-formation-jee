package com.app.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "ville")
public class Ville {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_ville")
	private int id;
	@NotEmpty(message = "Veuillez entrer un nom.")
	@Size(min = 4, max = 20, message = "Le nom doit etre compris entre 4 et 20 caracteresS")
	private String intitule;
	
	
	public Ville() {
		super();
	}


	public Ville(int id, String intitule) {
		super();
		this.id = id;
		this.intitule = intitule;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getIntitule() {
		return intitule;
	}


	public void setIntitule(String intitule) {
		this.intitule = intitule;
	}


	@Override
	public String toString() {
		return intitule;
	}


	
	
	
	

}
