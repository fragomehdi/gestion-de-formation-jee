package com.app.model;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

public class Matiere_note {
	@ManyToOne
	@JoinColumn(name="id_etudiant")
	private Etudiant etudiant;
	@ManyToOne
	@JoinColumn(name="id_matiere")
	private Matiere matiere;
	@ManyToOne
	@JoinColumn(name="id_semestre")
	private Semestre semestre;
	Double moyenne;
	@NotEmpty(message = "Veuillez entrer un nom.")
	@Size(min = 4, max = 20, message = "Le nom doit être compris entre 4 et 20 caractères")
	String decision;
	public Matiere_note() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Matiere_note(Etudiant etudiant, Matiere matiere, Semestre semestre,
			Double moyenne, String decision) {
		super();
		this.etudiant = etudiant;
		this.matiere = matiere;
		this.semestre = semestre;
		this.moyenne = moyenne;
		this.decision = decision;
	}
	public Etudiant getEtudiant() {
		return etudiant;
	}
	public void setEtudiant(Etudiant etudiant) {
		this.etudiant = etudiant;
	}
	public Matiere getMatiere() {
		return matiere;
	}
	public void setMatiere(Matiere matiere) {
		this.matiere = matiere;
	}
	public Semestre getSemestre() {
		return semestre;
	}
	public void setSemestre(Semestre semestre) {
		this.semestre = semestre;
	}
	public Double getMoyenne() {
		return moyenne;
	}
	public void setMoyenne(Double moyenne) {
		this.moyenne = moyenne;
	}
	public String getDecision() {
		return decision;
	}
	public void setDecision(String decision) {
		this.decision = decision;
	}
	
	
}
